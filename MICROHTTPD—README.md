Install
============


./bootstrap
./configure
make | make install
sudo make install

 ls /usr/local/include | grep http
 ls /usr/local/lib/ | grep http
 
 Problems
 -----------
 Fix “autoreconf: failed to run autopoint: No such file or directory”
    sudo apt-get install gettext
    sudo apt-get install autopoint
    sudo apt-get install gettext-tools
    
Optional:
    sudo apt-get install uncrustify 
            — Installing uncrustify hook and configuration
    sudo apt-get install texinfo
            WARNING: 'makeinfo' is missing on your system.
            You should only need it if you modified a '.texi' file, or
            any other file indirectly affecting the aspect of the manual.
            You might want to install the Texinfo package:

> find . -name "*.a"
./src/microhttpd/.libs/libmicrohttpd.a
 ./src/testcurl/libcurl_version_check.a
 cd src/microhttpd/
 cd src/microhttpd/.libs
 src/microhttpd/.libs/libmicrohttpd.a



More
---
        https://www.gnu.org/software/libmicrohttpd/
        https://www.gnu.org/software/libmicrohttpd/tutorial.html
        https://www.gnu.org/software/libmicrohttpd/manual/libmicrohttpd.html
        

Bug Tracking
---
    https://bugs.gnunet.org/my_view_page.php?refresh=true
        
API Change Monitor
---
        https://abi-laboratory.pro/?view=timeline&l=libmicrohttpd


Password to Cryptography
---
    https://webauthn.guide
    
    User: 
        I want to create a new account.
    Server:
        Sure! Send me a public key.
        
    User:
        All right! Creating a new keypair.
        Okay, here's the public key!

    Server:
        Thanks! Registration complete.



daemon.c:5120:41: warning: result of comparison of constant 26503942634640160
with expression of type 'unsigned int' is always false
[-Wtautological-constant-out-of-range-compare]
else if (daemon->worker_pool_size >= (SIZE_MAX / sizeof (struct
         ~~~~~~~~~~~~~~~~~~~~~~~~ ^  ~~~~~~~~~~~~~~~~~~~~~~~~~~

websocket_threaded_example.c:717:19: warning: comparison of array 'buf' not
equal to a null pointer is always true [-Wtautological-pointer-compare]
if (NULL != buf)
    ~~~~    ^~~
