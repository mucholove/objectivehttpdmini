#!/bin/bash

#  linkHeaders.sh
#  ObjectiveHTTPDmini
#
#  Created by Gustavo Tavares on 5/4/20.
#  Copyright © 2020 Gustavo Tavares. All rights reserved.



# find . | grep \.h$ | sort
# find . | grep \.m$ | sort

DIRECTORY_TO_LINK=$1
DIRECTORY_OF_LINKS=$2


# https://program-script.com/bash/functions/
# https://stackoverflow.com/questions/61492028/how-to-stop-bash-expansion-of-h-in-a-function?noredirect=1#comment108776941_61492028

function link_files_of_type_from_directory {
    local file_type=$1
    local directory_to_link=$2
    local directory_of_links=$3

    echo "File type $file_type"
    echo "Directory to link $directory_to_link"
    
    cd directory_of_links

    find "$directory_to_link" -type f -name "$file_type" -exec sh -c '
      for i do
        if test -e "$(basename "$i")"; then
          echo "$i exists"
        else
          echo "Linking: $i"
          ln -s "$i"
        fi
      done
    ' sh {} +
}



echo "Linking headers......................";

link_files_of_type_from_directory '*.h' $DIRECTORY_TO_LINK $DIRECTORY_OF_LINKS

echo "Linking M files......................";

link_files_of_type_from_directory '*.m' $DIRECTORY_TO_LINK $DIRECTORY_OF_LINKS








function link_headers_in_directory {
    local directory_to_link=$1;
    link_files_of_type_from_directory '*.h' $directory_to_link
}

function link_objc_files_in_directory {
    local directory_to_link=$1;
    link_files_of_type_from_directory '*.m' $directory_to_link
}

function link_all_files_in_directory {
    local directory_to_link=$1;
    for i in $(find $directory_to_link);
        do if test -e $(basename $i); then
        echo $i exists;
        else
        echo Linking: $i;
        ln -s $i;
        fi
    done;
}


# echo "Linking special support files...";

# link_all_files_in_directory ./../../Web/
#
# link_all_files_in_directory ./../../Support/MyCode/
#
# link_all_files_in_directory ./../../Support/StandardPaths/
#
# link_all_files_in_directory ./../../Support/NSPredicate+DateRangePredicates/
#
# link_all_files_in_directory ./../../Support/LoremIpsum/
#
# echo "Linking PCH files...";
#
# link_all_files_in_directory ./../../PCH/
#
#
# rm  NSAttributedStringWrapper.h  \
#     NSAttributedStringWrapper.m  \
#     NSView+GTConstraintHelpers.h \
#     NSView+GTConstraintHelpers.m \
#     NSView+SquaresWithColor.h    \
#     NSView+SquaresWithColor.m    \
#     NSTableView+MakeTableColumn.h \
#     NSTableView+MakeTableColumn.m \
#     NSImage+IconForSize.h         \
#     NSImage+IconForSize.m         \

