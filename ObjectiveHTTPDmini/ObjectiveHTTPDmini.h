//
//  ObjectiveHTTPDmini.h
//  ObjectiveHTTPDmini
//
//  Created by Gustavo Tavares on 4/10/20.
//  Copyright © 2020 Gustavo Tavares. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for ObjectiveHTTPDmini.
FOUNDATION_EXPORT double ObjectiveHTTPDminiVersionNumber;

//! Project version string for ObjectiveHTTPDmini.
FOUNDATION_EXPORT const unsigned char ObjectiveHTTPDminiVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <ObjectiveHTTPDmini/PublicHeader.h>

#import <ObjectiveHTTPDmini/GTKHTTPServer.h>
#import <ObjectiveHTTPDmini/GTKHTTPPath2ClassDictionaryServer.h>
#import <ObjectiveHTTPDmini/GTKHTTPServerSession.h>


#import <ObjectiveHTTPDmini/GTKHTTPServerDelegate.h>
#import <ObjectiveHTTPDmini/GTKServerAuthDelegate.h>

#import <ObjectiveHTTPDmini/GTKHTTPGetRequest.h>
#import <ObjectiveHTTPDmini/GTKHTTPPostRequest.h>

#import <ObjectiveHTTPDmini/GTKHTTPServerResponse.h>
#import <ObjectiveHTTPDmini/GTKHTTP2HTTPSServer.h>
#import <GTKFoundation/MimeType.h>


#import <ObjectiveHTTPDmini/GTKHTTPSwitchPath.h>
#import <ObjectiveHTTPDmini/GTKHTTPRuntimeNestablePath.h>
#import <ObjectiveHTTPDmini/GTKHTTPFilePath.h>
#import <ObjectiveHTTPDmini/GTKHTTPBundleResourcePath.h>

#import <ObjectiveHTTPDmini/NSError+AsRequestableObject.h>
#import <ObjectiveHTTPDmini/GTKMHDWebSocketConnection.h>


#import <ObjectiveHTTPDmini/GTKDebugDirectoryPath.h>
#import <ObjectiveHTTPDmini/GTKDebugWebSocketsPath.h>
#import <ObjectiveHTTPDmini/GTKMemoryAllocationHistoryPath.h>
#import <ObjectiveHTTPDmini/GTKMemoryAllocationPath.h>
#import <ObjectiveHTTPDmini/GTKMHDHTTPRequest.h>
