//
//  GTKMemoryAllocationHistoryPath.m
//  ObjectiveHTTPDmini
//
//  Created by Gustavo Tavares on 6/4/21.
//  Copyright © 2021 Gustavo Tavares. All rights reserved.
//

#import <ObjectiveHTTPDmini/GTKHTTPServerResponse.h>
#import <GTKFoundation/GTKFoundation.h>
#import <ObjectiveHTTPDmini/GTKMemoryAllocationHistoryPath.h>

@implementation GTKMemoryAllocationHistoryPath
-(id)
serveRequest:(id)theRequest
{
    #if GNUSTEP
    NSMutableString* toReturn = [NSMutableString string];

    [toReturn appendFormat:@"<html>"];
    [toReturn appendFormat:@"<body>"];
    [toReturn appendFormat:@"<table>"];

    GTKAllocationSnapshotTracker* tracker
        = [GTKAllocationSnapshotTracker sharedTracker];

    NSMutableArray* table
        = [tracker asTable];

    NSMutableOrderedSet* datesSnapshotted = [tracker datesSnapshotted];

    [toReturn appendFormat:@"<tr>"];
    [toReturn appendFormat:@"<th>Class</th>"];

    [datesSnapshotted
        enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL* stop){
            [toReturn appendFormat:@"<th>%lu</th>", idx];
        }];

    [toReturn appendFormat:@"</tr>"];


    for (NSMutableArray* row in table)
    {
        [toReturn appendFormat:@"<tr>"];

        BOOL       didWriteName         = NO;
        NSUInteger numberOfNullsToWrite = 0;


        [toReturn appendFormat:@"<td>-</td>"];

        for (id item in row)
        {
            if ([item isKindOfClass:[GTKClassAllocationSnapshot class]])
            {
                GTKClassAllocationSnapshot* snapshot = (GTKClassAllocationSnapshot*)item;

                if (!didWriteName)
                {
                    [toReturn appendFormat:@"<td>%@</td>", NSStringFromClass([snapshot  analyzedClass])];
                    didWriteName = YES;
                }

                while (numberOfNullsToWrite > 0)
                {
                    [toReturn appendFormat:@"<td>-</td>"];
                    numberOfNullsToWrite--;
                }

                int currentCount = [snapshot currentCount];

                [toReturn appendFormat:@"<td>%d</td>", currentCount];

            }
            else
            {
                if (didWriteName)
                {
                    [toReturn appendFormat:@"<td>-</td>"];
                }
            }
        }



        [toReturn appendFormat:@"</tr>"];
    }

    [toReturn appendFormat:@"</table>"];
    [toReturn appendFormat:@"</body>"];
    [toReturn appendFormat:@"</html>"];

    GTKHTTPServerResponse* response = AUTORELEASE([[GTKHTTPServerResponse alloc] init]);

    [response setMIMEType:[MimeType html]];
    [response setRawData:[toReturn dataUsingEncoding:NSUTF8StringEncoding]];

    return response;
    #else
    return @"Only available on GNUstep";
    #endif
}
@end
