//
//  GTKMemoryAllocationPath.m
//  ObjectiveHTTPDmini
//
//  Created by Gustavo Tavares on 6/4/21.
//  Copyright © 2021 Gustavo Tavares. All rights reserved.
//

#import <GTKFoundation/GTKFoundation.h>
#import <ObjectiveHTTPDmini/GTKHTTPServerResponse.h>
#import <ObjectiveHTTPDmini/GTKMemoryAllocationPath.h>

@implementation GTKMemoryAllocationPath

-(id)
serveRequest:(GTKHTTPRequest*)theRequest
{
    #if GNUSTEP
        NSMutableString* toReturn = [NSMutableString string];


        Class* allocationClassList = GSDebugAllocationClassList();

        Class* toAnalyzedClass = allocationClassList;

        NSDate* now = AUTORELEASE([[NSDate alloc] init]);

        GTKAllocationSnapshotTracker* tracker = [GTKAllocationSnapshotTracker sharedTracker];

        [toReturn appendFormat:@"<html>"];
        [toReturn appendFormat:@"<body>"];
        [toReturn appendFormat:@"<table>"];

        [toReturn appendFormat:@"<tr>"];
        [toReturn appendFormat:@"<th data-type=\"string\">Class</th>"];
        [toReturn appendFormat:@"<th data-type=\"number\">Count</th>"];
        [toReturn appendFormat:@"<th data-type=\"number\">Peak</th>"];
        [toReturn appendFormat:@"<th data-type=\"number\">Total</th>"];
        [toReturn appendFormat:@"</tr>"];

        while (*toAnalyzedClass)
        {
            Class analyzedClass = *toAnalyzedClass;

            int allocationCount = GSDebugAllocationCount(analyzedClass);
            int allocationPeak  = GSDebugAllocationPeak(analyzedClass);
            int allocationTotal = GSDebugAllocationTotal(analyzedClass);

            [toReturn appendFormat:@"<tr>"];

            [toReturn appendFormat:@"<td>%@</td>", NSStringFromClass(analyzedClass)];
            [toReturn appendFormat:@"<td>%d</td>", allocationCount];
            [toReturn appendFormat:@"<td>%d</td>", allocationPeak];
            [toReturn appendFormat:@"<td>%d</td>", allocationTotal];

            [toReturn appendFormat:@"</tr>"];

            toAnalyzedClass++;
        }
        [toReturn appendFormat:@"</table>"];
        [toReturn appendFormat:@"</body>"];
        [toReturn appendFormat:@"</html>"];

        GTKHTTPServerResponse* response = AUTORELEASE([[GTKHTTPServerResponse alloc] init]);

        [response setMIMEType:[MimeType html]];
        [response setRawData:[toReturn dataUsingEncoding:NSUTF8StringEncoding]];

        return response;
    #else
    return @"Only available on GNUstep systems.";
    #endif
}

@end

