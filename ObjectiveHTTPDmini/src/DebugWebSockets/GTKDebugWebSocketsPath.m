//
//  GTKDebugWebSocketsPath.m
//  ObjectiveHTTPDmini
//
//  Created by Gustavo Tavares on 5/11/21.
//  Copyright © 2021 Gustavo Tavares. All rights reserved.
//

#import <ObjectiveHTTPDmini/GTKDebugWebSocketsPath.h>
#import <ObjectiveHTTPDmini/GTKHTTPServer.h>
#import <ObjectiveHTTPDmini/GTKMHDWebSocketConnection.h>
#import <ObjectiveHTTPDmini/GTKMHDHTTPRequest.h>

@implementation GTKDebugWebSocketsPath
{
    GTKHTTPServer* _server;
}

S_ACCESSOR(GTKHTTPServer*, _server, server, setServer);

-(id)
serveRequest:(GTKHTTPRequest*)theRequest
{
    DEBUG_NO
    
    if (!_server)
    {
        return @"<html><body><p>No server for GTKDebugWebSocketsPath.</p></body></html>";
    }
    
    NSMutableArray* webSocketsToDebug = [_server webSocketsToDebug];
    
    if (!webSocketsToDebug || ([webSocketsToDebug count] == 0))
    {
        
        return FMT(@"<html><body><p>No web sockets to debug. <br> Is nil? %@. <br>Should Debug Web Socket? %@ </body></html>", webSocketsToDebug ? @"YES" : @"NO", [_server debugWebSockets] ? @"YES" : @"NO");
    }
    
    NSMutableString* s = [NSMutableString string];
    
    [s appendFormat:@"<html><body>"];
    
    [s appendFormat:@"<h1>Web Socket Connections</h1>"];
    
    [s appendFormat:@"<table>"];
    
    [s appendFormat:@"<tr>"];
    [s appendFormat:@"<th>User</th>"];
    [s appendFormat:@"<th>Frame Count</th>"];
    [s appendFormat:@"</tr>"];
    
    [webSocketsToDebug enumerateObjectsUsingBlock:^(GTKMHDWebSocketConnection* connection, NSUInteger idx, BOOL* stop){
        [s appendFormat:@"<tr>"];
        
        NSString* path = FMT(@"/debug/webSocket?atIndex=%lu", idx);
        
        [s appendFormat:@"<td>"];
        [s appendFormat:@"<a href=\"%@\">%@</a>", path, [[connection user] description] ?: @"(null)" ];
        [s appendFormat:@"</td>"];
        
        [s appendFormat:@"<td>"];
        [s appendFormat:@"<a href=\"%@\">%lu</a>", path, [[connection retainedFrames] count]];
        [s appendFormat:@"</td>"];
        
        [s appendFormat:@"</tr>"];
    }];
    
    [s appendFormat:@"</table>"];
    
    [s appendFormat:@"</body></html>"];
    
    GTKHTTPServerResponse* response = [GTKHTTPServerResponse htmlResponseFromString:s];
    
    return response;
}

@end

//@interface NSMutableString (EasyHTML)
//-(void)
//forElement:(NSString*)theElement
//appendFormat:(NSString*)format, ...;
//
//@end
//
//@implementation NSMutableString (EasyHTML)
//-(void)
//forElement:(NSString*)theElement
//appendFormat:(NSString*)format, ...
//{
//    // https://kubasejdak.com/variadic-functions-va-args
//    va_list args;
//    va_start(args, format);
//
//    va_end(args);
//
//    [self appendFormat:format, ]
//
//
//}
//
//@end

@interface GTKWebSocketConnectionDataProcessor (DebugWebSocketsPath)

ACCESSOR_H(BOOL, shouldRetainData, setShouldRetainData);
-(NSMutableData*)retainedData;

@end

@interface GTKMHDWebSocketConnection (DebugWebSocketsPath)

ACCESSOR_H(GTKWebSocketConnectionDataProcessor*, webSocketDataProcessor, setWebSocketDataProcessor);

@end

@implementation GTKDebugOneWebSocketPath
{
    GTKHTTPServer* _server;
}

S_ACCESSOR(GTKHTTPServer*, _server, server, setServer);

-(NSString*)
rowStyleWithBackgroundColor:(NSString*)theColor
{
    if ([theColor hasPrefix:@"#"])
    {
        
    }
    else
    {
        
    }
    
    NSMutableString* style = [NSMutableString string];
    
    // #A9A9A9;
    
    [style appendFormat:@"padding:8px;"];
    [style appendFormat:@"background-color: %@;", theColor];
    [style appendFormat:@"overflow: scroll;"];
    
    return style;
}


-(void)
onString:(NSMutableString*)s
renderRowForWebSocketFrame:(GTKWebSocketFrame*)obj
{
    id payload = [obj payload];
    
    NSData* data = nil;
    
    if ([payload isKindOfClass:[GTKTrackingMutableData class]])
    {
        data = [payload data];
    }
    else if ([payload isKindOfClass:[NSData class]])
    {
        data = payload;
    }
    
    [s appendFormat:@"<tr>"];
    
    [s appendFormat:@"<td> %@    </td>", NSStringFromClass([obj class])];
    [s appendFormat:@"<td> %@    </td>", NSStringForWebSocketFrameU8Int([obj type])];
    [s appendFormat:@"<td> %@    </td>", [obj isLastFrame] ? @"YES" : @"NO" ];
    [s appendFormat:@"<td> %@    </td>", [obj isComplete] ? @"YES" : @"NO" ];
    [s appendFormat:@"<td> %hhu  </td>", [obj reserved]];
    [s appendFormat:@"<td> %lu   </td>", [obj payloadSize]];
    [s appendFormat:@"<td> %@    </td>", NSStringFromClass([payload class])];
    
    [s appendFormat:@"</tr>"];
    

    
    NSError* internalError = nil;
    
    //*---

    NSString* payloadAsHex
        = [data asHexString];
        
    [s appendFormat:@"<tr>"];
    [s appendFormat:@"<td colspan=\"7\" style=\"%@\">", [self rowStyleWithBackgroundColor:@"#A9A9A9"]];
    [s appendString:@"<div style=\"width:100%;overflow:scroll;\">"];
    [s appendFormat:@"%@", payloadAsHex];
    [s appendFormat:@"</div>"];
    [s appendFormat:@"</td>"];
    [s appendFormat:@"</tr>"];
    
    //*---
    
    NSString* payloadAsString
        = [[NSString alloc]
            initWithData:data
            encoding:NSUTF8StringEncoding];
            
    [s appendFormat:@"<tr>"];
    [s appendFormat:@"<td colspan=\"7\" style=\"%@\">", [self rowStyleWithBackgroundColor:@"#E9E9E9"]];
    [s appendFormat:@"<div style=\"width:100%;overflow:scroll;\">"];
    [s appendFormat:@"%@", payloadAsString];
    [s appendFormat:@"</div>"];
    [s appendFormat:@"</td>"];
    [s appendFormat:@"</tr>"];
            
    
    if (![obj isControlFrame] && [data length])
    {
        GTKWebSocketRequest* request
            = [GTKWebSocketRequest
                requestFromData:data
                error:&internalError];
                
        
    
        [s appendFormat:@"<tr>"];
        [s appendFormat:@"<td colspan=\"7\" style=\"%@\">", [self rowStyleWithBackgroundColor:@"#C9C9C9"]];
        [s appendString:@"<div style=\"width:800px;overflow:scroll;\">"];
        

        if (internalError)
        {
            [s appendFormat:@"Error: %@", [internalError prettyPrintString]];
        }
        else
        {
            [s appendFormat:@"%@", [request description]];
        }
        
        [s appendFormat:@"</div>"];
        [s appendFormat:@"</td>"];
        [s appendFormat:@"</tr>"];
    }
}


-(id)
serveRequest:(GTKHTTPRequest*)theRequest
{
    DEBUG_NO
    NSString* atIndexStr  = [theRequest queryParameterForKey:@"atIndex"];
    NSNumber* atIndexNum  = [atIndexStr asNumber];
    
    if (!atIndexNum)
    {
        NSMutableString* s = [NSMutableString string];
        
        [s appendFormat:@"<html><body>"];
        
        [s appendFormat:@"Wasn't able to get a number. Got string: %@", atIndexStr];
        
        [s appendFormat:@"</body></html>"];
        
    }
    
    if (!_server)
    {
        return @"<html><body><p>No server for GTKDebugWebSocketsPath.</p></body></html>";
    }
    
    NSMutableArray* webSocketsToDebug = [_server webSocketsToDebug];
    
    if (!webSocketsToDebug || ([webSocketsToDebug count] == 0))
    {
        
        return FMT(@"<html><body><p>No web sockets to debug. <br> Is nil? %@. <br>Should Debug Web Socket? %@ </body></html>", webSocketsToDebug ? @"YES" : @"NO", [_server debugWebSockets] ? @"YES" : @"NO");
    }
    
    NSUInteger* atIndex = [atIndexNum unsignedIntegerValue];
    
    GTKMHDWebSocketConnection* webSocketConnection = [webSocketsToDebug objectAtIndex:atIndex];
    
    NSMutableString* s = [NSMutableString string];
    
    [s appendFormat:@"<html><body>"];
    
    [s appendFormat:@"<h3>Web Socket Connection</h1>"];
    [s appendFormat:@"%@", webSocketConnection];
    
    [s appendFormat:@"<h3>Frames</h3>"];
    [s appendFormat:@"<table style=\"table-layout:fixed;\">"];
    
    NSMutableArray* retainedFrames = [webSocketConnection retainedFrames];
    
    [s appendFormat:@"<tr>"];
    [s appendFormat:@"<th>Class</th>"];
    [s appendFormat:@"<th>Type</th>"];
    [s appendFormat:@"<th>Is Last Frame</th>"];
    [s appendFormat:@"<th>Is Complete</th>"];
    [s appendFormat:@"<th>Reserved</th>"];
    [s appendFormat:@"<th>Payload Size</th>"];
    [s appendFormat:@"<th>Payload (row below)</th>"];
    [s appendFormat:@"</tr>"];

    [retainedFrames enumerateObjectsUsingBlock:^(GTKWebSocketFrame* obj, NSUInteger idx, BOOL* stop){
        [self onString:s renderRowForWebSocketFrame:obj];
    }];
    
    [s appendFormat:@"</table>"];
    
    [s appendFormat:@"<h3>Messages</h3>"];
    
    NSMutableArray* retainedMessages = [webSocketConnection retainedMessages];
    
    [s appendFormat:@"<table style=\"table-layout:fixed;\">"];
    
    [retainedMessages enumerateObjectsUsingBlock:^(GTKWebSocketMessage* obj, NSUInteger idx, BOOL* stop){
        [self onString:s renderRowForWebSocketFrame:obj];
    }];
    
    [s appendFormat:@"</table>"];
    
    
    GTKWebSocketConnectionDataProcessor* webSocketDataProcessor
        = [webSocketConnection webSocketDataProcessor];
        
    NSData*   allData             = [webSocketDataProcessor retainedData];
    NSString* allDataAsHexString  = [allData asHexString];
    NSString* allDataAsUTF8String = [[NSString alloc] initWithData:allData encoding:NSUTF8StringEncoding];
    
    [s appendFormat:@"<h3>All Data</h3>"];
    
    
    NSArray*  arrayWith80CharLines     = [allDataAsHexString arrayWithStringOfMaxLength:80];
    NSString* hexStringWith80CharLines = [arrayWith80CharLines componentsJoinedByString:@"<br/>"];
    
    
    [s appendFormat:@"<h5>Hex</h5>"];
    [s appendFormat:@"<p>"];
    [s appendFormat:@"%@", hexStringWith80CharLines];
    [s appendFormat:@"</p>"];
    
    [s appendFormat:@"<br/>"];
    [s appendFormat:@"<br/>"];
    [s appendFormat:@"<br/>"];
    
    [s appendFormat:@"<h5>UTF8</h5>"];
    [s appendFormat:@"<p>"];
    [s appendFormat:@"%@", allDataAsUTF8String];
    [s appendFormat:@"</p>"];
    
    
    
    [s appendFormat:@"</body></html>"];
    
    
    GTKHTTPServerResponse* response = [GTKHTTPServerResponse htmlResponseFromString:s];

    return response;
}



@end
