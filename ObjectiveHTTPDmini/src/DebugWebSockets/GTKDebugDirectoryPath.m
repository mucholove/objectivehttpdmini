//
//  GTKDebugDirectoryPath.m
//  ObjectiveHTTPDmini
//
//  Created by Gustavo Tavares on 6/4/21.
//  Copyright © 2021 Gustavo Tavares. All rights reserved.
//

#import <ObjectiveHTTPDmini/GTKDebugDirectoryPath.h>

@implementation GTKDebugDirectoryPath
-(id)
serveRequest:(id)theRequest
{
    NSArray* paths = @[
        @"/debug/alloc",
        @"/debug/alloc/history",
        //---------------------
        @"/debug/webSocket",
        @"/debugWebSockets",
        //-------------
        @"/debug/log",
        @"/debug/exceptions",
    ];

    NSMutableString* s = [NSMutableString string];

    [s appendFormat:@"<html><head></head><body>"];

    [s appendFormat:@"<h1>Paths</h1>"];

    for (NSString* path in paths)
    {
        [s appendFormat:@"<a href=\"%@\">%@</a>", path, path];
    }

    [s appendFormat:@"</body></html>"];

    return s;

}
@end
