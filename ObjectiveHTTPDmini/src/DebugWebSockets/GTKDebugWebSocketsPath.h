//
//  GTKDebugWebSocketsPath.h
//  ObjectiveHTTPDmini
//
//  Created by Gustavo Tavares on 5/11/21.
//  Copyright © 2021 Gustavo Tavares. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GTKFoundation/GTKFoundation.h>
#import <ObjectiveHTTPDmini/GTKServerPathProtocols.h>



@class
    GTKMHDWebSocketConnection,
    GTKHTTPServer;

@interface GTKDebugWebSocketsPath : NSObject <IsGTKHTTPPathForClassDictionary>
ACCESSOR_H(GTKHTTPServer*, server, setServer);
@end

@interface GTKDebugOneWebSocketPath : NSObject <IsGTKHTTPPathForClassDictionary>
ACCESSOR_H(GTKMHDWebSocketConnection*, webSocketConnection, setWebSocketConnection);
@end


