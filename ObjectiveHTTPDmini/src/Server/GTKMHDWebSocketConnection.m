//
//  GTKMHDWebSocketConnection.m
//  ObjectiveHTTPDmini
//
//  Created by Gustavo Tavares on 3/12/21.
//  Copyright © 2021 Gustavo Tavares. All rights reserved.
//

#import <ObjectiveHTTPDmini/GTKMHDWebSocketConnection.h>
#import <ObjectiveHTTPDmini/GTKMHDHTTPRequest.h>
#import <ObjectiveHTTPDmini/GTKHTTPServer.h>
#import <GTKSockets/GTKSockets.h>
#import <microhttpd.h>

#pragma mark - Declaraction

@implementation GTKSocketManager (MHDWebSocketConnectionAdditions)
-(void)
addWebSocketConnection:(GTKMHDWebSocketConnection*)theWebSocketConnection
{
    DEBUG_NO

    MHD_socket socket = [theWebSocketConnection socket];

    DebugLog(@"Will add webSocketConnection: %@ for FileDescriptor: %d", theWebSocketConnection, socket);

    [self setObject:theWebSocketConnection
          forFileDescriptor:(int)socket];

    DebugLog(@"Did add webSocketConnection: %@ for FileDescriptor: %d", theWebSocketConnection, socket);
}

-(void)
removeWebSocketConnection:(GTKMHDWebSocketConnection*)theWebSocketConnection
{
    DEBUG_NO
    MHD_socket socket = [theWebSocketConnection socket];
    [self removeObjectForFileDescriptor:(int)socket];
}
@end

@interface GTKWebSocketConnectionDataProcessor (DebugDebug)
ACCESSOR_H(BOOL, shouldRetainData, setShouldRetainData);
@end



@implementation GTKMHDWebSocketConnection
{
    id<GTKWebSocketConnectionDelegate>             _delegate;
    GTKWebSocketConnectionDataProcessor*           _webSocketDataProcessor;
    // wslay_event_context_ptr                      _wslayCtx;
    GTKSocketManager*                              _manySocketManager;
    NSUUID*                                        _uuid;
    GTKHTTPRequest*                                _request;
    struct MHD_Connection*                         _connection;
    NSData*                                        _extraInData;
    MHD_socket                                     _socket;
    struct MHD_UpgradeResponseHandle*              _upgradeResponseHandle;
    BOOL                                           _isOpen;
    NSMutableArray*                                _messagesToSend;
    BOOL                                           _sentCloseMessage;
    BOOL                                           _recvdCloseMessage;
    BOOL                                           _didRemoveFromSocketManager;
    NSTimer*                                       _timeoutTimer;
    BOOL                                           _didDestroySocket;
    NSDate*                                        _lastPong;
    NSTimeInterval                                 _timeIntervalToTimeout;
    BOOL                                           _shouldRetainFrames;
    NSMutableArray*                                _retainedFrames;
    NSMutableArray*                                _retainedData;
    NSMutableArray*                                _retainedMessages;
    
    NSDate* _timeSinceLastPing;
    NSDate* _timeSinceLastPong;
    
}


M_ACCESSOR(NSDate*, _timeSinceLastPing, timeSinceLastPing, setTimeSinceLastPing);
M_ACCESSOR(NSDate*, _timeSinceLastPong, timeSinceLastPong, setTimeSinceLastPong);

#pragma mark Weak References

S_ACCESSOR(BOOL,                                           _didRemoveFromSocketManager, didRemoveFromSocketManager, setDidRemoveFromSocketManager);
S_ACCESSOR(GTKSocketManager*,                    _manySocketManager,     manySocketManager,     setManySocketManager);
S_ACCESSOR(id<GTKWebSocketConnectionDelegate>,   _delegate,              delegate,              setDelegate);

#pragma mark Strong References

M_ACCESSOR(GTKHTTPRequest*,                      _request,               request,               setRequest);
M_ACCESSOR(NSData*,                              _extraInData,           extraInData,           setExtraInData);

#pragma mark Debug

M_ACCESSOR(NSMutableArray*,                      _retainedData,          retainedData,          setRetainedData);
M_ACCESSOR(NSMutableArray*,                      _retainedFrames,        retainedFrames,        setRetainedFrames);
M_ACCESSOR(NSMutableArray*,                      _retainedFrames,        retainedMessages,      setRetainedMessages);
M_ACCESSOR(GTKWebSocketConnectionDataProcessor*, _webSocketDataProcessor, webSocketDataProcessor, setWebSocketDataProcessor);

-(BOOL)
shouldRetainFrames
{
    return _shouldRetainFrames;
}
-(void)
setShouldRetainFrames:(BOOL)theBool
{
    _shouldRetainFrames = theBool;
    [_webSocketDataProcessor setShouldRetainData:theBool];
}

-(id)
session
{
    id toReturn = [_request session];
    
    return toReturn;
}

-(NSString*)
sessionID
{
    id toReturn = [[_request session] identifier];
    
    return toReturn;
}

#pragma mark MHD references

// S_ACCESSOR(struct MHD_Connection*,               _connection,            connection,            setConnection);
S_ACCESSOR(void*,                                   _connection,            connection,            setConnection);
S_ACCESSOR(MHD_socket,                              _socket,                socket,                setSocket);
// S_ACCESSOR(struct MHD_UpgradeResponseHandle*,    _upgradeResponseHandle, upgradeResponseHandle, setUpgradeResponseHandle);
S_ACCESSOR(void*,                                   _upgradeResponseHandle, upgradeResponseHandle, setUpgradeResponseHandle);
S_ACCESSOR(BOOL,                                    _isOpen,                isOpen,                setIsOpen);



static NSData* _g_pongMessageData;
static NSData* _g_pingMessageData;

+(void)
load
{
    [super load];
    _g_pingMessageData = RETAIN([[GTKWebSocketFrame pingFrame] asData]);
    _g_pongMessageData = RETAIN([[GTKWebSocketFrame pongFrame] asData]);
}

+(NSData*)
pongMessageData
{
    return _g_pongMessageData;
}

+(NSData*)
pingMessageData
{
    return _g_pingMessageData;
}

-(void)
dealloc
{
    
    [_timeoutTimer invalidate];
    _timeoutTimer = nil;
    
    #if !HAS_ARC
    RELEASE(_messagesToSend);
    RELEASE(_timeoutTimer);
    RELEASE(_extraInData);
    RELEASE(_webSocketDataProcessor);
    RELEASE(_uuid);
    [super dealloc];
    #endif
}

//-(instancetype)
//initWithRequest:(GTKHTTPRequest*)theRequest
//MHDConnection:(struct MHD_Connection*)theConnection
//socket:(MHD_socket)theSocket
//responseHandle:(struct MHD_UpgradeResponseHandle*)theUpgradeResponseHandle

-(instancetype)
initWithRequest:(GTKHTTPRequest*)theRequest
MHDConnection:(void*)theConnection
socket:(MHD_socket)theSocket
responseHandle:(void*)theUpgradeResponseHandle
{
    self = [self init];

    if (self)
    {
        [self setRequest:theRequest];
        [self setConnection:theConnection];
        [self setSocket:theSocket];
        [self setUpgradeResponseHandle:theUpgradeResponseHandle];
    }

    return self;
}

-(instancetype)
init
{
    self = [super init];
    if (self)
    {
        ASSIGN_ID_gtk(_uuid,      [NSUUID UUID]);
        ASSIGN_ID_gtk(_lastPong,  [NSDate new]);
        _timeIntervalToTimeout = 30.0;
    }
    return self;
}


-(void)
sendMessage:(id)theMessage
{
    DEBUG_NO
    
    GTKWebSocketMessageWriter* writer = nil;

    if ([theMessage isKindOfClass:[GTKWebSocketMessageWriter class]])
    {
        DebugLog(@"Message is GTKWebSocketMessageWriter. No need to create writer.");
        writer = theMessage;
    }
    else
    {
        DebugLog(@"Message is kind of class: %@ - %@", NSStringFromClass([theMessage class]), theMessage);
        
        writer = AUTORELEASE([[GTKWebSocketMessageWriter alloc] init]);

        if ([theMessage isKindOfClass:[NSString class]])
        {
            [writer setPayload:theMessage asString:YES];
        }
        else
        {
            [writer setPayload:theMessage asString:NO];
        }
    }

    DebugLog(@"Will convert message to NSData.");

    NSData* theData = [writer asData];
    
    DebugLog(@"Did convert message to NSData.");
    
    DebugLog(@"Will send data.");

    [self sendData:theData];
    
    DebugLog(@"Did finish send message");

}

-(void)
sendData:(NSData*)theDataToSend
{
    [self sendData:(NSData*)theDataToSend
          error:nil];
}

-(void)
sendSocketQueueItem:(GTKSocketQueueItem*)theSocketQueueItem
error:(NSError**)outErr
{
    DEBUG_NO

    [_manySocketManager
        sendSocketQueueItem:theSocketQueueItem
        forFileDescriptor:_socket
        error:outErr];
}

-(void)
sendData:(NSData*)theDataToSend
error:(NSError**)outErr
{
    DEBUG_NO
    
    DebugLog(@"Will send data via many socket manager: %@", _manySocketManager);
    DebugLog(@"For file descriptor: %d", _socket);
    
    if ([theDataToSend isEqual:[NSData fromHexStringGTK:@"2000"]])
    {
        NSException* e = [NSException
            exceptionWithName:@"GTKDebugHelperInvocation"
            reason:@"Making it easy on me."
            userInfo:nil];
            
        @throw e;
    }
    
    [_manySocketManager sendData:theDataToSend
                        forFileDescriptor:_socket
                        error:outErr];
                        
    DebugLog(@"Did send data.");

    /*
    @synchronized (_messagesToSend)
    {
        if (!_messagesToSend)
        {
            ASSIGN_ID_gtk(_messagesToSend, [NSMutableArray array]);
        }
        [_messagesToSend addObject:theDataToSend];
    }
    */

    /*
    struct wslay_event_msg wsMessage = {
        .opcode     = WSLAY_BINARY_FRAME,
        .msg        = [theDataToSend bytes],
        .msg_length = [theDataToSend length]
    };
    */

    // wslay_event_queue_fragmented_msg() function instead.

    /*
     |Opcode  | Meaning                             | Reference |
    -+--------+-------------------------------------+-----------|
     | 0      | Continuation Frame                  | RFC 6455  |
    -+--------+-------------------------------------+-----------|
     | 1      | Text Frame                          | RFC 6455  |
    -+--------+-------------------------------------+-----------|
     | 2      | Binary Frame                        | RFC 6455  |
    -+--------+-------------------------------------+-----------|
     | 8      | Connection Close Frame              | RFC 6455  |
    -+--------+-------------------------------------+-----------|
     | 9      | Ping Frame                          | RFC 6455  |
    -+--------+-------------------------------------+-----------|
     | 10     | Pong Frame                          | RFC 6455  |
    -+--------+-------------------------------------+-----------|
     | For more see: https://tools.ietf.org/html/rfc6455#section-11.8
     */

//    wsMessage->opcode = WSLAY_BINARY_FRAME; // 2;
//    wsMessage->msg    = [theDataToSend bytes];
//    wsMessage->length = [theDataToSend length];
//

    /*
    int didNotSucceed = 0;

    didNotSucceed
        =  wslay_event_queue_msg(
                _wslayCtx,
                &wsMessage);

    if (didNotSucceed)
    {
        switch (didNotSucceed)
        {
            case WSLAY_ERR_NO_MORE_MSG:
                AlwaysLog(@"Could not queue given message. The one of possible reason is that close control frame has been queued/sent and no further queueing message is not allowed.");
            case WSLAY_ERR_INVALID_ARGUMENT:
                AlwaysLog(@"The given message is invalid; or RSV1 is set for control frame; or bit is set in rsv which is not allowed (see wslay_event_config_set_allowed_rsv_bits()).");
            case WSLAY_ERR_NOMEM:
                AlwaysLog(@"Out of memory.");
        }
    }
    */

    /*
        Other options:

                wslay_event_queue_msg_ex() accepts rsv parameter: reserved bits to send.
                    To set reserved bits, use macros
                        WSLAY_RSV1_BIT
                        WSLAY_RSV2_BIT
                        WSLAY_RSV3_BIT

                    See wslay_event_config_set_allowed_rsv_bits() to see the allowed reserved bits to set.

                Fragmented Send:
                    union wslay_event_msg_source {
                        int   fd;
                        void *data;
                    };

                    struct wslay_event_fragmented_msg {
                        uint8_t                             opcode;
                        union wslay_event_msg_source        source;
                        wslay_event_fragmented_msg_callback read_callback;
                    };
     */
    // wslay_event_want_write(_wslayCtx);
}



-(id)
user
{
    DEBUG_NO
    
    id request = [self request];
    
    DebugLog(@"Request: %@", request);
    
    id user = [request user];
    
    DebugLog(@"User: %@", user);
    
    return user;
}



-(NSUUID*)
UUID
{
    if (!_uuid)
    {
        ASSIGN_ID_gtk(_uuid, [NSUUID UUID]);
    }
    return _uuid;
}

-(NSUInteger)
hash
{
    return [[self UUID] hash];
}
 // UNUSED
// static uint8_t getFinFromBytes(char* theBytes)
// {
//     return (theBytes[0] >> 7) & 1;
// }
//
// static uint8_t getRSVFromBytes(char* theBytes)
// {
//     return (theBytes[0] >> 4) & 7;
// }
//
// static uint8_t getOpCodeFromBytes(char* theBytes)
// {
//     return theBytes[0] & 0xfu;
// }

#pragma mark - Handle Web Socket Specific Connections

-(void)
close
{
    [self closeConnection];
}

-(void)
closeConnection // Sends the WebSocket Close Connection
{
    DEBUG_NO
    
    if ([_delegate respondsToSelector:@selector(willCloseWebSocketConnection:)])
    {
        [_delegate willCloseWebSocketConnection:self];
    }

    GTKWebSocketMessageWriter* writer
        = AUTORELEASE([[GTKWebSocketMessageWriter alloc] init]);

    [writer setType:WebSocketFrameClose];

    [self sendMessage:writer];
    
    DebugLog(@"Will make timer.");

    NSTimer* timer
        = [NSTimer scheduledTimerWithTimeInterval:10
                   target:self
                   selector:@selector(destroySocket)
                   userInfo:nil
                   repeats:NO];
        
    DebugLog(@"Will assign timer.");

    ASSIGN_ID_gtk(_timeoutTimer, timer);
}

-(void)
freeFromSocketManager
{
    if (!_didRemoveFromSocketManager)
    {
        _didRemoveFromSocketManager = YES;
        [_manySocketManager deleteSocketDescriptor:_socket];
    }

}

-(BOOL)
didTimeoutAtDate:(NSDate*)theDate
{
    return [self didTimeoutAtTimeInterval:[theDate timeIntervalSinceReferenceDate]];
}


-(BOOL)
didTimeoutAtTimeInterval:(NSTimeInterval)theTimeInterval
{
    DEBUG_NO

    DebugLog(@"Will get time interval for last pong: %@", _lastPong);
    NSTimeInterval lastPongTimeInterval     = [_lastPong timeIntervalSinceReferenceDate];
    DebugLog(@"Did get time interval for last pong: %@", _lastPong);

    NSTimeInterval timeIntervalBetweenDates = (lastPongTimeInterval - theTimeInterval);

    if (timeIntervalBetweenDates > _timeIntervalToTimeout)
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

-(void)
destroySocket
{
    @synchronized (self) {
        if (!_didDestroySocket)
        {
            if ([_delegate respondsToSelector:@selector(didCloseWebSocketConnection:)])
            {
                [_delegate didCloseWebSocketConnection:self];
            }
            [self freeFromSocketManager];
            _didDestroySocket = YES;
            MHD_upgrade_action(self->_upgradeResponseHandle, MHD_UPGRADE_ACTION_CLOSE);
        }
    }

}


-(void)
closedByManagerBecauseSocketStoppedWorking
{
    DEBUG_NO
    
    _didRemoveFromSocketManager = YES;
    [self destroySocket];
}

-(void)
handleError:(NSError*)error
{
    if ([_delegate respondsToSelector:@selector(webSocketConnection:gotError:)])
    {
        [_delegate webSocketConnection:self gotError:error];
    }

    [self closeConnection];
}

-(void)
sendPong
{
    DEBUG_NO
    
    DebugLog(@"Will send pong (length=%lu): Hex: %@", [_g_pongMessageData length], [_g_pongMessageData asHexString]);
    
    GTKSocketQueueItem* socketQueueItem
        = [GTKSocketQueueItem item];
    
    [socketQueueItem setFileDescriptor:_socket];
    [socketQueueItem setOrigin:@selector(sendPong)];
    [socketQueueItem setData:_g_pongMessageData];
    
    [self sendSocketQueueItem:socketQueueItem
          error:nil];
}

#pragma mark - Process Data

-(void)
processData:(NSData*)theData
{
    [self processData:theData error:nil];
}

-(void)
processData:(NSData*)theData
error:(NSError**)outErr
{
    DEBUG_NO
    @synchronized (self)
    {
        if (_didDestroySocket)
        {
            return;
        }
        
        if (!_webSocketDataProcessor)
        {
            _webSocketDataProcessor = [[GTKWebSocketConnectionDataProcessor alloc] init];
            [_webSocketDataProcessor setDelegate:self];
            if (_shouldRetainFrames)
            {
                [_webSocketDataProcessor setShouldRetainData:_shouldRetainFrames];
            }
        }

        #if !HAS_ARC
        BOOL useAutoreleasePool = NO;
        
        NSAutoreleasePool* pool = nil;
        
        if (useAutoreleasePool)
        {
            pool = [[NSAutoreleasePool alloc] init];
        }
        #endif
        
        [_webSocketDataProcessor
            processData:theData
            error:outErr];
            
        #if !HAS_ARC
        [pool drain];
        #endif
        
    }
}

-(void)
webSocketDataProcessor:(GTKWebSocketConnectionDataProcessor*)theWebSocketConnection
didCompleteFrame:(GTKWebSocketFrame*)theWebSocketFrame
{
    DEBUG_NO
    DebugLog(@"Web Socket Processor did complete frame: %@", [theWebSocketFrame prettyPrintString]);
    
    if (_shouldRetainFrames) // Used for debug
    {
        if (!_retainedFrames)
        {
            _retainedFrames = [[NSMutableArray alloc] init];
        }
        [_retainedFrames addObject:theWebSocketFrame];
    }
}

//-(void)
//webSocketDataProcessor:(GTKWebSocketConnectionDataProcessor*)theWebSocketConnection
//didProcessData:(NSData*)theData
//frame:(GTKWebSocketFrame*)theWebSocketFrame
//message:(GTKWebSocketMessage*)theMessage
//{
//    DEBUG_NO
//    DebugLog(@"Web Socket Processor will process data.");
//
//    if (![theWebSocketFrame isControlFrame])
//    {
//        [_]
//    }
//    DebugLog(@"Web Socket Processor did process data.");
//}


#pragma mark - Data Processor Delegate

-(void)
webSocketDataProcessor:(GTKWebSocketConnectionDataProcessor*)theWebSocketConnection
didCompleteMessage:(GTKWebSocketMessage*)theMessage
{
    DEBUG_NO

    WebSocketFrameType activeMessageType = [theMessage type];
    
    if (_shouldRetainFrames) // Used for debug
    {
        if (!_retainedMessages)
        {
            _retainedMessages = [[NSMutableArray alloc] init];
        }
        [_retainedMessages addObject:theMessage];
    }
    
    DebugLog(@"Recieved message of type: %@", NSStringForWebSocketFrameU8Int(activeMessageType));

    if (activeMessageType == WebSocketFrameClose)
    {
        if (_sentCloseMessage)
        {
            [self destroySocket];
        }
        else
        {
            _recvdCloseMessage = YES;
            // [self sendCloseMessage];
            [self destroySocket];
        }

        // [self sendCloseMessage]
        /////////[self closeConnection];
        return;
    }

    BOOL canRecvMessage = !_sentCloseMessage && !_recvdCloseMessage;

    DebugLog(@"%@ - Can Recv Message: %d", self, canRecvMessage);

    if (canRecvMessage)
    {
        if (activeMessageType == WebSocketFramePong)
        {
            DebugLog(@"Finishing `PONG` message.");
            ASSIGN_ID_gtk(_lastPong, [NSDate new]);
        }
        else if (activeMessageType == WebSocketFramePing)
        {
            DebugLog(@"Finishing `PING` type.");
            [self sendPong];
        }
        else
        {
            DebugLog(@"Message: %@", [CAST(id)theMessage prettyPrintString]);
        }

        if ([_delegate respondsToSelector:@selector(webSocketConnection:didReceiveMessage:)])
        {
            [_delegate webSocketConnection:self didReceiveMessage:theMessage];
        }
    }
}

-(void)
webSocketDataProcessor:(GTKWebSocketConnectionDataProcessor*)processor
gotError:(NSError*)error
{
    [self handleError:error];
}



#pragma mark - The C Machine

@end
