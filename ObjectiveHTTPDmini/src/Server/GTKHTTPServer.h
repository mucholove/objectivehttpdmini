//
//  GTKHTTPServer.h
//  ObjectiveHTTPD
//
//  Created by Gustavo Tavares on 3/20/20.
//

#import <Foundation/Foundation.h>


#import <GTKFoundation/GTKFoundation.h>
#import <ObjectiveHTTPDmini/GTKServerPathProtocols.h>
#import <ObjectiveHTTPDmini/GTKHTTPServerDelegate.h>
#import <ObjectiveHTTPDmini/GTKServerAuthDelegate.h>
#import <ObjectiveHTTPDmini/HTTPServerProtocol.h>
#import <ObjectiveHTTPDmini/GTKHTTPServerResponse.h>

@class
    GTKHTTPRequest;

@interface GTKMHDUpgradeToWebSocketResponse : GTKHTTPServerResponse
@end

@interface GTKHTTPCapturedException : NSObject
ACCESSOR_H(GTKHTTPExceptionLocation,        exceptionLocation, setExceptionLocation);
ACCESSOR_H(GTKHTTPRequest*,                 request,           setRequest);
ACCESSOR_H(GTKHTTPServerResponse*,          response,          setResponse);
ACCESSOR_H(NSException*,                    exception,         setException);

// ACCESSOR_H(NSString*,    path,      setPath);
// ACCESSOR_H(NSString*,    user,      setUser);
// ACCESSOR_H(NSString*,    session,   setSession);
@end

@interface GTKHTTPServer : NSObject<
     HTTPServerProtocol
    ,GTKHTTPServerDelegate
    ,GTKWebSocketConnectionDelegate
>

ACCESSOR_H(NSMutableArray*, capturedExceptions,    setCapturedExceptions);
ACCESSOR_H(BOOL,            debugWebSockets,       setDebugWebSockets);
ACCESSOR_H(BOOL,            retainWebSocketFrames, setRetainWebSocketFrames);

-(NSMutableArray*)webSocketsToDebug;

-(NSMutableArray*)
pathsToDebug;
-(void)
setPathsToDebug:(NSMutableArray*)pathsToDebug;

ACCESSOR_H(void*,                httpd,           setHttpd);
ACCESSOR_H(id,                   delegate,        setDelegate);
ACCESSOR_H(id,                   authDelegate,    setAuthDelegate);
ACCESSOR_H(int,                  threadPoolSize,  setThreadPoolSize);
ACCESSOR_H(NSData*,              keyFile,         setKeyFile);
ACCESSOR_H(NSData*,              certFile,        setCertFile);

-(void)
initLocalVars;

#pragma mark The Main Handler

-(int) // Called after request is `Stuffed` in Handle Connected Callback.
manageRequest:(GTKHTTPRequest*)aRequest
forPath:(NSString*)aPath
withData:(const char*)upload_data
withDataLength:(size_t*)upload_data_size
onConnection:(void*)connection // (struct MHD_Connection*)
withContext:(void**)con_cls;


#pragma mark Accumulator Method

-(int)
handleRequest:(GTKHTTPRequest*)aRequest
forPath:(NSString*)aPath
withData:(const char*)upload_data
withDataLength:(size_t*)upload_data_size
onConnection:(void*)connection // (struct MHD_Connection*)
withContext:(void**)con_cls;

#pragma mark Response to HTTP Object

-(int)
handleRequest:(GTKHTTPRequest*)aRequest
forPath:(NSString*)aPath
onConnection:(void*)connection // (struct MHD_Connection*)
withContext:(void**)con_cls;

#pragma mark Response Maker

-(id)
serveRequest:aRequest;

-(BOOL)
shouldDebugPath:(NSString*)path;

#pragma mark Socket MGMT

-(NSMutableArray*)
webSocketConnectionsForUser:(id)theUser;

-(BOOL)
sendData:(NSData*)theData
toUser:(id)theUser
error:(NSError**)outErr;

-(void)
toUser:(id)user
pushObject:(id)theObject
path:(NSString*)thePath
excludeSession:(id)theSession
asJSONSchema:(BOOL)asJsonSchema;

#pragma mark - Run Loop

-(void)
runInNSRunLoop;

-(void)
runInNSRunLoopInForeground:(BOOL)runInForeground;

-(void)
server:(GTKHTTPServer*)server
didServeRequest:(GTKHTTPRequest*)request
withResponse:(GTKHTTPServerResponse*)response
finishedCode:(GTKHTTPRequestFinishedCode)finishedCode;

@end



/*
int
    gtkAcceptPolicyCallback(
        void                   *cls,
        const struct sockaddr  *addr,
        socklen_t               addrlen);

void
    gtkRequestCompletedCallback(
       void *cls
       ,struct MHD_Connection * connection
       ,void **con_cls
       ,enum MHD_RequestTerminationCode terminationCode);


static int
    addKeyValuesToDictionary(
        void *cls
        ,enum MHD_ValueKind kind
        ,const char *key
        ,const char *value);

static int
    iterate_post (
        void *cls,
        enum MHD_ValueKind kind,
        const char *key,
        const char *filename,
        const char *content_type,
        const char *transfer_encoding,
        const char *bytes,
        uint64_t off,
        size_t size);

int
    HandleFailedAuthenticationOnConnection(
        struct MHD_Connection  *connection,
        const char             *url,
        const char             *user);




#pragma mark The Responder

int
    gtkAccessHandlerCallback(
        void*                   cls,
        struct MHD_Connection  *connection,
        const char             *url,
        const char             *method,
        const char             *version,
        const char             *upload_data,
        size_t                 *upload_data_size,
        void                  **con_cls);

int
    HandleUnconnectedCallback(
        void                   *cls,
        struct MHD_Connection  *connection,
        const char             *url,
        const char             *method,
        const char             *version,
        const char             *upload_data,
        size_t                 *upload_data_size,
        void                   **con_cls);

int
    HandleConnectedCallback(
        void                   *cls,
        struct MHD_Connection  *connection,
        const char             *url,
        const char             *method,
        const char             *version,
        const char             *upload_data,
        size_t                 *upload_data_size,
        void                   **con_cls);

*/

  /*
  SQLite allows a parameter wherever a string literal, numeric constant, or NULL is allowed. (Parameters may not be used for column or table names.) A parameter takes one of the following forms:

     ?
     ?NNN
     :AAA
     $AAA
     @AAA

     NNN is an integer value
     AAA is an identifier

     The application can invoke the sqlite3_bind() interfaces
         to attach values to the parameters:

     1 — Prior to calling sqlite3_step() for the first time or
     2 — Immediately after sqlite3_reset(),

     Each call to sqlite3_bind() overrides prior bindings on the same parameter.
   */

  /*

 1 — WRITING IMAGES...
 FILE *fp = fopen("woman.jpg", "rb");
 // ERROR HANDLING OMMITED — if (fp == NULL) { .... } ()
 fseek(fp, 0, SEEK_END);
 // ERROR HANDLING OMMITED — if (ferror(fp)) { .... } ()
 int flen = ftell(fp);
 // ERROR HANDLING OMMITED — if (flen == -1) { .... } ()
 fseek(fp, 0, SEEK_SET);
 // ERROR HANDLING OMMITED — if (ferror(fp)) { ... }
 char data[flen+1];
 int size = fread(data, 1, flen, fp);
 // ERROR HANDLING OMMITED — if (ferror(fp)) { ... }
 int r = fclose(fp);
 // ERROR HANDLING OMMITED — if (r == EOF) {

 2 — READING IMAGES...
 fwrite(sqlite3_column_blob(pStmt, 0), bytes, 1, fp);

 3 — Metadata in SQLite can be obtained using the PRAGMA command.

     - Column info //  returns one row for each column in the Cars table.
         char *sql = "PRAGMA table_info(Cars)";

     — All tables in DB...
         char *sql = "SELECT name FROM sqlite_master WHERE type='table'";

 sqlite3_bind_blob(

       pStmt
     , 1
     , data
     , size
     , SQLITE_STATIC);

  sqlite3_bind_text(
        select_users_stmt   // sqlite3_stmt_pointer
      , 1                  // index of sql parameter to be set — The index for named parameters can be looked up using the [sqlite3_bind_parameter_index()]
      , "Aladdin"          // value to bind
      , -1                 // length of string or -1 to search till end
      , nil);              //  destructor for string or blob

  sqlite3_bind_text(
        select_users_stmt
      , 2
      , sys_encoded_password
      , -1
      , nil);
   */

