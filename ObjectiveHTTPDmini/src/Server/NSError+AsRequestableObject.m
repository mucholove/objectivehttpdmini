//
//  NSError+AsRequestableObject.m
//  ObjectiveHTTPDmini
//
//  Created by Gustavo Tavares on 11/24/20.
//  Copyright © 2020 Gustavo Tavares. All rights reserved.
//

#import <ObjectiveHTTPDmini/NSError+AsRequestableObject.h>
#import <GTKFoundation/GTKFoundation.h>


@implementation GTKError
@end
@implementation GTKError (AsJSON)
-(void)
    writeOnJSONStream:(GTKNeXTPListWriter*)aStream
{
    [super writeOnJSONStream:aStream];
    [aStream writeObject:[self userInfo] forKey:@"userInfo"];
}
@end

@implementation NSError (AsHTML)

-(NSUInteger)
httpCode
{
    return 501;
}

-(NSString*)
    messageForHTTPStatusCode:(GTKHTTPCode)code
{
    NSString  *message = nil;

    switch (code) {
        case GTKHTTPCodeNotFound:
            message = @"Not found";
        case GTKHTTPCodeAuthFailed:
            message = @"Authentication failed.";
        case GTKHTTPCodeNotAuthorized:
            message = @"Unauthorized.";
        default:
            message = @"Unknown code";
    }

    return message;
}

-(NSString*)
    asHTML
{
    GTKHTTPCode  code    = CAST(GTKHTTPCode)[self code];
    NSString    *path    = [[self userInfo] objectForKey:@"path"];
    NSString    *message = [self messageForHTTPStatusCode:(GTKHTTPCode)code];


    NSString* notFoundPage
        = [NSString stringWithFormat:
            @"<html><head></head><body>"
            @"%ld %@ — Requested: %@/"
            @"</body></html>\n"
                , code
                , message
                , path];

    return notFoundPage;
}

@end
