//
//  GTKHTTPServer.m
//  ObjectiveHTTPD
//
//  Created by Gustavo Tavares on 3/20/20.
//
//
//  MPWHTTPServer.m
//  microhttp
//
//  Created by Marcel Weiher on 9/6/10.
//  Copyright 2010 Marcel Weiher. All rights reserved.
//

#include <microhttpd.h>
#include <argon2.h>
#include <sqlite3.h>
#include <arpa/inet.h>
#include <poll.h>

#import <ObjectiveHTTPDmini/GTKHTTPServer.h>
#import <ObjectiveHTTPDmini/GTKServerAuthDelegate.h>
#import <ObjectiveHTTPDmini/GTKMHDHTTPRequest.h>
#import <ObjectiveHTTPDmini/GTKHTTPGetRequest.h>
#import <ObjectiveHTTPDmini/GTKHTTPPostRequest.h>
#import <ObjectiveHTTPDmini/GTKHTTPServerResponse.h>
#import <ObjectiveHTTPDmini/NSError+AsRequestableObject.h>
#import <GTKSockets/GTKSockets.h>
#import <ObjectiveHTTPDmini/GTKMHDWebSocketConnection.h>

#if __has_feature(objc_arc)
	#define SAFE_CAST(theType, theVar)  (__bridge theType)theVar
	#define BRIDGE_FOR_ARC(theVar)      (__bridge void*)theVar
	#define CAST_TO(theType)            (__bridge theType)
	#define RETAIN_COUNT(obj)           0
#else
	#define SAFE_CAST(theType, theVar)  (theType)theVar
	#define BRIDGE_FOR_ARC(theVar)      theVar
	#define CAST_TO(theType)            (theType)

    #ifndef RETAIN_COUNT
        #define RETAIN_COUNT(obj)           [obj retainCount]
    #endif

#endif


#ifndef OBJC_ARC
    #if __has_feature(objc_arc)
        #define OBJC_ARC 1
        #define HAS_ARC 1
    #else
        #define OBJC_ARC 0
        #define HAS_ARC 0
    #endif
#endif


#define LOG_HTTPD_DEBUG(...) \
   AlwaysLog(@"%s\n", [[NSString stringWithFormat:__VA_ARGS__] UTF8String]);

#pragma mark - Declarations

static char* ipAddressAsCStringFromSockAddr(const struct sockaddr* requestSockAddress);

int
    gtkAcceptPolicyCallback(
        void*                   cls,
        const struct sockaddr*  requestSockAddress,
        socklen_t               addrlen);

int
    gtkAccessHandlerCallback(
          void*                   cls,
          struct MHD_Connection  *connection,
          const char             *url,
          const char             *method,
          const char             *version,
          const char             *upload_data,
          size_t                 *upload_data_size,
          void                   **con_cls);

int
    HandleConnectedCallback(
        void                   *cls,
        struct MHD_Connection  *connection,
        const char             *url,
        const char             *method,
        const char             *version,
        const char             *upload_data,
        size_t                 *upload_data_size,
        void                   **con_cls);

int
    HandleUnconnectedCallback(
        void                   *cls,
        struct MHD_Connection  *connection,
        const char             *url,
        const char             *method,
        const char             *version,
        const char             *upload_data,
        size_t                 *upload_data_size,
        void                   **con_cls);

char *
    substring(
          char *theSubstring
        , char const *theReference
        , size_t len);


void
    gtkRequestCompletedCallback(
        void *cls
        ,struct MHD_Connection * connection
        ,void **con_cls
        ,enum MHD_RequestTerminationCode terminationCode);


void LogResponseObjectForPath(id self, id aPath, id responseObject, BOOL debug);


#pragma mark - Features

NSString* HTTPResponseCode = @"HTTPResponseCodeForNSError";



@implementation GTKMHDUpgradeToWebSocketResponse
@end

@implementation GTKHTTPCapturedException
{
    GTKHTTPExceptionLocation    _exceptionLocation;
    GTKHTTPRequest*             _request;
    GTKHTTPServerResponse*      _response;
    NSException*                _exception;

    // NSString*        _path;
    // NSString*        _user;
    // NSString*        _session;
}

S_ACCESSOR(GTKHTTPExceptionLocation, _exceptionLocation, exceptionLocation, setExceptionLocation);
M_ACCESSOR(GTKHTTPRequest*,          _request,           request,           setRequest);
M_ACCESSOR(NSException*,             _exception,         exception,         setException);
M_ACCESSOR(GTKHTTPServerResponse*,   _response,          response,          setResponse);

// M_ACCESSOR(NSString*,    _path,       path,      setPath);
// M_ACCESSOR(NSString*,    _user,       user,      setUser);
// M_ACCESSOR(NSString*,    _session,    session,   setSession);

@end


@implementation GTKHTTPServer
{
    void*                        _httpd;
    int                          _port;
    id<GTKHTTPServerDelegate>    _delegate;
    id<GTKServerAuthDelegate>    _authDelegate;
    NSString*                    _email;
    NSString*                    _bonjourName;
    NSData*                      _defaultResponse;
    NSString*                    _defaultMimeType;
    int                          _threadPoolSize;
    NSMutableArray*              _pathsToDebug;
    NSData*                      _keyFile;
    NSData*                      _certFile;
    NSMutableDictionary*         _userToSocketConnections;
    GTKSocketManager*            _manySocketManager;
    NSTimeInterval*              _runLoopTimer;
    BOOL                         _shouldKeepRunning;
    BOOL                         _debugWebSockets;
    BOOL                         _retainWebSocketFrames;
    NSMutableArray*              _webSocketsToDebug;
    dispatch_queue_t             _backToClientQueue;
    NSMutableArray*              _capturedExceptions;
}

GETTER_PLAIN(NSMutableArray*, _webSocketsToDebug, webSocketsToDebug);

M_ACCESSOR(NSData*, _keyFile,           keyFile,         setKeyFile);
M_ACCESSOR(NSData*, _certFile,          certFile,        setCertFile);

M_ACCESSOR(NSMutableArray*, _capturedExceptions, capturedExceptions, setCapturedExceptions);


S_ACCESSOR(BOOL,                _retainWebSocketFrames, retainWebSocketFrames, setRetainWebSocketFrames);
// struct MHD_Daemon*
S_ACCESSOR(BOOL,                _debugWebSockets,   debugWebSockets, setDebugWebSockets);
S_ACCESSOR(void*,               _httpd,             _httpd,         setHttpd);
S_ACCESSOR(int,                 _port,              port,           setPort);
S_ACCESSOR(int,                 _threadPoolSize,    threadPoolSize, setThreadPoolSize )

-(BOOL)
shouldDebugPath:(NSString*)thePath
{
    // if ([_pathsToDebug objectForKey:thePath])
    if ([_pathsToDebug containsObject:thePath])
    {
        return YES;
    }
    else
    {
        return NO;
    }
}
GETTER_PLAIN( NSMutableArray*,      _pathsToDebug,     pathsToDebug)
-(void)
setPathsToDebug:(NSArray*)thePaths
{
    if ([thePaths respondsToSelector:@selector(addObject:)])
    {
        ASSIGN_ID_gtk(_pathsToDebug, thePaths);
    }
    else
    {
        NSMutableArray* dict = [NSMutableArray arrayWithArray:thePaths];
        // ASSIGN_ID_gtk(_pathsToDebug, [thePaths mutableCopy]);
        ASSIGN_ID_gtk(_pathsToDebug, dict);
    }
}
M_ACCESSOR( id<GTKServerAuthDelegate>, _authDelegate,     authDelegate,     setAuthDelegate);
M_ACCESSOR( id,                        _delegate,         _delegate,        setDelegate);
M_ACCESSOR( NSData*,                   _defaultResponse , _defaultResponse, setDefaultResponse );


-(NSData*)
defaultResponse
{
    if ( !_defaultResponse ) {
        BOOL useTextFile = NO;

        if (useTextFile)
        {
            [self setDefaultResponse:
                [NSData
                    dataWithContentsOfFile:@"default.txt"
                    options:0
                    error:nil]];
        }
        else
        {
            NSString* dResponse
                = @"<html><body>This is the default response</body></html>";

            [self setDefaultResponse:[dResponse dataUsingEncoding:NSUTF8StringEncoding]];
        }
    }
    return _defaultResponse;
}

-(void)
initLocalVars
{
    [self setPort:[self defaultPort]];
    [self setThreadPoolSize:20];
    ASSIGN_ID_gtk(_userToSocketConnections, [NSMutableDictionary dictionary]);
}

-(instancetype)
init
{
    self = [super init];

    if (self)
    {
        [self initLocalVars];
    }

    return self;
}

#pragma mark - Accessors

-(int)
defaultPort
{
    return 51001;
}

-(id)
delegate
{
    if ( [self _delegate] )
    {
        return [self _delegate];
    }
    return self;
}

-(struct MHD_Daemon*)
httpd
{
    return (struct MHD_Daemon*)[self _httpd];
}

#pragma mark - Accept Connection

static char* ipAddressAsCStringFromSockAddr(const struct sockaddr* requestSockAddress)
{
    char* s = NULL;

    switch(requestSockAddress->sa_family)
    {
        case AF_INET: {
            struct sockaddr_in* addr_in = (struct sockaddr_in*)requestSockAddress;
            s                           = malloc(INET_ADDRSTRLEN);
            inet_ntop(AF_INET, &(addr_in->sin_addr), s, INET_ADDRSTRLEN);
            break;
        }
        case AF_INET6: {
            struct sockaddr_in6* addr_in6 = (struct sockaddr_in6*)requestSockAddress;
            s                             = malloc(INET6_ADDRSTRLEN);
            inet_ntop(AF_INET6, &(addr_in6->sin6_addr), s, INET6_ADDRSTRLEN);
            break;
        }
        // case AF_LOCAL:
        // case AF_UNIX:
        // case AF_APPLETALK:
        default:
            AlwaysLog(@"NOT HANDLED ADDRESS FAMILY.");
            break;
    }

    return s;
}

int
    gtkAcceptPolicyCallback(
        void*                   cls,
        const struct sockaddr*  requestSockAddress,
        socklen_t               addrlen)
{
    DEBUG_C_FUNC_NO

    id self = nil;

    @autoreleasepool {
        #if HAS_ARC
        self = (__bridge id)cls;
        #else
        self = (id)cls;
        #endif

        char* s = ipAddressAsCStringFromSockAddr(requestSockAddress);

        DebugLog(@"Will accept connectiom: IP address - %s\n", s);

        free(s);

    }

    return MHD_YES;
}

#pragma mark - Error Handling

-(NSData*)
errorPage:exception
{
    return [@"Not Found 404\n" asData];
}

-(NSString*)
stringFromData:(NSData*)putData
printingDebugInfo:(BOOL)logDebug
{
    NSString* stringFromData = nil;

    if (!putData)
    {
        return nil;
    }

    #if GNUSTEP
    stringFromData = AUTORELEASE([[NSString alloc] initWithData:putData encoding:NSUTF8StringEncoding]);
    #else
    BOOL usedLossyConversion;

    NSStringEncoding stringEncoding
        = [NSString
            stringEncodingForData:putData
            encodingOptions:0
            convertedString:&stringFromData
            usedLossyConversion:&usedLossyConversion];

    if (logDebug)
    {
        if (stringFromData)
        {
            AlwaysLog(@"String with coding %lu from data: %@"
                    ,(unsigned long)stringEncoding
                    ,stringFromData);

            AlwaysLog(@"Used Lossy Conversion?: %hhd"
                    ,usedLossyConversion);
        }
        else
        {
            AlwaysLog(@"Couldn't create string from data");
        }
    }
    #endif

    return stringFromData;
}



#pragma mark - Request Completed Callback

-(void)
server:(GTKHTTPServer*)server
didServeRequest:(GTKHTTPRequest*)request
withResponse:(GTKHTTPServerResponse*)response
finishedCode:(GTKHTTPRequestFinishedCode)finishedCode
{
    BOOL debug  = YES;

    NSString* theRequestPath = [request path];

    debug = [self shouldDebugPath:theRequestPath];

    switch (finishedCode)
    {
         case MHD_REQUEST_TERMINATED_COMPLETED_OK:
            DebugLog(@"%@: Request completed — SUCCESS", NSStringFromClass([self class]));
            break;
        case MHD_REQUEST_TERMINATED_READ_ERROR:
            DebugLog(@"%@: Request completed — READ ERROR", NSStringFromClass([self class]));
            break;
        case MHD_REQUEST_TERMINATED_WITH_ERROR:
            DebugLog(@"%@: Request completed — ERROR", NSStringFromClass([self class]));
            break;
        case MHD_REQUEST_TERMINATED_CLIENT_ABORT:
            DebugLog(@"%@: Request completed — CLIENT ABORTED", NSStringFromClass([self class]));
            break;
        case MHD_REQUEST_TERMINATED_DAEMON_SHUTDOWN:
            DebugLog(@"%@: Request completed — DAEMON SHUTDOWN", NSStringFromClass([self class]));
            break;
        case MHD_REQUEST_TERMINATED_TIMEOUT_REACHED:
            DebugLog(@"%@: Request completed — TIMEOUT REACHED", NSStringFromClass([self class]));
            break;
        default:
            DebugLog(@"%@: Request completed — UNKNOWN TERMINATION CODE", NSStringFromClass([self class]));
            break;
    }

    if (debug)
    {
        NSPrintF(@"\t\tPATH: %@", theRequestPath);
        NSPrintF(@"\t\tSTATUS CODE: %ld", [[[request response] ifResponds] httpResponseCode] ?: 9999);
        NSPrintF(@"\t\tMETHOD: %@", StringForGTKHTTPMethod([request HTTPMethod]));
    }

    if ([[[request response] MIMEType] isEqual:[MimeType json]])
    {
        if ([request isKindOfClass:[GTKHTTPPostRequest class]])
        {
            if (debug)
            {
                NSPrintF(@"\t\tBODY: %*.s", (int)[[request payload] length], (char*)[[request payload] bytes]);
            }
        }

        if (debug)
        {
            NSPrintF(@"\t\tRETURN: %*.s", (int)[[request response] length], (char*)[[request response] bytes] );
        }
    }

    BOOL debugRetainCount = YES;

    if (debug || debugRetainCount)
    {
        #if HAS_ARC

        AlwaysLog(@"%@: Extra Object to Release — %@ — Retain Count - N/A"
            ,[CAST(__bridge id)self className]
            , CAST(__bridge id)request);

        #else

        NSUInteger retainCount = [request retainCount];

        AlwaysLog(@"%@: Extra Object to Release — %@ — Retain Count - %lu"
            ,[(id)self className]
            , (id)request
            , retainCount);

        if (retainCount > 1)
        {
            AlwaysLog(@"WARNING WARNING WARNING RETAIN COUNT > 1");
        }

        [GTKAllocationSnapshotTracker takeSnapshot];

        #endif
    }


}

void
    gtkRequestCompletedCallback(
        void *cls
        ,struct MHD_Connection * connection
        ,void **con_cls
        ,enum MHD_RequestTerminationCode terminationCode)
{
    @autoreleasepool
    {
        /** 1 —
         * Error handling the connection (resources
         * exhausted, other side closed connection,
         * application error accepting request, etc.)
         * @ingroup request
         */

         #if HAS_ARC
         id self                     = (__bridge id)cls;
         GTKMHDHTTPRequest* request  = (__bridge GTKMHDHTTPRequest*)*con_cls
         #else
         id self                     = (id)cls;
         GTKMHDHTTPRequest* request  = (id)*con_cls;
         #endif

        [self server:self
              didServeRequest:request
              withResponse:[[request ifResponds] response]
              finishedCode:terminationCode];

        #if !HAS_ARC
        RELEASE(request);
        #endif
    }
}

#pragma mark - HTTPD / ObjectiveC interface

static int
    addKeyValuesToDictionary(
         void *cls
        ,enum MHD_ValueKind kind
        ,const char *key
        ,const char *value)
{
    @autoreleasepool
    {
        [CAST_TO(NSMutableDictionary*)cls
            setObject:[NSString
                        stringWithCString:value
                        encoding:NSISOLatin1StringEncoding]
            forKey:[NSString
                        stringWithCString:key
                        encoding:NSISOLatin1StringEncoding]];
    }

    return MHD_YES;
}




// USED IN by the MHD_CreatePostProcessor
// inside — AccessPolicyCallback
// +----------- WARNING -------------------+
// +--  For some reason... MHD_post_process is not being called so we are resorting to simpler model...
// +--  Why? Maybe because I wasn't using it as a reference.
// +-- &iteratePost?
// +---------------------------------------+
BOOL G_UsePostProcessor = NO;
static int
    iterate_post (
        void *cls,
        enum MHD_ValueKind kind,
        const char *key,
        const char *filename,
        const char *content_type,
        const char *transfer_encoding,
        const char *bytes,
        uint64_t off,
        size_t size)
{
    DEBUG_C_FUNC_NO


    @autoreleasepool
    {
        DebugLog(@"Will iterate_post: key %s filename: %s content_type: %s transfer_encoding: %s len: %d content: %.*s"
                    ,key
                    ,filename
                    ,content_type
                    ,transfer_encoding
                    ,(int)size
                    ,(int)size
                    ,bytes);

        GTKHTTPPostRequest *processor
            = CAST_TO(GTKHTTPPostRequest*)cls ;

        NSString* fileNameString
            = filename
                ? [NSString
                        stringWithCString:filename
                        encoding:NSISOLatin1StringEncoding]
                : nil;

        NSString* keyString
            = key
                ? [NSString
                        stringWithCString:key
                        encoding:NSISOLatin1StringEncoding]
                : nil;

        if ( keyString )
        {
            [processor
                appendBytes:bytes
                length:(int)size
                toKey:keyString
                filename:fileNameString
                contentType:nil];
        }
        else
        {
            DebugLog(@"no key for POST data");
        }

    }

    return MHD_YES;
}

objectAccessor(NSString*,
    _defaultMimeType,
    setDefaultMimeType)

-(void)
fillDictWithRequestHeaders:(NSMutableDictionary**)headerDict
fromConnection:(struct MHD_Connection*)connection
{
    // headerDict
    //    = [NSMutableDictionary dictionary];

    MHD_get_connection_values (
        connection,
        MHD_HEADER_KIND,
        addKeyValuesToDictionary,
        (void*)&headerDict);
}

-(void)
fillDictWithURLParams:(NSMutableDictionary**)parameterDict
fromConnection:(struct MHD_Connection*)connection
{
    // NSMutableDictionary* parameterDict
    //    = nil;;

    MHD_get_connection_values (
        connection,
        MHD_GET_ARGUMENT_KIND,
        addKeyValuesToDictionary,
        ( void*)&parameterDict);
}

// MARK: ACCESS HANDLER CALLBACK AND ATTEMPTS TO UNDERSTAND

-(BOOL)
respondsToRequest:(GTKMHDHTTPRequest*)theRequest
{
    return YES;
}

#pragma mark - Upgrade To Web Socket

void
    UpgradeToWebSocket(
        void*                               cls,
        struct MHD_Connection*              connection,
        void*                               con_cls,
        const char*                         extra_in,
        size_t                              extra_in_size,
        MHD_socket                          sock,
        struct MHD_UpgradeResponseHandle*   theUpgradeResponseHandle
){
    DEBUG_C_FUNC_NO

    // https://github.com/boazsegev/facil.io/blob/master/lib/facil/http/websockets.c

    GTKMHDHTTPRequest* request = (GTKMHDHTTPRequest*)cls;
    GTKHTTPServer* server   = [request server];

    // WebSockets
    //            Sec-WebSocket-Key
    //            Sec-WebSocket-Extensions
    //            Sec-WebSocket-Accept
    //            Sec-WebSocket-Protocol
    //            Sec-WebSocket-Version


    DebugLog(@"Managing web socket connection.");
    DebugLog(@"Request                 : %@", request);
    DebugLog(@"Socket File Descriptor  : %d", sock);

    GTKMHDWebSocketConnection * socketConnection
        = AUTORELEASE([[GTKMHDWebSocketConnection  alloc]
            initWithRequest:request
            MHDConnection:connection
            socket:sock
            responseHandle:theUpgradeResponseHandle]);

    DebugLog(@"Socket connection: %@", socketConnection);

    [server manageWebSocketConnection:socketConnection];
}

-(void)
addWebSocketConnection:(GTKMHDWebSocketConnection*)theWebSocketConnection
forUser:(id)theUser
{
    DEBUG_NO

    DebugLog(@"User to socket connections dict : %@", _userToSocketConnections);
    DebugLog(@"User                            : %@", theUser);

    id key = [self keyForUser:theUser];

    NSMutableArray* userSockets = nil;
    userSockets = [_userToSocketConnections objectForKey:key];

    DebugLog(@"PreAdd: User Socket Array       : %@", userSockets);

    if (!userSockets)
    {
        userSockets = [NSMutableArray array];

        [_userToSocketConnections
            setObject:userSockets
            forKey:key];
    }

    [userSockets addObject:theWebSocketConnection];

    DebugLog(@"PostAdd: User Socket Array      : %@", userSockets);
}

-(void)
closeWebSocketConnection:(GTKMHDWebSocketConnection*)theWebSocketConnection
{
    id theUser = [theWebSocketConnection user];

    NSMutableArray* webSocketConnections = [self webSocketConnectionsForUser:theUser];

    [webSocketConnections removeObject:theWebSocketConnection];
}

-(NSMutableArray*)
webSocketConnectionsForUser:(id)theUser
{
    id userSockets = [_userToSocketConnections objectForKey:[self keyForUser:theUser]];
    return userSockets;
}

-(id)
keyForWebSocketConnection:(GTKMHDWebSocketConnection*)theWebSocketConnection
{
    id key = nil;

    // key = [[theWebSocketConnection request] user];
    key = @([theWebSocketConnection socket]);

    return key;
}

-(GTKSocketManager*)
manySocketManager
{
    DEBUG_NO

    if (!_manySocketManager)
    {
        DebugLog(@"Will create new `many socket manager`");
        // ASSIGN(_manySocketManager, [[GTKSocketManager alloc] init]);
        ASSIGN_ID_gtk(_manySocketManager, [[GTKPollWebSocketManager alloc] init]);
    }


    DebugLog(@"Returning many socket manager: %@", _manySocketManager);

    return _manySocketManager;
}


-(void)
manageWebSocketConnection:(GTKMHDWebSocketConnection*)theWebSocketConnection
{
    DEBUG_NO

    id user = [theWebSocketConnection user];
    id key  = [self keyForWebSocketConnection:theWebSocketConnection];

    if (_debugWebSockets)
    {
        if (!_webSocketsToDebug)
        {
            _webSocketsToDebug = [[NSMutableArray alloc] init];
        }
        [_webSocketsToDebug addObject:theWebSocketConnection];


        if (_retainWebSocketFrames)
        {
            [theWebSocketConnection setShouldRetainFrames:YES];
        }
    }

    // Consider using EPOLL to monitor all the sockets....
    [theWebSocketConnection setDelegate:self];


    DebugLog(@"GTKHTTPServer: Will add WebSocketConnection for User: %@ / %@", theWebSocketConnection, user);

    [self addWebSocketConnection:theWebSocketConnection
          forUser:user];

    DebugLog(@"GTKHTTPServer: Did add WebSocketConnection for User: %@ / %@", theWebSocketConnection, user);

    GTKSocketManager* socketManager = [self manySocketManager];
    MHD_socket        socketFD      = [theWebSocketConnection socket];

    DebugLog(@"%@: Will add WebSocketConnection %@ for File Descriptor: %d", socketManager, theWebSocketConnection, socketFD);

    [theWebSocketConnection setManySocketManager:socketManager];
    [socketManager addWebSocketConnection:theWebSocketConnection];

    DebugLog(@"%@: Did add WebSocketConnection %@ for File Descriptor: %d", socketManager, theWebSocketConnection, socketFD);


}
-(id)
keyForUser:(id)user
{
    return [user identifier];
    // return user;
}


-(void)
freeWebSocketConnection:(GTKMHDWebSocketConnection*)theWebSocketConnection
{
    DEBUG_NO

    id key     = [self keyForWebSocketConnection:theWebSocketConnection];
    id user    = [theWebSocketConnection user];
    id userKey = [user identifier];

    DebugLog(@"Will free web socket connection: %@", theWebSocketConnection);
    DebugLog(@"Key          : %@", key);
    DebugLog(@"User key     : %@", userKey);

    @synchronized (_userToSocketConnections)
    {
        NSMutableArray* array = [_userToSocketConnections objectForKey:userKey];
        [array removeObject:theWebSocketConnection];
        [theWebSocketConnection freeFromSocketManager];
    }
}

-(BOOL)
sendData:(NSData*)theData
toUser:(id)theUser
error:(NSError**)outErr
{
    NSArray* userWebSocketConnections
        = [self webSocketConnectionsForUser:theUser];

    for (GTKMHDWebSocketConnection* connection in userWebSocketConnections)
    {
        [connection sendData:theData];
    }

    return YES;
}


#pragma mark - Web Socket Connection Callback

-(void)
webSocketConnection:(id)theWebSocketConnection
gotError:(NSError*)error
{
    AlwaysLog(@"Web Socket Connection Got Error: %@", error);
    [(GTKMHDWebSocketConnection*)theWebSocketConnection closeConnection];
}

//-(void)
//webSocketConnection:(id)theWebSocketConnection
//didRecieveFrame:(GTKWebSocketFrame*)theWebSocketFrame
//{
//
//}

-(void)
webSocketConnection:(id)theWebSocketConnection
didReceiveMessage:(GTKWebSocketMessage*)theMessage
{
    DEBUG_NO
    DebugLog(@"Web socket connection: %@", theWebSocketConnection);
    DebugLog(@"Got message: %@", theMessage);

    WebSocketFrameType frameType = [theMessage type];

    DebugLog(@"Frame Type: %@", NSStringForWebSocketFrameU8Int(frameType));

    if ((frameType == WebSocketFrameText) || (frameType == WebSocketFrameBinary))
    {


        NSData* payload = [theMessage payload];

        GTKWebSocketRequest* request = [GTKWebSocketRequest requestFromData:payload];

        NSString*     path            = [request path];
        NSDictionary* queryParameters = [request queryParameters];

        id response = [[self delegate] serveRequest:request];

        GTKMHDWebSocketConnection* webSocketConnection = (GTKMHDWebSocketConnection*)theWebSocketConnection;

        DebugLog(@"Will prepare response.");

        GTKWebSocketRequest* requestResponse = AUTORELEASE([[GTKWebSocketRequest alloc] init]);

        id user = [theWebSocketConnection user];

        DebugLog(@"Web Socket Connection: %@", user);

        [requestResponse setUser:user];
        [requestResponse setPath:path];
        [requestResponse setIsResponse:YES];
        [requestResponse setPayload:response];

        NSData* responseData = [[requestResponse asWebSocketFrame] asData];

        DebugLog(@"Got response data: %@", responseData);

        [webSocketConnection sendData:responseData];
    }
}

-(void)
willCloseWebSocketConnection:(id)theWebSocketConnection
{
    DEBUG_NO
    DebugLog(@"Will close web socket connection: %@", theWebSocketConnection);
}

-(void)
didCloseWebSocketConnection:(id)theWebSocketConnection
{
    DEBUG_NO
    DebugLog(@"Did close web socket connection: %@", theWebSocketConnection);
}

#pragma mark - Handle Connected Callback

static inline void
    stuffRequest(GTKMHDHTTPRequest*         request)
{
    DEBUG_C_FUNC_NO

    GTKHTTPServer* server
        = [request server];


    DebugLog(@"Request Server: %@", server);

    struct MHD_Connection* connection
        = [request connection];

    NSMutableDictionary* headerDict
        = [request headers]; // = [NSMutableDictionary dictionary];

    MHD_get_connection_values (
        connection,
        MHD_HEADER_KIND,
        addKeyValuesToDictionary,
        CAST_TO(void*)headerDict); // ( void*)[request headers]);

    NSMutableDictionary* parameterDict
        = [request queryParameters]; // = [NSMutableDictionary dictionary];

    MHD_get_connection_values (
        connection,
        MHD_GET_ARGUMENT_KIND,
        addKeyValuesToDictionary,
        CAST_TO(void*)parameterDict); // // ( void*)[request queryParameters]);


    // MHDConnectionInfo *unionConnectionInfo
    //     = MHD_get_connection_info(
    //             connection,
    //             MHD_CONNECTION_INFO_CLIENT_ADDRESS);

        id session = nil;
        id<GTKServerAuthDelegate> theAuthDelegate = [server authDelegate];

        if (theAuthDelegate)
        {
            NSString* keyForUser           = [theAuthDelegate keyForUserInHTTPHeader];
            NSString* keyForToken          = [theAuthDelegate keyForTokenInHTTPHeader];
            NSString* keyForUserLowercase  = [keyForUser lowercaseString];
            NSString* keyForTokenLowercase = [keyForToken lowercaseString];

            NSString* user  = nil;
            NSString* token = nil;

            // 1 - Search Headers
            DebugLog(@"Searching headers for user / token. Headers: %@", headerDict);

            user  = [headerDict objectForKey:keyForUser];
            token = [headerDict objectForKey:keyForToken];

            if (!user)  { user  = [headerDict objectForKey:keyForUserLowercase];  }
            if (!token) { token = [headerDict objectForKey:keyForTokenLowercase]; }

            // 2 - Search Cookies
            if (!user && !token)
            {
                NSDictionary* cookies = [request cookies];

                DebugLog(@"Header search failed. Searching cookies for user / token. Cookies: %@", cookies);

                user   = [cookies objectForKey:keyForUser];
                token  = [cookies objectForKey:keyForToken];

                if (!user)  { user  = [cookies objectForKey:keyForUserLowercase];  }
                if (!token) { token = [cookies objectForKey:keyForTokenLowercase]; }
            }

            // 3 - Use Basic Auth
            if (!user && !token)
            {
                char* cUser     = NULL;
                char* cPassword = NULL;

                cUser = MHD_basic_auth_get_username_password(
                            connection,
                            &cPassword);

                DebugLog(@"Header search failed. Using HTTP Basic Auth. cUser %s / cPassword %s", cUser, cPassword);

                if (cUser)
                {
                    user = AUTORELEASE([[NSString alloc] initWithCString:cUser encoding:NSUTF8StringEncoding]);
                    MHD_free(cUser);
                }

                if (cPassword)
                {
                    token = AUTORELEASE([[NSString alloc] initWithCString:cPassword encoding:NSUTF8StringEncoding]);
                    MHD_free(cPassword);
                }
            }

            DebugLog(@"Auth Delegate: %@", [server authDelegate]);
            DebugLog(@"User: %@", user);
            DebugLog(@"Token: %@", token);

            if (user && token)
            {
                DebugLog(@"Will check if session has valid username / token.");

                session = [[server authDelegate]
                            isValidUserName:user
                            withToken:token];

                DebugLog(@"%@ - session exists in AuthDelegate: %@", (session ? @"YES" : @"NO"), session);

                LOG_HTTPD_DEBUG(@"From header — user %@ - pword - %@ - session - %@", user, token, session);
                LOG_HTTPD_DEBUG(@"AUTH USER RC COUNT - %lu - %@", [session retainCount], [session className]);

                [request setSession:session];
            }
        }


        if ([request isKindOfClass:[GTKHTTPPostRequest class]]) // && G_UsePostProcessor)
        {
            NSString* contentType = [headerDict objectForKey:@"Content-Type"];
            debug = YES;
            DebugLog(@"Content type is: %@", contentType);

            if (![contentType isEqual:[MimeType json]])
            {
                DebugLog(@"Will create post_processor");
                void* post_processor
                    = (void*)MHD_create_post_processor (
                             connection
                            ,8192
                            ,&iterate_post
                            ,CAST_TO(void*)request);

                if (post_processor == NULL) {
                    DebugLog(@"Could NOT create POST_PROCESSOR....");
                    // NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];

                    // int returnCode
                    //     = [self respondToRequest:request
                    //             withObject:[NSError errorWithDomain:@"CouldNotProcess"
                    //                                 code:501
                    //                                 userInfo:@{ @"Reason": @"Could NOT process data. Server busy." }]
                    //             httpCode:501];
                    // [pool drain];

                    // return returnCode;
                } else {
                    DebugLog(@"Did CREATE POST_processor %p\n",post_processor);
                }

                [CAST(GTKHTTPPostRequest*)request setProcessor:post_processor];
            }
        }
}



int
    HandleConnectedCallback(
        void                   *cls,
        struct MHD_Connection  *connection,
        const char             *url,
        const char             *method,
        const char             *version,
        const char             *upload_data,
        size_t                 *upload_data_size,
        void                   **con_cls)
{
    DEBUG_C_FUNC_NO

    id self = CAST_TO(id)cls;

    NSString* urlString
        = [NSString
            stringWithCString:url
            encoding:NSISOLatin1StringEncoding];

    debug = [self shouldDebugPath:urlString];

    DebugLog(@"Continuing access handler callback: %s: url: '%s'\n"
                ,method
                ,url);

    // Guessing this fails with ARC because of **
    DEBUG_PUTS("Will safe cast...");
    // GTKMHDHTTPRequest *request = SAFE_CAST(GTKMHDHTTPRequest*, *con_cls);
    // GTKMHDHTTPRequest *request = (GTKMHDHTTPRequest*)*con_cls;
    GTKMHDHTTPRequest *request = *con_cls; // This is what we use in OSX
    DEBUG_PUTS("Did safe cast...");


    if (![request isStuffed])
    {

        DebugLog(@"Will check if responds to request.");
        BOOL respondsToRequest = [self respondsToRequest:request];

        if (!respondsToRequest)
        {
            DebugLog(@"Does not respond to request. Returning Unstuffed 404.");
            NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];

            int returnCode
                = [self respondToRequest:request
                        withObject:[NSError errorWithDomain:@"PathNotFound"
                                            code:404
                                            userInfo:@{
                                                @"Reason"          : @"Resource not found. No response obj.",
                                                HTTPResponseCode   : @404,
                                            }]];
            [pool drain];

            return returnCode;
        }


        DebugLog(@"Does respond to request...");

        DebugLog(@"Will stuff request...\n");

        [request setServer:self];
        [request setConnection:connection];

        @try
        {
            stuffRequest(request);
        }
        @catch(NSException* exception)
        {
            AlwaysLog(@"Error stuffing request. (Probably fetching user) Will return error.");

            NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];

            DebugLog(@"Will make error object.");
            NSError* error = [exception asErrorWithCode:501];
            DebugLog(@"Did make error object: %@", error);

            int returnCode = [self respondToRequest:request
                                   withObject:error];
            [pool drain];

            return returnCode;
        }

        [request setIsStuffed:YES];

        // [request hostName];

        DebugLog(@"Did stuff request.\n");

        DebugLog(@"Will check for upgrade...");

        NSDictionary* headers                = [request headers];
        NSString*     upgrade                = [headers objectForKey:@"Upgrade"];
        NSString*     webSocketVersionHeader = [headers objectForKey:@"Sec-WebSocket-Version"];

        DebugLog(@"Got upgrade: %@", upgrade);


        BOOL requestsUpgradeToWebsocket = ((upgrade && [upgrade isEqual:@"websocket"]) || (webSocketVersionHeader));

        if (requestsUpgradeToWebsocket && [request isAuthenticated])
        {
            DebugLog(@"Will UPGRADE to WEB-SOCKET");
            /*  RFC 6455 - The WebSocket Protocol - Opening Handshake - pgs. 6,7,8 - Fette & Melnikov

                The server has to prove to the client that it received the
                client's WebSocket handshake, so that the server doesn't accept
                connections that are not WebSocket connections.  This prevents an
                attacker from tricking a WebSocket server by sending it carefully
                crafted packets using XMLHttpRequest [XMLHttpRequest] or a form
                submission.

                To prove that the handshake was received, the server has to take two
                pieces of information and combine them to form a response.  The first
                piece of information comes from the |Sec-WebSocket-Key| header field
                in the client handshake:

                    Sec-WebSocket-Key: dGhlIHNhbXBsZSBub25jZQ==

                For this header field, the server has to take the value (as present
                in the header field, e.g., the base64-encoded [RFC4648] version minus
                any leading and trailing whitespace) and concatenate this with the
                Globally Unique Identifier (GUID, [RFC4122]) "258EAFA5-E914-47DA-
                95CA-C5AB0DC85B11" in string form, which is unlikely to be used by
                network endpoints that do not understand the WebSocket Protocol.  A
                SHA-1 hash (160 bits) [FIPS.180-3], base64-encoded (see Section 4 of
                [RFC4648]), of this concatenation is then returned in the server's
                handshake.

                Concretely, if as in the example above, the |Sec-WebSocket-Key|
                header field had the value "dGhlIHNhbXBsZSBub25jZQ==", the server
                would concatenate the string "258EAFA5-E914-47DA-95CA-C5AB0DC85B11"
                to form the string "dGhlIHNhbXBsZSBub25jZQ==258EAFA5-E914-47DA-95CA-
                C5AB0DC85B11".  The server would then take the SHA-1 hash of this,
                giving the value 0xb3 0x7a 0x4f 0x2c 0xc0 0x62 0x4f 0x16 0x90 0xf6
                0x46 0x06 0xcf 0x38 0x59 0x45 0xb2 0xbe 0xc4 0xea.  This value is
                then base64-encoded (see Section 4 of [RFC4648]), to give the value
                "s3pPLMBiTxaQ9kYGzzhZRbK+xOo=".  This value would then be echoed in
                the |Sec-WebSocket-Accept| header field.

                The handshake from the server is much simpler than the client
                handshake.  The first line is an HTTP Status-Line, with the status
                code 101:

                    HTTP/1.1 101 Switching Protocols
            */

            DebugLog(@"Will create UPGRADE response.");

            struct MHD_Response* response
                =  MHD_create_response_for_upgrade(
                        UpgradeToWebSocket,
                        request);

            DebugLog(@"Did create UPGRADE response.");

            MHD_add_response_header (response
                , "Upgrade"
                , "websocket");

            /*
             * If being translated to HTTP2
             * Safari might not load this connection.
             * - https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Connection
             */
            MHD_add_response_header (response
                , "Connection"
                , "Keep-Alive");

            //: Generate Accept Key


            NSString* acceptKeyBase = [headers objectForKey:@"Sec-WebSocket-Key"];

            DebugLog(@"Accept Key Base: %@", acceptKeyBase);

            if (acceptKeyBase)
            {
                NSMutableString* acceptResponseBuilder = [NSMutableString string];

                [acceptResponseBuilder appendString:acceptKeyBase];

                [acceptResponseBuilder appendString:@"258EAFA5-E914-47DA-95CA-C5AB0DC85B11"];

                DebugLog(@"Accept Response Before Hash: %@", acceptResponseBuilder);

                NSString* shaHashOfAccpetResponse = [GTKSHA1Hasher hashOfString:acceptResponseBuilder];

                DebugLog(@"Accept Response After Hash: %@", shaHashOfAccpetResponse);

                NSString* toSendAcceptKey = [shaHashOfAccpetResponse asBase64_gtk];

                DebugLog(@"Accept Key: %@", toSendAcceptKey);

                MHD_add_response_header (response
                    , "Sec-WebSocket-Accept"
                    , [toSendAcceptKey UTF8String]);


            }

            // the value of the Sec-WebSocket-Key sent in the handshake request, appends 258EAFA5-E914-47DA-95CA-C5AB0DC85B11, takes SHA-1 of the new value, and is then base64 encoded.

            //: Analyze Extensions

            id extensions = [headers objectForKey:@"Sec-WebSocket-Extensions"];

            if ([extensions isKindOfClass:[NSString class]])
            {
                DebugLog(@"Extension is string");
            }
            else if ([extensions isKindOfClass:[NSArray class]])
            {
                DebugLog(@"Extension is array");
            }


            DebugLog(@"Sending SWITCHING protocols.");

            int statusCode =
                MHD_queue_response(
                    connection,
                    MHD_HTTP_SWITCHING_PROTOCOLS,
                    response);

            return statusCode;
        }
        else if (upgrade && [upgrade isEqual:@"h2c"] && [request isAuthenticated])
        {
           // > GET / HTTP/1.1
           // > Host: server.example.com
           // > Connection: Upgrade, HTTP2-Settings
           // > Upgrade: h2c
           // > HTTP2-Settings: <base64url encoding of HTTP/2 SETTINGS payload>
        }
        else if (upgrade && [upgrade isEqual:@"h2"] && [request isAuthenticated])
        {
            // HTTP/2 over TLS uses the "h2" protocol identifier.
            // The "h2c" protocol identifier MUST NOT be sent by a client or
            //  selected by a server; the "h2c" protocol identifier
            // describes a protocol that does not use TLS.

            //  Starting HTTP/2 with Prior Knowledge
            //
            //  HTTP/2 connections over cleartext TCP:
            //   > A client MUST send the connection preface (Section 3.5)
            //   > and then MAY immediately send HTTP/2 frames to such a server;
            //   > servers can identify these connections by the presence of the connection preface.
            //  HTTP/2 over TLS
            //   > MUST use protocol negotiation in TLS [TLS-ALPN].
            //      The client connection preface starts with a sequence of 24 octets.
            //          As HEX:    0x505249202a20485454502f322e300d0a0d0a534d0d0a0d0a
            //          As Char*:  PRI * HTTP/2.0\r\n\r\nSM\r\n\r\n
            //  This sequence MUST be followed by a SETTINGS frame (Section 6.5), which MAY be empty.
            //  The client sends the client connection preface immediately upon receipt of a 101 (Switching Protocols) response (indicating a successful upgrade) or as the first application data octets of a TLS connection.
            //  If starting an HTTP/2 connection with prior knowledge of server support for the protocol,
            //  the client connection preface is sent upon connection establishment.
        }





        // MHD_HEADER_KIND = 1,
        //
        // Cookies.
        //      Note that the original HTTP header containing the
        //      cookie(s) will still be available and intact.
        //  MHD_COOKIE_KIND = 2,
        // MHD_POSTDATA_KIND = 4, only if a content encoding supported by MHD is used (currently only URL encoding), and only if the posted content fits within the available memory pool.
        // MHD_GET_ARGUMENT_KIND = 8, QUERY PARAMETERS
        // MHD_FOOTER_KIND = 16 // HTTP footer (only for HTTP 1.1 chunked encodings).

        // union MHD_ConnectionInfo
        // enum  MHD_ConnectionInfoType
        // MHD_CONNECTION_INFO_CIPHER_ALGO
        // MHD_CONNECTION_INFO_PROTOCOL         // NULL for non-HTTPS. Non-Null otherwise
        // MHD_CONNECTION_INFO_CLIENT_ADDRESS   // struct sockaddr ** ( union MHD_ConnectionInfo * and that union contains a struct sockaddr*).
        // MHD_CONNECTION_INFO_GNUTLS_SESSION
        // MHD_CONNECTION_INFO_DAEMON
        // MHD_CONNECTION_INFO_CONNECTION_FD    // File Descriptor. Usually TCP socket. Applications might use this access to manipulate TCP options, for example to set the “TCP-NODELAY” option for COMET-like applications
        // MHD_CONNECTION_INFO_SOCKET_CONTEXT   // Returns the client-specific pointer to a void * that was (possibly) set during a MHD_NotifyConnectionCallback
                                                // The con_cls is fresh for each HTTP request, while the socket_context is fresh for each socket.

        // Setting custom options for an individual connection
        // MHD_set_connection_option (struct MHD_Connection *daemon, enum MHD_CONNECTION_OPTION option, ...)
        //      MHD_CONNECTION_OPTION_TIMEOUT
    }


    DebugLog(@"%@: Handling Connected Callback On: %@ — %lu/h %lu/p"
            ,NSStringFromClass([self class])
            ,urlString
            ,[[request headers] count]
            ,[[request queryParameters] count]);


    // DebugLog(@"Returning...");
    return [self
        manageRequest:request
        forPath:urlString
        withData:upload_data
        withDataLength:upload_data_size
        onConnection:connection
        withContext:con_cls];
}

// enum MHD_Result
int
    UpgradeHTTPToHTTPSHandler(
        void*                               cls,
        struct MHD_Connection*              connection,
        const char*                         extra_in,
        size_t                              extra_in_size,
        MHD_socket                          sock,
        struct MHD_UpgradeResponseHandle*   urh)
{
    AlwaysLog(@"Inside HTTP to HTTPS upgrade handler.");



    MHD_upgrade_action(urh, MHD_UPGRADE_ACTION_CLOSE);

    return MHD_YES;

}

// Perform special operations related to upgraded connections.
// MHD_Result MHD_upgrade_action (struct MHD_UpgradeResponseHandle *urh, enum MHD_UpgradeAction action, ...)
// MHD_upgrade_action(upgradeResponseHandle, MHD_UPGRADE_ACTION_CLOSE);

int
    HandleUnconnectedCallback(
        void                   *cls,
        struct MHD_Connection  *connection,
        const char             *url,
        const char             *method,
        const char             *version,
        const char             *upload_data,
        size_t                 *upload_data_size,
        void                   **con_cls)
{
    DEBUG_C_FUNC_NO

    id self = SAFE_CAST(id, cls);
    LOG_HTTPD_DEBUG(@"%@: Handling Unconnected Callback" ,NSStringFromClass([self class]));


    NSString* pathAsString = [NSString stringWithUTF8String:url];
    debug                  = [self shouldDebugPath:pathAsString];

    /*
     * MHD_CONNECTION_INFO_PROTOCOL
     *  - Allows finding out the TLS/SSL protocol used (HTTPS connections only).
     *  - NULL is returned for non-HTTPS connections.
     *  - Takes no extra arguments.
     *  ---------------------------
     *  - MHD_get_connection_info
     *  - enum MHD_ConnectionInfoType
     */


    id session = nil;

    /*

     *
     * WILL CAUSE BAD ACCESS IF INITIALIZED for SOME REASON
     * char *urlSubstring = NULL;
     * char *urlSubstring;
     *
     * substring(urlSubstring, url, 5);
     *
     * if (strcmp(urlSubstring, "/auth") != 0)
     * https://stackoverflow.com/a/4770992/1483037
     */

    /*
     *   The Argon2 documentation recommends
     *       — 0.5 seconds for authentication.
     *   Libsodium’s documentation recommends
     *       1 second  for web applications
     *       5 seconds for desktop applications
     *
     *   I personally wouldn’t recommend anything more than
     *       1 second or your users will hate you and logins will end up
     *       DoS’ing your application.
     *
     *   Anything less than 0.5 seconds is a certain security failure.
     *
     *   https://web.archive.org/web/20191224044056/https://www.twelve21.io/how-to-choose-the-right-parameters-for-argon2/
     */

    #if 0
    static int requests
        = 0;

    if ( ++requests % 5000 ==0 )
    {
        AlwaysLog(@"request: %d",requests);
    }
    #endif


    GTKMHDHTTPRequest* request = nil;

    LOG_HTTPD_DEBUG(@"Request at Path %s — Auth Delegate: %@", url, [self authDelegate]);

    AlwaysLog(@"Found session %@", session);

    if ( !strcmp("GET", method))
    {
        request = [GTKHTTPGetRequest requestOnConnection:connection
                                     session:session];
    }
    else if ( !strcmp("POST", method) )
    {
        DebugLog(@"Handling POST");

        /*
        * Create a `struct MHD_PostProcessor`.
        *
        * A `struct MHD_PostProcessor` can be used to (incrementally) parse
        * the data portion of a POST request.
        *
        * Note: Some buggy browsers fail to set the encoding type.
        *      If you want to support those...
        *               you  may have to call #MHD_set_connection_value
        *               with the proper encoding type before
        *               creating a post processor (if no supported encoding
        *               type is set, this function will fail).
        *
        * PARAM—1:Connection
        *        The connection on which the POST is
        *        happening (used to determine the POST format)
        * PARAM—2:buffer_size
        *        The maximum number of bytes to use for
        *        internal buffering (used only for the parsing,
        *        specifically the parsing of the keys).
        *
        *        A tiny value (256-1024) should be sufficient.
        *        Do NOT use a value smaller than 256.  For good
        *        performance, use 32 or 64k (i.e. 65536).
        * PARAM-3:ITER
        *        Iterator to be called with the parsed data,
        *        Must NOT be NULL.
        * PARAM-4:ARG_FOR_ITER—iter_cls
        *        First argument to @a iter
        * RETURNS...
        *        NULL
        *           on error (out of memory, unsupported encoding),
        *        PP handle otherwise.
        */

        // AlwaysLog(@"POST url: '%s'\n",url);


        request = [GTKHTTPPostRequest
            processorForSession:session
            onConnection:connection];


    }
    else
    {
        // !strcmp("OPTIONS", method)
        // !strcmp("PROPFIND", method)
        AlwaysLog(@"%@: Unhandled HTTP Verb: %s"
                ,NSStringFromClass([self class])
                ,method);

        return MHD_NO;
    }

    *con_cls = BRIDGE_FOR_ARC(request);
    RETAIN(request);

    [request setPath:pathAsString];
    [request setSession:session];

	#if HAS_ARC
    AlwaysLog(@"Queing Request %@", request);
	#else
    AlwaysLog(@"Queing Request %@ - Retain Count - %lu", request, [request retainCount]);
	#endif

    return MHD_YES;
}


#define DENIED \
    "<html><head><title>Access denied</title></head><body>Access denied</body></html>"
int
    HandleFailedAuthenticationOnConnection(
        struct MHD_Connection  *connection,
        const char             *url,
        const char             *user)
{
    struct MHD_Response *response;
    int ret;

    response = MHD_create_response_from_buffer (
                    strlen (DENIED),
#if HAS_ARC
                    (void*)DENIED,
#else
                    (void*)DENIED,

#endif
                    MHD_RESPMEM_PERSISTENT);

    ret = MHD_queue_basic_auth_fail_response (
                connection
                ,"TestRealm"
                ,response);

    return ret;
}

/*
 const char * strstr ( const char * str1, const char * str2 );
       char * strstr (       char * str1, const char * str2 );
 Locate substring
 Returns a pointer to the first occurrence of str2 in str1, or a null pointer if str2 is not part of str1.

 The matching process does not include the terminating null-characters, but it stops there.
 */

char *
    substring(
          char *theSubstring
        , char const *theReference
        , size_t len)
{
    memcpy(theSubstring, theReference, len);
    theSubstring[len] = '\0';
    return theSubstring;
}


#pragma mark - Access Handler

/*
    The callback function for the respective URL will be called at least twice.

    1st Call:
        The first call happens after the server has received the headers.
         The client should use the last void** argument to
         store internal context for the session.

         The first call to the callback function is mostly for this type of
         initialization and for internal access checks.

         At least, the callback function should "remember" that
         the first call with just the headers has happened.

         Queueing a response during the first call (for a given connection)
            should only be used for errors.

                                          |  If the client queues a response during this first call,
                                          |     a 100 CONTINUE response will be suppressed,
                                          |     he request body will not be read and the connection
                                          |     will be closed after sending the response.

        After the first call, the callback function will be called
            with upload data. Until *upload_data_size is zero,
            the callback may not queue a response, any such attempt will fail.

        The callback function should update *upload_data_size to
            indicate how many bytes were processed.

        Depending on available buffer space, incremental processing of the
            upload maybe required.

    2nd Call (or once upload_data has been processed):

        Once all of the upload data has been processed,
            MHD will call the callback a second time with *upload_data_size
            being zero. At this point, the callback should
            queue a "normal" response.

        If queueing a response is not possible,
            the callback may either block or simply not queue a response
            depending on the threading mode that is used.

        If the callback does not queue a response at this point,
            MHD will either (eventually) timeout the connection
            or keep calling it.


 */

int
    gtkAccessHandlerCallback(
          void*                   cls,
          struct MHD_Connection  *connection,
          const char             *url,
          const char             *method,
          const char             *version,
          const char             *upload_data,
          size_t                 *upload_data_size,
          void                   **con_cls)
{
    int returnCode = 0;

    #if !HAS_ARC
    NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];
    #endif

    if ( *con_cls == NULL )
    {
        returnCode =
        HandleUnconnectedCallback(
             cls
            ,connection
            ,url
            ,method
            ,version
            ,upload_data
            ,upload_data_size
            ,con_cls);
    }
    else
    {
        returnCode = HandleConnectedCallback(
             cls
            ,connection
            ,url
            ,method
            ,version
            ,upload_data
            ,upload_data_size
            ,con_cls);
    }

    #if !HAS_ARC
    [pool drain];
    #endif


    return returnCode;
}

#pragma mark "Manage Requests"

-(int)
manageRequest:(GTKMHDHTTPRequest*)aRequest
forPath:(NSString*)aPath
withData:(const char*)upload_data
withDataLength:(size_t*)upload_data_size
onConnection:(void*)connection // onConnection:(struct MHD_Connection*)connection
withContext:(void**)con_cls
    // withContext:(void**)theExtraArgument
{
    BOOL debug = NO;

    debug = [self shouldDebugPath:[aRequest path]];

    if ( *upload_data_size != 0 )
    {
        DebugLog(@"Will handle upload data of size (ulong)  : %zu\n", *upload_data_size);  // prints as unsigned decimal
        DebugLog(@"Will handle upload data of size (hex)    : %zx\n", *upload_data_size);  // prints as hex
        DebugLog(@"Will handle upload data of size (signed) : %zd\n", *upload_data_size);  // prints as signed decimal

        // return [(GTKHTTPPostRequest*)request
        //         appendBytes:upload_data
        //         length:upload_data_size
        //         error:&internalError];


        return [self
                handleRequest:aRequest
                forPath:aPath
                withData:upload_data
                withDataLength:upload_data_size
                onConnection:connection
                withContext:con_cls];
    }
    else
    {
        DebugLog(@"No data: Preparing to respond to request.");
        // return [[aRequest server] respondToRequest:aRequest];
        return [self
                handleRequest:aRequest
                forPath:aPath
                onConnection:connection
                withContext:con_cls];

//        int extractedExpr = [self
//                             handleRequest:aRequest
//                             forPath:aPath
//                             onConnection:connection
//                             withContext:con_cls];
//
//        return extractedExpr;
    }
}

-(int)
handleRequest:(GTKMHDHTTPRequest*)aRequest
forPath:(NSString*)aPath
withData:(const char*)upload_data
withDataLength:(size_t*)upload_data_size
onConnection:(struct MHD_Connection*)connection
withContext:(void**)con_cls
{
    DEBUG_NO

    debug = [[aRequest server] shouldDebugPath:aPath];

    GTKHTTPPostRequest* postRequest = (GTKHTTPPostRequest*)aRequest;
    void*               processor   = [postRequest processor];

    if (processor)
    {
        DebugLog(@"Will `post_process` with %@/%p", postRequest, [postRequest processor]);
        DebugLog(@"Data to process...size: %zu", *upload_data_size);
        // https://stackoverflow.com/questions/2137779/how-do-i-print-a-non-null-terminated-string-using-printf
        DebugLog(@"%.*s", (int)*upload_data_size, upload_data);
        BOOL didProcess = NO;

        didProcess = MHD_post_process (processor,upload_data ,*upload_data_size);

        if (didProcess == MHD_YES)
        {
            DebugLog(@"Did `post_process`");
            *upload_data_size = 0;
            return MHD_YES;
        }
        else
        {
            DebugLog(@"POST PROCESS FAIL...");
            return MHD_NO;
        }
    }
    else
    {
        DebugLog(@"Will get JSON data");
        NSMutableData* jsonData = [postRequest payload];

        DebugLog(@"Will apend to JSON data %@", jsonData);
        [jsonData appendBytes:upload_data
                  length:(NSUInteger)*upload_data_size];
        DebugLog(@"Did apend to JSON data");

        *upload_data_size = 0;

        return MHD_YES;
    }
}

-(void)
actuallyToUser:(id)theUser
pushObject:(id)theObject
path:(NSString*)thePath
excludeSession:(id)sessionToExclude
asJSONSchema:(BOOL)asJsonSchema
{
    DEBUG_NO

    id otherWebSocketConnections = [self webSocketConnectionsForUser:theUser];

    if (otherWebSocketConnections && [otherWebSocketConnections count])
    {
        NSData* dataToSend = nil;

        DebugLog(@"Consider forwarding to: %@", otherWebSocketConnections);

        for (GTKMHDWebSocketConnection* connection in otherWebSocketConnections)
        {
            id sessionToRecv = [connection session];
            
            DebugLog(@"Will test if sessionToRecv: %@ == sessionToExclude: %@", sessionToRecv, sessionToExclude);
            
            BOOL shouldExcludeSession = [sessionToRecv isEqual:sessionToExclude];

            if (shouldExcludeSession)
            {
                DebugLog(@"Did exclude session. :)");
            }
            else
            {
                DebugLog(@"Will send data to session: %@", sessionToRecv);
                
                if (!dataToSend)
                {
                    DebugLog(@"Will prepare `dataToSend`");
                    
                    GTKWebSocketRequest* request = AUTORELEASE([[GTKWebSocketRequest alloc] init]);
                    
        
                    [request setPath:thePath];

                    NSUUID* oRequestID = [NSUUID UUID];
    
                    [request setRequestID:oRequestID];
                    [request setHTTPCode:GTKHTTPCodeOK];
                    [request setHTTPMethod:GTKHTTPMethod_Get];
                    
                    DebugLog(@"Request ID   : %@ - %@", oRequestID, [oRequestID UUIDString]);
                    DebugLog(@"Request Path : %@", thePath);

                    NSData* serializedObject = nil;

                    if (asJsonSchema)
                    {
                        DebugLog(@"Will call `+GTKJSONSchemaStyleWriter:dataForObject`");
                        serializedObject = [GTKJSONSchemaStyleWriter dataForObject:theObject];
                    }
                    else
                    {
                        DebugLog(@"Will call `+GTKJSONSchemaStyleWriter:dataForObject`");
                        serializedObject = [GTKJSONWriter dataForObject:theObject];
                    }
    
                    DebugLog(@"Will send data of length: %lu - %@", [serializedObject length], serializedObject);
                    DebugLog(@"Bytes to send: %.*s", [serializedObject length], [serializedObject bytes]);
                    
                    [request setPayload:serializedObject];

                    GTKWebSocketFrame* asFrame = [request asWebSocketFrame];
                    
                    dataToSend = [asFrame asData];
                }
                
                DebugLog(@"Will send data to session: %@", sessionToRecv);
                
                [connection sendData:dataToSend];
                
                DebugLog(@"Did send data to session: %@", sessionToRecv);
            }
        }
    }

    RELEASE(theUser);
    RELEASE(theObject);
    RELEASE(thePath);
    RELEASE(sessionToExclude);
}


-(void)
toUser:(id)theUser
pushObject:(id)theObject
path:(NSString*)thePath
excludeSession:(id)sessionToExclude
asJSONSchema:(BOOL)asJsonSchema
{
    DEBUG_NO

    RETAIN(theUser);
    RETAIN(theObject);
    RETAIN(thePath);
    RETAIN(sessionToExclude);
    

    BOOL useDispatchQueue = NO;

    if (useDispatchQueue)
    {
        if (!_backToClientQueue)
        {
            _backToClientQueue = dispatch_queue_create("back_to_client_queue", DISPATCH_QUEUE_SERIAL);
        }


        dispatch_async(_backToClientQueue, ^{
            NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];

            [self actuallyToUser:theUser
                  pushObject:theObject
                  path:thePath
                  excludeSession:sessionToExclude
                  asJSONSchema:asJsonSchema];

            [pool drain];
        });
    }
    else
    {
        [self actuallyToUser:theUser
              pushObject:theObject
              path:thePath
              excludeSession:sessionToExclude
              asJSONSchema:asJsonSchema];
    }
}


-(int)
respondToRequest:(GTKMHDHTTPRequest*)aRequest
withObject:(id)toEncode
{
    BOOL debug      = NO;
    int  returnCode = 0;

    if ([toEncode isKindOfClass:[GTKMHDUpgradeToWebSocketResponse class]])
    {
        DebugLog(@"Will create UPGRADE response.");

        struct MHD_Response* response
                =  MHD_create_response_for_upgrade(
                        UpgradeToWebSocket,
                        aRequest);

        MHD_add_response_header (response
                , "Upgrade"
                , "websocket");

        DebugLog(@"Sending SWITCHING protocols.");

        int statusCode =
            MHD_queue_response(
                    [aRequest connection],
                    MHD_HTTP_SWITCHING_PROTOCOLS,
                    response);

        return statusCode;
    }

    DebugLog(@"Will wrap object.");
    GTKHTTPServerResponse* response
        = [GTKHTTPServerResponse
                wrapObject:toEncode
                forRequest:aRequest
                debug:debug];

    [aRequest setResponse:response];

    DebugLog(@"Did wrap object. Got: %@", response);

    DebugLog(@"Will queue.");
    DebugLog(@"Response length: %lu", [response length]);
    DebugLog(@"Response bytes: %s", (char*)[response bytes]);

    // Changed to RESPMEM_MUST_COPY
    // Was getting garbled response when using
    // RESPMEM persistent after embeembedding
    struct MHD_Response* responseForMHD = MHD_create_response_from_buffer (
		    [response length] ,
		    (void*)[response bytes],
		    // MHD_RESPMEM_PERSISTENT);
		    MHD_RESPMEM_MUST_COPY);
    DebugLog(@"Did queue.");

    DebugLog(@"Will get application headers.");
    NSDictionary* applicationHeaders = [response applicationHeaders];
    DebugLog(@"Did get application headers %@.", applicationHeaders);

    for (id headerKey in applicationHeaders)
    {
        DebugLog(@"Will get header: %@", headerKey);
        id headerValue = [applicationHeaders objectForKey:headerKey];
        DebugLog(@"Did get header: %@/%@", headerKey, headerValue);

        DebugLog(@"Will add response header...");
        MHD_add_response_header (responseForMHD,
                [[headerKey asString] UTF8String],
                [[headerValue asString] UTF8String]);
        DebugLog(@"Did add response header...");
    }


    DebugLog(@"Will get cookies");
    NSArray* cookies = [response cookies];
    DebugLog(@"Did get cookies");

    for (GTKHTTPCookie *cookie in cookies)
    {
        DEBUG_PUT("Will add (cookie) response header...");
        NSString* cookieString = [cookie asString];
        MHD_add_response_header(responseForMHD,
                [[HTTPResponseHeader setCookie] UTF8String],
                [cookieString UTF8String]);
        DEBUG_PUT("Did add (cookie) response header...");
    }


    DebugLog(@"Will set connection header.");
    MHD_add_response_header (responseForMHD
            ,"Connection"
            , "Keep-Alive");
    DebugLog(@"Did set connection header.");


    DEBUG_PUT("Will set content type header");
    MHD_add_response_header (responseForMHD
            ,"Content-Type"
            ,[[response mimeType] UTF8String]);
    char mimebuf[200];
    bzero(mimebuf, 200);

    DebugLog(@"Did set content type header to %s", [[response mimeType] UTF8String]);

    NSUInteger statusCode = [response httpResponseCode];

    DebugLog(@"Will queue response with status code: %lu\n", (unsigned long)statusCode);

    returnCode
        = MHD_queue_response(
                [aRequest connection]
                // connection
                ,statusCode
                ,responseForMHD);

    DebugLog(@"Did queue response with status code: %d\n", statusCode);


    // DEBUG_PUTS("Will destroy response...");
    // MHD_destroy_response(response);
    // DEBUG_PUTS("Did destroy response...");

    switch (returnCode) {
        case MHD_YES:
            DebugLog(@"%s: MHD Queue Response Success %d\n", [NSStringFromClass([self class]) UTF8String], returnCode);
            break;
        case MHD_NO:
            DebugLog(@"%s: MHD Queue Response Failure %d\n", [NSStringFromClass([self class]) UTF8String], returnCode);
            break;
        default:
            DebugLog(@"%s: MHD Queue Unknown State %d\n",    [NSStringFromClass([self class]) UTF8String], returnCode);
            break;
    }

    // RELEASE(responseObject);

    return returnCode;
}

void LogResponseObjectForPath(GTKHTTPServer* server, id aPath, id responseObject, BOOL debug) {
    if (debug && [responseObject isKindOfClass:[NSString class]])
    {
        NSString* responseString = (NSString*)responseObject;

        if ([responseString length] > 100)
        {
            DebugLog(@"Response object (String) is %s.", [[responseString substringWithRange:NSMakeRange(0,100)] UTF8String]);
        }
        else
        {
            DebugLog(@"Response object (String) is %s.", [responseString UTF8String]);
        }
    }
    else if (debug && [responseObject isKindOfClass:[NSData class]])
    {
        DebugLog(@"Response object is NSData of length %lu.", [(NSData*)responseObject length]);
    }
    else if (debug && !responseObject)
    {
        DEBUG_PUT("Response object is NIL.");
    }
    else
    {
        if ([responseObject respondsToSelector:@selector(length)] && ([responseObject length] > 100))
        {
            DebugLog(@"%@: Response Obj - @%@ is %@ — (Response Object Too Long)"
                , NSStringFromClass([server class])
                , aPath
                , NSStringFromClass([responseObject class]));

        }
        else
        {
            DebugLog(@"%@: Response Obj - @%@ is %@ — %@"
                , NSStringFromClass([server class])
                , aPath
                , NSStringFromClass([responseObject class])
                , responseObject);
        }
    }
}

-(int)
handleRequest:(GTKMHDHTTPRequest*)aRequest
forPath:(NSString*)aPath
onConnection:(struct MHD_Connection*)connection
withContext:(void**)con_cls
{
	BOOL debug        = YES;
    id responseObject = nil;

    if (debug && ([aRequest HTTPMethod] == GTKHTTPMethod_Post))
    {
        DebugLog(@"======== IS POST =========");
        DebugLog(@"Request JSON Data: %@",      [aRequest payload]);
        DebugLog(@"Request Post Data Dict: %@", [aRequest postDataDict]);
    }

    id toReturn = nil;

    GTKHTTPExceptionLocation exceptionLocation = GTKHTTPExceptionLocationResponseFetching;

    @try
    {
        DEBUG_PUT("Will get response object.");
        // NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];

        responseObject = [[self delegate] serveRequest:aRequest];

        if (!responseObject)
        {
            responseObject = [NSError
                    errorWithDomain:@"ServerError"
                    code:404
                    userInfo:@{
                            HTTPResponseCode        : @404,
                            @"Reason"               : @"Resource not found. No response obj.",
                    }];
        }

        LogResponseObjectForPath(self, aPath, responseObject, debug);


        exceptionLocation = GTKHTTPExceptionLocationResponseEncoding;

        toReturn = [self respondToRequest:aRequest
                         withObject:responseObject];
    }
    @catch (NSException* exception)
    {
		#if !__has_feature(objc_arc)
            puts("\n\nGOT EXCEPTION!!!\n\n");
		#endif

                DebugLog(@"%@: Exception (serving %@): %@"
                   , NSStringFromClass([self class])
                   , aPath
                   , exception);

                DebugLog(@"Call stack symbols:%@", [exception callStackSymbols]);
                DebugLog(@"Call stack addr:%@", [exception callStackReturnAddresses]);



                // -(NSArray<NSNumber* >*)callStackReturnAddresses;
                // \\ // \\ // \\ // \\ // \\ // \\ // \\ //
                // An array of NSNumber objects encapsulating NSUInteger values.
                // Each value is a call frame return address.
                // The array of stack frames starts at the point at which the
                // exception was first raised, with the first items being
                // the most recent stack frames.
                // NSException subclasses posing as the NSException class
                // or subclasses or other API elements that interfere with
                // the exception-raising mechanism may not get this information.

                // -(NSArray<NSString* >*)callStackSymbols;
                // \\ // \\ // \\ // \\ // \\ // \\ // \\ //
                // An array of strings describing the call stack backtrace at
                // the moment the exception was first raised.
                // The format of each string is determined by API:
                //      backtrace_symbols()
                // \ / \ / \ / \ / \ / \ / \ / \ / \ / \ / \


                // [aRequest setResponseObject:[self errorPage:exception]];

                NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];

                if (!_capturedExceptions)
                {
                    _capturedExceptions = [[NSMutableArray alloc] init];
                }

                GTKHTTPCapturedException* capturedException = [[GTKHTTPCapturedException alloc] init];

                [capturedException setException:exception];
                [capturedException setRequest:aRequest];
                [capturedException setResponse:responseObject];
                [capturedException setExceptionLocation:exceptionLocation];

                [_capturedExceptions addObject:capturedException];

                RELEASE(capturedException);


                #if DEBUG
                    LOG_HTTPD_DEBUG(@"Converting exception to error WITH sensitive info.");
                    responseObject = [exception asErrorWithCode:501 domain:nil includeSensitiveInfo:YES];

                #else
                    LOG_HTTPD_DEBUG(@"Converting exception to error NO sensitive info.");
                    responseObject = [exception asErrorWithCode:501];
                #endif

                toReturn = [self respondToRequest:aRequest
                                 withObject:responseObject];

                [pool drain];
     }




    return toReturn;

}




// MARK: STOP / START HTTP

/**
 Start a webserver on the given port.  Variadic version of
 #MHD_start_daemon_va.
  1:FLAGS —
    flags combination of `enum MHD_FLAG` values
  2:BIND_TO_PORT
    use '0' to bind to random free port,
    ignored if provied
        MHD_OPTION_SOCK_ADDR
        MHD_OPTION_LISTEN_SOCKET
    or if specified
        MHD_USE_NO_LISTEN_SOCKET
  3:AccessPolicyCallback to call to
    An APC checks which clients will be allowed to connect.
    You can pass NULL to accept connections from any IP.
  4:Extra argument for AccessPolicyCallback (hence self)
  5:DH Handler called for all requests (repeatedly)
  6:Extra argument for DH handler

 returns...
    NULL on error,
    aHANDLE to daemon on success

*/

-(BOOL)
start
{
    return [self startOrError:nil];
}

-(BOOL)
startOrError:(NSError**)outErr
{
    return [self startSecureHTTPServerUsingKeyFile:[self keyFile] andCertifcateFile:[self certificateFile]];
}


 void* GTK_URILogger(
    void*                    cls,
    const char*              uri,
    struct MHD_Connection*   con)
{
    AlwaysLog(@"URI Logger: %s", uri);

    return NULL; // If value is non-null—it will become the first arg to GTK_AccessHandler
}

void GTK_MHDLogger(
    void*       cls,
    const char* fm,
    va_list     ap)
{
    NSString* fmt = [NSString stringWithUTF8String:fm];
 
    NSString* toPrint = AUTORELEASE([[NSString alloc]
        initWithFormat:fmt
        arguments:ap]);
        
    NSLog(@"%@", toPrint);
}


-(BOOL)
startSecureHTTPServerUsingKeyFile:(char*)keyFile
andCertifcateFile:(char*)certFile
{
    int attempts=0;
    /**
     * Print errors messages to custom error logger or to `stderr` if
     * custom error logger is not set.
     * @sa ::MHD_OPTION_EXTERNAL_LOGGER
     */
    // MHD_USE_ERROR_LOG = 1,

    /**
     * Run in debug mode.  If this flag is used, the library should
     * print error messages and warnings to `stderr`.
     */
    // MHD_USE_DEBUG = 1

    int flags = 0;



    flags |= MHD_USE_SELECT_INTERNALLY;
    flags |= MHD_USE_TLS;
    // flags |= MHD_USE_SSL;
    flags |= MHD_USE_ERROR_LOG;
    flags |= MHD_ALLOW_UPGRADE;
    flags |= MHD_ALLOW_SUSPEND_RESUME;

    BOOL debug = YES;

    if (debug) {
        flags |= MHD_USE_DEBUG;
    }


    /*
    MHD_OPTION_EXTERNAL_LOGGER
    Use the given function for logging error messages. This option must be followed by two arguments; the first must be a pointer to a function of type ’void fun(void * arg, const char * fmt, va_list ap)’ and the second a pointer of type ’void*’ which will be passed as the "arg" argument to "fun".

    Note that MHD will not generate any log messages without the MHD_USE_DEBUG flag set and if MHD was compiled with the "–disable-messages" flag.
     */

    if (MHD_is_feature_supported(MHD_FEATURE_MESSAGES))
    {
        AlwaysLog(@"MHD — was compiled with messages. Should print errors.");
    }
    else
    {
        AlwaysLog(@"MHD — WARNING — No messages.");
    }

    BOOL allowFindFreePort = NO;

    // [self setPort:80];
    // [self setPort:443];





    while ( ![self httpd] && attempts < 50) {
        NSLog(@"Attempt: %d on Port: %d", attempts, [self port]);
        [self setHttpd:
            MHD_start_daemon (
                 flags
                ,[self port]                 // PORT#
                ,gtkAcceptPolicyCallback     // APC#      — AccessPolicyCallback
                   ,SAFE_CAST(void*, self)                    // APC_CLS#  — Extra Argument
                ,gtkAccessHandlerCallback    // DH#       - AccessHandlerCallback
                    ,SAFE_CAST(void*, self)                    // DH_CLS#         — Extra Argument
                // VARIADIC OPTIONS
                // AP list of options (type-value pairs, terminated with #MHD_OPTION_END).
                // LOGGER MUST BE 1st OPTION
                ,MHD_OPTION_EXTERNAL_LOGGER
                    ,GTK_MHDLogger
                    ,self
                //
                ,MHD_OPTION_NOTIFY_COMPLETED
                    ,gtkRequestCompletedCallback
                    ,SAFE_CAST(void*, self)
                ,MHD_OPTION_THREAD_POOL_SIZE
                    ,[self threadPoolSize]
                ,MHD_OPTION_HTTPS_MEM_KEY
                    ,keyFile
                ,MHD_OPTION_HTTPS_MEM_CERT
                    ,certFile

                // ,MHD_OPTION_NOTIFY_CONNECTION
                //     ,MHD_NotifyConnectionCallback
                //     ,firstArgumentToNotifyConnectionCallback
                // MHD_OPTION_UNESCAPE_CALLBACK,
                //      ,GTK_URIUnescaper
                //      ,firstArgumentToURIUnescaper
                //,MHD_OPTION_URI_LOG_CALLBACK
                //
                // ,MHD_OPTION_PER_IP_CONNECTION_LIMIT,
                //    ,10
                // MHD_OPTION_HTTPS_MEM_TRUST, root_ca_pem,
                // —> IF YOU WANT TO MAKE EVERYONE EMIT CERTICATES
                // ,MHD_USE_ERROR_LOG,
                // MHD_USE_DEBUG,
                ,MHD_OPTION_END)
        ];

        if (![self httpd])
        {
            if (allowFindFreePort) {
                [self setPort:[self port]+1];
            }
            attempts++;
        }
    }




    if ([self httpd] && ([self httpd] != NULL))
    {
        AlwaysLog(@"%s === Server started (Port=%d) (Pointer=%p)"
                ,[NSStringFromClass([self class]) UTF8String]
                ,[self port]
                ,[self httpd]
                );

        GTKSocketManager* socketManager = [self manySocketManager];

        AlwaysLog(@"Did start web socket manager: %@", socketManager);

        return YES;
    }
    else
    {
        AlwaysLog(@"FAILURE!!! Couldn't start server: %@", self);
        return NO;
    }
}

-(void)
stopHttpd
{
    if ([self httpd])
    {
        AlwaysLog(@"%@ — Stopping httpd: %p"
                ,NSStringFromClass([self class])
                ,[self httpd]);

        MHD_stop_daemon (
            [self httpd]);

        AlwaysLog(@"%@ — Did stop %p"
            ,NSStringFromClass([self class])
            ,[self httpd]);

        // Maybe here? pointer being freed was not allocated
        // [self setHttpd:NULL];
    }
    else
    {
        AlwaysLog(@"%@ - No HTTPD server to stop.", NSStringFromClass([self class]));
    }
}

-(void)
stop
{
    // [self stopBonjour];
    [self stopHttpd];
}


#if !HAS_ARC
-(void)
dealloc
{
    [self stop];
    [super dealloc];
}
#endif

// MARK: - INFO ON OTHER KINDS OF HTTP METHODS...

// MARK: OPTIONS...

//      To find out which request methods a server supports,
//          one can use curl and issue an OPTIONS
//  ---------------------------------------------------
//        E.G
//        HTTP/1.1 204 No Content
//        Allow: OPTIONS, GET, HEAD, POST
//        Used in CORS (Cross-Orgin-Resource-Sharing)
//        OPTIONS requests are what we call pre-flight requests in Cross-origin resource sharing (CORS).
//        https://stackoverflow.com/questions/29954037/why-is-an-options-request-sent-and-can-i-disable-it
//        https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS
//        https://enable-cors.org

// MARK: PROPFIND options...
//        Basically a machine readable GET-REQUEST for
//        WebDAV applications
//              WebDav: Web Distributed Authoring and Versioning
//                  is an extension of the HTTP protocol that
//                  facilitates collaboration between users in editing
//                  and managing documents and files stored on
//                  World Wide Web servers.
//        https://duckduckgo.com/?q=http+propfind&t=osx&ia=web
//        https://webmasters.stackexchange.com/questions/59211/what-is-http-method-propfind-used-for#59212
//        https://github.com/dmfs/davwiki/wiki/PROPFIND
//        https://en.wikipedia.org/wiki/WebDAV#Server_support
//        CalDAV and CardDAV are based on WebDAV
//        https://en.wikipedia.org/wiki/CalDAV

// || !strcmp("OPTIONS", method) )
// || !strcmp("PATCH", method)


- (NSString*)
mimeType
{
    if (
        self != [self delegate]
             && [[self delegate] respondsToSelector:@selector(mimeType)])
    {
        return [[self delegate]
                            mimeType];
    }
    if ( _defaultMimeType)
    {
        return _defaultMimeType;
    }
    // return @"text/plain";
    // return [MimeType plainText];
    return [MimeType html];
}

-(id)
serveRequest:(GTKMHDHTTPRequest*)aRequest
{
    NSString* aPath = [aRequest path];

    return [NSString stringWithFormat:
                @"<html><body>"
                @"%@ is serving path: %@"
                @"</body></html>"
                    , NSStringFromClass([self class])
                    , aPath];
}


-(void)
runInNSRunLoop
{
    [self runInNSRunLoopInForeground:NO];
}

-(void)
runInNSRunLoopInForeground:(BOOL)runInForeground
{
    DEBUG_NO
    /*
    NSTimeInterval runLoopTimeInterval = 0.1000; // mag++
    // NSTimeInterval runLoopTimeInterval = 0.0100; // mag++
    // NSTimeInterval runLoopTimeInterval = 0.0010; // mag++
    // NSTimeInterval runLoopTimeInterval = 0.0001; // minValue macOS

    NSTimer* timer
         = [NSTimer timerWithTimeInterval:runLoopTimeInterval
             target:self
             selector:@selector(doRunLoop)
             userInfo:nil
             repeats:YES];

    ASSIGN_ID_gtk(_runLoopTimer, timer);

     [[NSRunLoop mainRunLoop]
         addTimer:_runLoopTimer
         forMode:NSDefaultRunLoopMode];
    //
    // [[NSRunLoop mainRunLoop] run];
    */

    _shouldKeepRunning = YES;

    NSRunLoop*  serverRunLoop
        = [NSRunLoop mainRunLoop];

    // @try
    // {
        if (runInForeground)
        {
             AlwaysLog(@"Server will keep running. To quit, press 'q'.");
             AlwaysLog(@"!!! Note: this dosen't actually work...just user Ctrl-C");
        }

        while (_shouldKeepRunning && [serverRunLoop runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]])
        {
            // AlwaysLog(@"Running run loop...");
            // if ([self httpd] == NULL)
            // {
            //     break;
            // }
            if (runInForeground)
            {
                // if (getchar() == 'q')
                // {
                //     _shouldKeepRunning = NO;
                // }
            }
        };
        AlwaysLog(@"Exiting run loop...");
    // }
    // @catch (NSException* exception)
    // {
    //     AlwaysLog(@"Will quit because of exception!!!");
    //     AlwaysLog(@"Name               : %@", [exception name]);
    //     AlwaysLog(@"Reason             : %@", [exception reason]);
    //     AlwaysLog(@"User Info          : %@", [exception userInfo]);
    //     if ([exception respondsToSelector:@selector(callStackReturnAddresses)])
    //     {
    //         AlwaysLog(@"Stack Address Info : %@", [exception callStackReturnAddresses]);
    //     }
    //     if ([exception respondsToSelector:@selector(callStackSymbols)])
    //     {
    //         AlwaysLog(@"Stack Symbols Info : %@", [exception callStackSymbols]);
    //     }
    // }
}

-(void)
doRunLoop
{
    AlwaysLog(@"Doing RUN LOOP");
    if ([self httpd] == NULL)
    {
        // Just. Keep. Going :)
        _shouldKeepRunning = NO;
        // CFRunLoopStop(CFRunLoopGetCurrent()); // Apparently the only way to stop the run loop when run via timer...
        exit(1);
    }
}


@end



@implementation GTKHTTPServer (testing)

+(NSArray*)
testSelectors
{
    NSMutableArray* tests
        = [NSMutableArray array];

    [tests addObjectsFromArray:@[
        @"tHandleRequestsWhenPathIsNotNil"
    ]];

    BOOL wasAbleToAddTestConnectionsToLibMicroHTTPD
        = NO;

    if (wasAbleToAddTestConnectionsToLibMicroHTTPD)
    {
        [tests addObjectsFromArray:@[
             @"tUnconnectedCallbackReturnsAnIntAndAttachesAnObject"
            ,@"tConnectedCallbackReturnsMHD_NOWhenUploadSizeIsNot0"
            ,@"tConnectedCallbackReturnsMHD_YESWith0UploadSize"
        ]];
    }

    return tests;
}

+(void)
tHandleRequestsWhenPathIsNotNil
{
    GTKHTTPServer* cls
        = [[GTKHTTPServer alloc] init];

    GTKHTTPGetRequest* aRequest
        = [GTKHTTPGetRequest
            requestOnConnection:nil
            session:nil];

    [aRequest setPath:@"/"];

    id requestResult = [cls serveRequest:aRequest];

    id message
        = [NSString stringWithFormat:
            @"%@ Does not handle the nil case"
            ,[cls className]];

    EXPECT_NOT_NIL(requestResult, message);
}


@end

/*


+(void)
tUnconnectedCallbackReturnsAnIntAndAttachesAnObject
{
    GTKHTTPServer *cls
        = [[GTKHTTPServer alloc] init];

    void  *connection
        = NULL;

    const char *upload_data
        = "SOMETHING";

    size_t *upload_data_size
        = NULL;

    *upload_data_size
        = strlen(upload_data);

    void *con_cls
        = NULL;

#if HAS_ARC
    int callStatus
        = HandleUnconnectedCallback(
            (__bridge void*)cls,                   // void                   *cls,
            connection,            // struct MHD_Connection  *connection,
            "/",                   // const char             *url,
            "GET",                 // const char             *method,
            MHD_HTTP_VERSION_1_1,  // const char             *version,
            NULL,                  // const char             *upload_data,
            NULL,                  // size_t                 *upload_data_size,
            (void**)&con_cls);     // void                   **con_cls);
#else
    int callStatus
        = HandleUnconnectedCallback(
            (void*)cls,                   // void                   *cls,
            connection,            // struct MHD_Connection  *connection,
            "/",                   // const char             *url,
            "GET",                 // const char             *method,
            MHD_HTTP_VERSION_1_1,  // const char             *version,
            NULL,                  // const char             *upload_data,
            NULL,                  // size_t                 *upload_data_size,
            (void**)&con_cls);     // void                   **con_cls);
#endif

    EXPECT_INT(MHD_YES, callStatus, @"Returning something other than MHD_NO when upload size in not 0");
    EXPECT_NOT_NIL(con_cls, @"Did not set CON_CLS inside unconnected callback.");
}

+(void)
    tConnectedCallbackReturnsMHD_NOWhenUploadSizeIsNot0
{
    GTKHTTPServer *cls
        = [[GTKHTTPServer alloc] init];

    void  *connection
        = NULL;

    const char *upload_data
        = "SOMETHING";

    size_t *upload_data_size = 0x00;

    *upload_data_size
        = strlen(upload_data);

    GTKHTTPPostRequest *con_cls
        = [[GTKHTTPPostRequest alloc] init];

#if HAS_ARC
    int callStatus
        = HandleConnectedCallback(
            (__bridge void*)cls,                   // void                   *cls,
            connection,            // struct MHD_Connection  *connection,
            "/",                   // const char             *url,
            "GET",                 // const char             *method,
            MHD_HTTP_VERSION_1_1,  // const char             *version,
            upload_data,           // const char             *upload_data,
            upload_data_size,      // size_t                 *upload_data_size,
            (GTKHTTPPostRequest**)&con_cls);             // void                   **con_cls);
#else
    int callStatus
        = HandleConnectedCallback(
            cls,                   // void                   *cls,
            connection,            // struct MHD_Connection  *connection,
            "/",                   // const char             *url,
            "GET",                 // const char             *method,
            MHD_HTTP_VERSION_1_1,  // const char             *version,
            upload_data,           // const char             *upload_data,
           upload_data_size,      // size_t                 *upload_data_size,
            (void**)&con_cls);     // void                   **con_cls);
#endif

    EXPECT_INT(MHD_YES, callStatus, @"Returning something other than MHD_NO when upload size in not 0");
}

+(void)
     tConnectedCallbackReturnsMHD_YESWith0UploadSize
{
    GTKHTTPServer *cls
        = [[GTKHTTPServer alloc] init];

    void  *connection
        = NULL;

    GTKHTTPGetRequest *con_cls
        = [GTKHTTPGetRequest
            requestOnConnection:nil
            session:nil];

#if HAS_ARC
    int callStatus
        = HandleConnectedCallback(
            (__bridge void*)cls,  // void                   *cls,
            connection,            // struct MHD_Connection  *connection,
            "/",                   // const char             *url,
            "GET",                 // const char             *method,
            MHD_HTTP_VERSION_1_1,  // const char             *version,
            NULL,                  // const char             *upload_data,
            0,                     // size_t                 *upload_data_size,
            &con_cls);     // void                   **con_cls);
#else
    int callStatus
        = HandleConnectedCallback(
            cls,                   // void                   *cls,
            connection,            // struct MHD_Connection  *connection,
            "/",                   // const char             *url,
            "GET",                 // const char             *method,
            MHD_HTTP_VERSION_1_1,  // const char             *version,
            NULL,                  // const char             *upload_data,
            0,                     // size_t                 *upload_data_size,
            (void**)&con_cls);     // void                   **con_cls);
#endif

    EXPECT_INT(MHD_YES, callStatus,   @"Returning something other than MHD_YES on when upload size is 0");
}

@end
*/
