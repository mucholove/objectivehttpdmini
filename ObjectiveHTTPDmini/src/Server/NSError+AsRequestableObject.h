//
//  NSError+AsRequestableObject.h
//  ObjectiveHTTPDmini
//
//  Created by Gustavo Tavares on 11/24/20.
//  Copyright © 2020 Gustavo Tavares. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GTKError : NSError
@end

@interface GTKError (AsJSON)
@end

@interface NSError (AsHTML)
-(NSUInteger)httpCode;
-(NSString*)asHTML;
-(NSString*)asJSON;
@end

