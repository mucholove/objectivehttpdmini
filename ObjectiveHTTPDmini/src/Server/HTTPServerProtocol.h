//
//  HTTPServerProtocol.h
//  ObjectiveHTTPDmini
//
//  Created by Gustavo Tavares on 5/29/20.
//  Copyright © 2020 Gustavo Tavares. All rights reserved.
//

@protocol HTTPServerProtocol
-(int)
port;
-(void)
setPort:(int)newVar;

-(BOOL)
startSecureHTTPServerUsingKeyFile:(char*)keyFile
andCertifcateFile:(char*)certFile;

-(BOOL)
startSecureHTTPServerUsingKeyFile:(char*)keyFile
andCertifcateFile:(char*)certFile
withAuthDelegate:(id<GTKServerAuthDelegate>)anAuthDelegate;

-(void)
stop;

-(NSString*)
stringFromData:(NSData*)putData
printingDebugInfo:(BOOL)logDebug;
@end
