//
//  GTKMHDWebSocketConnection.h
//  ObjectiveHTTPDmini
//
//  Created by Gustavo Tavares on 3/12/21.
//  Copyright © 2021 Gustavo Tavares. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <GTKFoundation/GTKFoundation.h>
#import <GTKSockets/GTKSockets.h>

@class
    GTKHTTPRequest,
    GTKHTTPServer,
    GTKSocketManager,
    GTKSocketQueueItem,
    GTKMHDWebSocketConnection,
    GTKWebSocketFrame,
    GTKWebSocketMessage;

@interface GTKMHDWebSocketConnection  : NSObject <SocketManagerItem, GTKWebSocketDataProcessorDelegate>
ACCESSOR_H(BOOL,                                 shouldRetainFrames,    setShouldRetainFrames);
ACCESSOR_H(id<GTKWebSocketConnectionDelegate>,   delegate,              setDelegate);
ACCESSOR_H(GTKHTTPRequest*,                      request,               setRequest);
ACCESSOR_H(NSData*,                              extraInData,           setExtraInData);
ACCESSOR_H(BOOL,                                 isOpen,                setIsOpen);

ACCESSOR_H(NSDate*, timeSinceLastPing, setTimeSinceLastPing);
ACCESSOR_H(NSDate*, timeSinceLastPong, setTimeSinceLastPong);

-(id)
user;

-(void)
sendData:(NSData*)theDataToSend;


ACCESSOR_H(GTKSocketManager*, manySocketManager,     setManySocketManager);
ACCESSOR_H(void*,             connection,            setConnection);
ACCESSOR_H(int,               socket,                setSocket);
ACCESSOR_H(void*,             upgradeResponseHandle, setUpgradeResponseHandle);

-(instancetype)
initWithRequest:(GTKHTTPRequest*)theRequest
MHDConnection:(void*)theConnection
socket:(int)theSocket
responseHandle:(void*)theUpgradeResponseHandle;

// ACCESSOR_H(struct MHD_Connection*,             connection,            setConnection);
// ACCESSOR_H(MHD_socket,                         socket,                setSocket);
// ACCESSOR_H(struct MHD_UpgradeResponseHandle*,  upgradeResponseHandle, setUpgradeResponseHandle);
//
// -(instancetype)
// initWithRequest:(GTKHTTPRequest*)theRequest
// MHDConnection:(struct MHD_Connection*)theConnection
// socket:(MHD_socket)theSocket
// responseHandle:(struct MHD_UpgradeResponseHandle*)theUpgradeResponseHandle;

-(BOOL)
didTimeoutAtDate:(NSDate*)theDate;

-(BOOL)
didTimeoutAtTimeInterval:(NSTimeInterval)theTimeInterval;

-(void)
closedByManagerBecauseSocketStoppedWorking;


ACCESSOR_H(NSMutableArray*, retainedData,     setRetainedData);
ACCESSOR_H(NSMutableArray*, retainedFrames,   setRetainedFrames);
ACCESSOR_H(NSMutableArray*, retainedMessages, setRetainedMessages);

-(void)deleteSocketDescriptor:(int)theSocketDescriptor;
-(void)closeConnection;
-(void)freeFromSocketManager;

-(NSString*)
sessionID;

-(id)
session;

-(void)
sendData:(NSData*)theDataToSend
error:(NSError**)outErr;

@end



@interface GTKSocketManager (MHDWebSocketConnectionAdditions)
-(void)
addWebSocketConnection:(GTKMHDWebSocketConnection*)theWebSocketConnection;
-(void)
removeWebSocketConnection:(GTKMHDWebSocketConnection*)theWebSocketConnection;
@end

