//
//  GTKHTTP2HTTPSServer.m
//  ObjectiveHTTPDmini
//
//  Created by Gustavo Tavares on 11/14/20.
//  Copyright © 2020 Gustavo Tavares. All rights reserved.
//

#import <ObjectiveHTTPDmini/GTKHTTP2HTTPSServer.h>
#include <microhttpd.h>



@implementation GTKHTTP2HTTPSServer

int
    http2httpsHandler(
        void*                     cls,
        struct MHD_Connection    *connection,
        const char               *url,
        const char               *method,
        const char               *version,
        const char               *upload_data,
        size_t                   *upload_data_size,
        void                    **con_cls)
{
    // fprintf(stderr, "Will rewrite...\n");
    #if __has_feature(objc_arc)
    GTKHTTP2HTTPSServer *server = (__bridge GTKHTTP2HTTPSServer*)cls;
    #else
    GTKHTTP2HTTPSServer *server = (GTKHTTP2HTTPSServer*)cls;
    #endif
    
    int returnCode = 0;
    
    if ( *con_cls == NULL ) {
        #if __has_feature(objc_arc)
            *con_cls = (__bridge void*)(server);
        #else
            *con_cls = (void*)(server);
        #endif
        
        return MHD_YES;
    } else {
        #if __has_feature(objc_arc)
        @autoreleasepool {
        #else
        NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
        #endif
        
        
        
        struct MHD_Response* response
               = MHD_create_response_from_buffer (
                    0,
                    "",
                    MHD_RESPMEM_PERSISTENT);
                   
        NSString* s = [NSString stringWithUTF8String:url];
        
        
        // fprintf(stderr, "HTTP2HTTPS: Redirecting to: %s", [s UTF8String]);
        
        if ([s hasPrefix:@"/"]) {
            
            // if ([s length] == 1) {
            //    s = [NSString stringWithFormat:@"https://localhost:%d", [server localHostPort]];
            // } else {
                s = [NSString stringWithFormat:@"https://localhost:%lu%@", [server localHostPort], s];
            // }
        } else
        if (s == nil) {
            s = [NSString stringWithFormat:@"https://localhost:%lu", [server localHostPort]];
        } else
        if ([s hasPrefix:@"http"]){
            s = [s stringByReplacingCharactersInRange:NSMakeRange(0, 4) withString:@"https"];
        } else {
            s = [NSString stringWithFormat:@"https://%@", s];
        }
        
        fprintf(stderr, "\nHTTP2HTTPS: Redirecting: %s to: %s\n", url, [s UTF8String]);

          MHD_add_response_header (response
             ,"Location"
             , [s UTF8String]);
         
          returnCode
              = MHD_queue_response(
                    connection
                  , 301
                  , response);
                  
          MHD_destroy_response(response);
          
          #if __has_feature(objc_arc)
          }
          #else
          [pool drain];
          #endif
          
          return returnCode;
    }
}

-(int)
    defaultPort
{
    return 61001;
}


-(BOOL)
start
{
    // return [self startSecureHTTPServerUsingKeyFile:[self keyFile] andCertifcateFile:[self certificateFile];
    return [self startSecureHTTPServerUsingKeyFile:nil andCertifcateFile:nil];
    
    
}


-(BOOL)
startSecureHTTPServerUsingKeyFile:(char*)keyFile
andCertifcateFile:(char*)certFile
{
    int attempts=0;
    int flags = 0;
    
    flags |= MHD_USE_SELECT_INTERNALLY;
    flags |= MHD_USE_ERROR_LOG;
    
    BOOL debug = YES;
    
    if (debug) {
        flags |= MHD_USE_DEBUG;
    }

    if (MHD_is_feature_supported(MHD_FEATURE_MESSAGES))
    {
        AlwaysLog(@"MHD — was compiled with messages. Should print errors.");
    }
    else
    {
        AlwaysLog(@"MHD — WARNING — No messages.");
    }
    
    BOOL allowFindFreePort = NO;
    
    
    while ( ![self httpd] && attempts < 50) {
        [self setHttpd:
            MHD_start_daemon(
                 flags,
                [self port],          // PORT#
                NULL,                 // APC#      — AccessPolicyCallback
                NULL,                 // APC_CLS#  — Extra Argument
                http2httpsHandler,    // DH#       - AccessHandlerCallback
                #if __has_feature(objc_arc)
                    (__bridge void*)(self),
                #else
                    self,
                #endif
                MHD_OPTION_END)];

        if (![self httpd])
        {
            if (allowFindFreePort) {
                [self setPort:[self port]+1];
            }
            attempts++;
        }
    }
    
    
    
    AlwaysLog(@"%@ === HTTP 2 HTTPS Server started (Port=%d) (Pointer=%p)"
            ,NSStringFromClass([self class])
            ,[self port]
            ,[self httpd]
            );
    
    return [self httpd] != NULL;
}

@end
