//
//  GTKHTTP2HTTPSServer.h
//  ObjectiveHTTPDmini
//
//  Created by Gustavo Tavares on 11/14/20.
//  Copyright © 2020 Gustavo Tavares. All rights reserved.
//

#import <ObjectiveHTTPDmini/ObjectiveHTTPDmini.h>



@interface GTKHTTP2HTTPSServer : GTKHTTPServer

@property NSUInteger localHostPort;

-(BOOL)start;

@end


