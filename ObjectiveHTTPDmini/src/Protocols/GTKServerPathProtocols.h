//
//  GTKServerPathProtocols.h
//  ObjectiveHTTPDmini
//
//  Created by Gustavo Tavares on 5/11/21.
//  Copyright © 2021 Gustavo Tavares. All rights reserved.
//

#ifndef GTKServerPathProtocols_h
#define GTKServerPathProtocols_h

@class
    GTKHTTPServer;

@protocol IsGTKHTTPPathForClassDictionary <NSObject>

+(id)pathServerWithParent:(id)parent;
-(id)parentServer;
-(void)setParentServer:(id)parent;

+(BOOL)requiresAuthentication;
-(BOOL)requiresAuthentication;

-(id)serveRequest:theRequest;

@optional
-(GTKHTTPServer*)server;
-(void)setServer:(GTKHTTPServer*)theServer;
-(BOOL)isAuthorizedRequest:(id)theRequest;

@end

@protocol IsGTKHTTPSwitchPath <NSObject>
@optional
    -(id)serveGetRequest:(id)aGetRequest;
    -(id)serveDeleteRequest:(id)aDeleteRequest;
    -(id)servePostRequest:(id)aPostRequest;
    -(id)servePutRequest:(id)aPutRequest;
    -(id)servePatchRequest:(id)aPatchRequest;
    -(id)serveUnknownTypeRequest:(id)anUnknownTypeRequest;
@end

typedef NS_ENUM(NSInteger, GTKHTTPExceptionLocation)
{
    GTKHTTPExceptionLocationSocketLoop,
    GTKHTTPExceptionLocationResponseFetching,
    GTKHTTPExceptionLocationResponseEncoding,
    GTKHTTPExceptionLocationOther,
};


typedef NS_ENUM(NSInteger, GTKHTTPRequestFinishedCode)
{
   GTKRequestFinishedOKCode            = 0,
   GTKRequestFinishedWithErrorCode     = 1,
   GTKRequestFinishedOnTimeout         = 2,
   GTKRequestFinishedOnShutdown        = 3,
   GTKRequestFinishedOnClientSentError = 4,
   GTKRequestFinishedOnClientAbort     = 5,
};


#endif /* GTKServerPathProtocols_h */
