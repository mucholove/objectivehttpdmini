//
//  MPWResource.h
//  MPWShellScriptKit
//
//  Created by Marcel Weiher on 11/25/10.
//  Copyright 2010 Marcel Weiher. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GTKFoundation/GTKFoundation.h>

@class GTKHTTPRequest;

typedef NS_ENUM(NSInteger, GTKHTTPCookieSameSiteType) {
    GTKHTTPCookieSameSiteNone,
    GTKHTTPCookieSameSiteLax,
    GTKHTTPCookieSameSiteStrict,
};



@interface GTKHTTPCookie : NSObject

+(instancetype)
cookie;

+(instancetype)
cookieWithName:(NSString*)theName
value:(NSString*)theValue
maxAge:(NSNumber*)theMaxAge;

scalarAccessor_h(NSString*, name,        setName);
scalarAccessor_h(NSString*, value,       setValue);
// --------------------------------------------------
scalarAccessor_h(NSString*, domain,      setDomain);
scalarAccessor_h(NSString*, path,        setPath);
// --------------------------------------------------
scalarAccessor_h(NSDate*,   expires,     setExpires);
scalarAccessor_h(NSNumber*, maxAge,      setMaxAge);
// --------------------------------------------------
scalarAccessor_h(BOOL,      isSecure,    setIsSecure);
scalarAccessor_h(BOOL,      isHTTPOnly,  setIsHTTPOnly);
// --------------------------------------------------
-(void)setSameSiteSetting:(GTKHTTPCookieSameSiteType)theSetting;
-(NSString*)asString;
/*
 Set-Cookie: <cookie-name>=<cookie-value>; Expires=<date>
 Set-Cookie: <cookie-name>=<cookie-value>; Max-Age=<non-zero-digit>
 Set-Cookie: <cookie-name>=<cookie-value>; Domain=<domain-value>
 Set-Cookie: <cookie-name>=<cookie-value>; Path=<path-value>
 Set-Cookie: <cookie-name>=<cookie-value>; Secure
 Set-Cookie: <cookie-name>=<cookie-value>; HttpOnly

 Set-Cookie: <cookie-name>=<cookie-value>; SameSite=Strict
 Set-Cookie: <cookie-name>=<cookie-value>; SameSite=Lax
 Set-Cookie: <cookie-name>=<cookie-value>; SameSite=None
 */

@end

@interface GTKMutableStringContainer : NSMutableString
@end

@interface GTKHTMLMutableString : GTKMutableStringContainer
@end

@interface GTKJSONMutableString : GTKMutableStringContainer
@end


@interface GTKHTTPServerResponse : NSObject
{

}

ACCESSOR_H(GTKHTTPCode, httpResponseCode, setHTTPResponseCode);

+(instancetype)
htmlResponseFromString:(NSString*)theString;

+(instancetype)
wrapObject:(id)toEncode
forRequest:(GTKHTTPRequest*)theRequest;

+(instancetype)
wrapObject:(id)toEncode
forRequest:(GTKHTTPRequest*)theRequest
debug:(BOOL)debug;

scalarAccessor_h(GTKHTTPCode,
    httpResponseCode,
    setHTTPResponseCode);

-(NSDictionary*)applicationHeaders;
-(void)setApplicationHeaders:(NSDictionary*)theNewDict;
    
-(NSArray*)cookies;
-(void)addCookie:(GTKHTTPCookie*)theCookie;
-(void)removeCookie:(GTKHTTPCookie*)theCookie;



// ACCESSOR_H(NSData*, rawData, setRawData)
@property (retain) NSData* rawData;
-(void*)bytes;
-(NSUInteger)length;

ACCESSOR_H(NSString*, MIMEType, setMIMEType)
-(NSString*)mimeType;
-(char*)mimeTypeForMicroHTTPD;

-(void)
setValue:(id)value
forHeader:(NSString*)key;


+(id)response;


@end

/*
objectAccessor_h(NSError,
    error,
    setError)

idAccessor_h(source, setSource)
 */

