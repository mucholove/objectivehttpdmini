//
//  MPWResource.m
//  MPWShellScriptKit
//
//  Created by Marcel Weiher on 11/25/10.
//  Copyright 2010 Marcel Weiher. All rights reserved.
//

#import <ObjectiveHTTPDmini/GTKHTTPServerResponse.h>
#import <ObjectiveHTTPDmini/GTKMHDHTTPRequest.h>
#import <ObjectiveHTTPDmini/NSError+AsRequestableObject.h>
#import <GTKFoundation/GTKFoundation.h>

@interface NSObject(imageRepWithData)

+imageRepWithData:(NSData*)data;

@end

// MPWByteStream
//
//@interface NSData (ByteStreaming)
//
//-(void)writeOnByteStream:(MPWByteStream*)aStream
//
//@end
//
//
//@implementation NSData(ByteStreaming)
//
//-(void)writeOnByteStream:(MPWByteStream*)aStream
//{
//    [aStream appendBytes:[self bytes] length:[self length]];
//}
//
//-(long)targetLength
//{
//    return [self length];
//}
//
//
//-(void)flush
//{
//}
//@end



@implementation GTKMutableStringContainer
{
    NSMutableString* _string;
}
+(instancetype)
string
{
    return AUTORELEASE([[self alloc] init]);
}
-(instancetype)
init
{
    #if !GNUSTEP
    self = [super init];
    #endif
    
    _string = [[NSMutableString alloc] init];
    
    return self;
}
-(NSUInteger)
length
{
    return [_string length];
}
-(unichar)
characterAtIndex:(NSUInteger)index
{
    return [_string characterAtIndex:index];
}
-(void)
replaceCharactersInRange:(NSRange)range
withString:(NSString*)aString
{
    [_string replaceCharactersInRange:range withString:aString];
}
-(void)
getCharacters:(unichar*)buffer
range:(NSRange)range
{
    [_string getCharacters:buffer range:range];
}
@end

@implementation GTKHTMLMutableString
@end

@implementation GTKJSONMutableString
@end

@implementation GTKHTTPCookie
{
    GTKHTTPCookieSameSiteType sameSiteType;
    NSString*                 name;
    NSString*                 value;
    // ----------------
    NSString*                 domain;
    NSString*                 path;
    // ----------------
    NSDate*                   expires;
    NSNumber*                 maxAge;
    // ----------------
    BOOL                      isSecure;
    BOOL                      isHTTPOnly;
}


#if !HAS_ARC
-(void)
dealloc
{
    RELEASE(name);
    RELEASE(value);
    RELEASE(domain);
    RELEASE(path);
    RELEASE(expires);
    RELEASE(maxAge);
    [super dealloc];
}
#endif

M_ACCESSOR(NSString*, name,        name,       setName);
M_ACCESSOR(NSString*, value,       value,      setValue);
// --------------------------------------------------
M_ACCESSOR(NSString*, domain,      domain,     setDomain);
M_ACCESSOR(NSString*, path,        path,       setPath);
// --------------------------------------------------
M_ACCESSOR(NSDate*,   expires,     expires,    setExpires);
M_ACCESSOR(NSNumber*, maxAge,      maxAge,     setMaxAge);
// --------------------------------------------------
scalarAccessor(BOOL,      isSecure,    setIsSecure);
scalarAccessor(BOOL,      isHTTPOnly,  setIsHTTPOnly);

+(instancetype)
cookie
{
    return AUTORELEASE([[self alloc] init]);
}

+(instancetype)
cookieWithName:(NSString*)theName
value:(NSString*)theValue
maxAge:(NSNumber*)theMaxAge
{
    GTKHTTPCookie* cookie = nil;
    
    [cookie setName:theName];
    [cookie setValue:theValue];
    [cookie setMaxAge:theMaxAge];
    
    return cookie;
}

-(void)
initLocalVars
{

    sameSiteType = GTKHTTPCookieSameSiteStrict;
}

-(instancetype)
init
{
    self = [super init];

    if (self)
    {
        sameSiteType = GTKHTTPCookieSameSiteStrict;
    }

    return self;
}


-(void)
    setSameSiteSetting:(GTKHTTPCookieSameSiteType)theSetting
{
    sameSiteType = theSetting;
}

-(NSString*)
    asString
{
    NSMutableString* cookie = [NSMutableString string];
    [cookie appendFormat:@"%@=", [self name]];
    [cookie appendFormat:@"%@", [self value]];
    [cookie appendFormat:@";"];

    // https://developer.mozilla.org/en-US/docs/Web/HTTP/Cookies
    // A cookie with the HttpOnly attribute is inaccessible to the JavaScript Document.cookie API
    //
    if ([self domain]) {
        [cookie appendFormat:@" Domain=%@;", [self domain]];
    }
    if ([self path]) {
        [cookie appendFormat:@" Path=%@;", [self path]];
    }

    if ([self isHTTPOnly]) {
        [cookie appendFormat:@" HttpOnly;"];
    }
    if ([self isSecure]) {
        [cookie appendFormat:@" Secure;"];
    }
    if ([self expires]) {
        NSISO8601DateFormatter *formatter = AUTORELEASE([[NSISO8601DateFormatter alloc] init]);
        [cookie appendFormat:@" Expires=%@;", [formatter stringFromDate:[self expires]]];
    }

    if ([self maxAge]) {
        [cookie appendFormat:@" Max-Age=%@;", [self maxAge]];
    }

    [cookie appendFormat:@"SameSite="];
    switch (sameSiteType) {
        case GTKHTTPCookieSameSiteNone:
            [cookie appendFormat:@"None"];
            break;
        case GTKHTTPCookieSameSiteLax:
            [cookie appendFormat:@"Lax"];
            break;
        case GTKHTTPCookieSameSiteStrict:
            [cookie appendFormat:@"Strict"];
            break;
    }
    [cookie appendFormat:@";"];

   return cookie;
}


@end


@implementation GTKHTTPServerResponse
{
    GTKHTTPCode          _httpResponseCode;
    //\\//\\/\\//\\//\\//\\//\\//
    id                   _value;
    NSMutableDictionary* _applicationHeaders;
    NSData*              _rawData;
    NSMutableArray*      _cookies;
    NSString*            _mimeType;
    NSData*              _mimeAsData;
}

#if !__has_feature(objc_arc)
-(void)
dealloc
{
    DEBUG_NO
	// RELEASE(source);
	// RELEASE(rawData);
	// RELEASE(_value);
	// RELEASE(MIMEType);
    // RELEASE(error);

    
    RELEASE(_value);
    RELEASE(_applicationHeaders);
    RELEASE(_rawData);
    RELEASE(_cookies);
    RELEASE(_mimeType);
    RELEASE(_mimeAsData);
    // char                mimebuf[200];
    // free(mimebuf);
	[super dealloc];
}
#endif

+(instancetype)
htmlResponseFromString:(NSString*)theString
{
    GTKHTTPServerResponse* response = AUTORELEASE([[self alloc] init]);
    
    [response setMIMEType:[MimeType html]];
    [response setRawData:[theString dataUsingEncoding:NSUTF8StringEncoding]];
    
    return response;
}

+(instancetype)
responseForException:(NSException*)theException
prefferedMimeType:(NSString*)prefferedMimeType
{
    DEBUG_NO
    
    GTKHTTPServerResponse* response = [self response];
    
    BOOL includeSenstiveInfo = NO;

    DebugLog(@"Got exception: %@", theException);

    #if DEBUG
    includeSenstiveInfo = YES;
    #endif

    NSError* error
        = [theException asErrorWithCode:501
                     domain:@"PrepareServerResponse"
                     includeSensitiveInfo:includeSenstiveInfo];

    [response setHTTPResponseCode:501];

    if ([prefferedMimeType isEqualTo:[MimeType json]])
    {
        [response setRawData:[[error asJSON] asData]];
        [response setMIMEType:[MimeType json]];
    }
    else
    {
        // [@"<html><body>Unable to respond to request.</body></html>" asData]
        [response setRawData:[[error asHTML] escapedDataAsHTMLUTF8String]];
        [response setMIMEType:[MimeType html]];
    }
    
    return response;
}

-(void)
setStatusCodeBasedOnObject:(id)toEncode
{
    DEBUG_NO
    DebugLog(@"Will check if responds to HTTPCode");
    if ([toEncode respondsToSelector:@selector(httpCode)])
    {
        DebugLog(@"Did check. Does respond: %lu", [toEncode httpCode]);
        [self setHTTPResponseCode:[toEncode httpCode]];
        DebugLog(@"Response code set.");
    } else {
        DebugLog(@"Did check. Doesn't respond. Will set to 200.");
        [self setHTTPResponseCode:200];
        DebugLog(@"Response code set.");
    }
}


+(instancetype)
htmlResponseForRequest:(GTKHTTPRequest*)theRequest
object:(id)toEncode
{
    DEBUG_NO
    
    GTKHTTPServerResponse* response = [self response];

    [response setValue:toEncode];

    [response setStatusCodeBasedOnObject:toEncode];
    
    DebugLog(@"Will check if responds to `asHTML`");
    if ([toEncode respondsToSelector:@selector(asHTML)])
    {
        DebugLog(@"Will call `asHTML`");
        // [response setRawData:[[toEncode asHTML] escapedDataAsHTMLUTF8String]];
        [response setRawData:[[toEncode asHTML] dataUsingEncoding:NSUTF8StringEncoding]];
        DebugLog(@"Did call `asHTML`");
    }
    else
    {
        DebugLog(@"Does not respond to `asHTML`");
        if ([toEncode isKindOfClass:[NSString class]] && [toEncode hasPrefix:@"<html>"])
        {
            DebugLog(@"Is an NSString and contains HTML");
            [response setRawData:[toEncode dataUsingEncoding:NSUTF8StringEncoding]];
        }
        else
        {
            [response setRawData:[@"<html><body>Unable to respond to request.</body></html>" dataUsingEncoding:NSASCIIStringEncoding]];
        }
    }
    
    [response setMIMEType:[MimeType html]];
    
    return response;
}

+(instancetype)
jsonResponseForRequest:(GTKHTTPRequest*)theRequest
object:(id)toEncode
{
    DEBUG_NO
    
    GTKHTTPServerResponse* response = [self response];

    [response setValue:toEncode];

    [response setStatusCodeBasedOnObject:toEncode];
    
    
    NSData* theJSONData = nil;
        
    BOOL straightToData = YES;
    
    if (straightToData)
    {
        DebugLog(@"Will call `asJSONData`");
        theJSONData = [toEncode asJSONData];
        DebugLog(@"Did call `asJSONData`");
    }
    else
    {
        DebugLog(@"Will call `asJSON`");
        NSString* asJSON = [toEncode asJSON];
        DebugLog(@"As JSON is: %@", asJSON);
        theJSONData = [asJSON dataUsingEncoding:NSUTF8StringEncoding];
    }
    
    DebugLog(@"As JSON bytes: %s", CAST(char*)[theJSONData bytes]);
    DebugLog(@"As JSON length: %lu", [theJSONData length]);
    [response setRawData:theJSONData];
    DebugLog(@"Did call `asJSON`");

    [response setMIMEType:[MimeType json]];
    
    return response;
}

+(instancetype)
noValidPrefferedMimeTypeErrorResponseForRequest:(GTKHTTPRequest*)theRequest
object:(id)toEncode
{
    DEBUG_NO
    
    GTKHTTPServerResponse* response = [self response];

    [response setValue:toEncode];

    [response setStatusCodeBasedOnObject:toEncode];
    
    DebugLog(@"WARNING — Unaccounted for MimeType - NULL RESPONSE.");
    [response setMIMEType:[MimeType html]];

    NSData* theData = nil;

    NSMutableString* html = [NSMutableString string];
    [html appendString:@"<html>"];
    [html appendString:@"<head>"];
    
    #if !DEBUG
    [html appendString:@"<link type=\"text/css\" href=\"/siteStylesheet.css\" rel=\"stylesheet\">"];
    #endif
    
    [html appendString:@"</head><body><p>"];
    
    #if DEBUG
    NSString* objAsString = nil;
    
    if ([objAsString isKindOfClass:[NSString class]])
    {
        objAsString = toEncode;
    }
    else if ([objAsString respondsToSelector:@selector(prettyPrintString)])
    {
        objAsString = [toEncode prettyPrintString];
    }
    else
    {
        objAsString = [toEncode description];
    }
    [html appendFormat:@"An error occured when trying to return object: %@ for path: \"%@\"", [objAsString escapedHTMLForASCIIString], [theRequest path]];
    #else
    [html appendString:@"An Error Occured."];
    #endif
    
    [html appendString:@"</p></body></html>"];
    DebugLog(@"Will send: %@", html);
    theData = [html dataUsingEncoding:NSUTF8StringEncoding];
        
    [response setRawData:theData];
    [response setHTTPResponseCode:501];

    DebugLog(@"Returning response...");
    return response;
}

+(instancetype)
wrapObject:(id)toEncode
forRequest:(GTKHTTPRequest*)theRequest
{
    return [self
        wrapObject:toEncode
        forRequest:theRequest
        debug:NO];
}

+(instancetype)
wrapObject:(id)toEncode
forRequest:(GTKHTTPRequest*)theRequest
debug:(BOOL)shouldDebug
{
    DEBUG_NO
    
    if (shouldDebug)
    {
        debug = shouldDebug;
    }
    

    DebugLog(@"Request to wrap object: %@", toEncode);
    if ([toEncode isKindOfClass:[GTKHTTPServerResponse class]]) {
        DebugLog(@"No need to encode. Already a response. Returning...");
        return toEncode;
    }
    
    if ([toEncode isKindOfClass:[GTKHTMLMutableString class]])
    {
        return [self htmlResponseForRequest:theRequest object:toEncode];
    }
    
    if ([toEncode isKindOfClass:[GTKJSONMutableString class]])
    {
        return [self jsonResponseForRequest:theRequest object:toEncode];
    }

    for (NSString* prefferedMimeType in [theRequest acceptedTypes])
    {
        BOOL acceptsAnyType = [prefferedMimeType isEqualToString:@"*/*"];

        @try
        {
            DebugLog(@"Preffered Mime Type is %@", prefferedMimeType);
            if ([prefferedMimeType isEqualTo:[MimeType json]] || acceptsAnyType)
            {
                return [self jsonResponseForRequest:theRequest object:toEncode];
            }
            else if ([prefferedMimeType isEqualTo:[MimeType html]])
            {
                return [self htmlResponseForRequest:theRequest object:toEncode];

            }
        }
        @catch (NSException* exception)
        {
            return [self responseForException:exception
                         prefferedMimeType:prefferedMimeType];
        }
    }
    
    BOOL notifyClientOfNoPrefferedMimeType = NO;
    
    DebugLog(@"NO PREFFERED MIME-TYPE!!!");
    
    
    
    if (notifyClientOfNoPrefferedMimeType)
    {
        return [self noValidPrefferedMimeTypeErrorResponseForRequest:theRequest
                     object:toEncode];
    }
    else
    {
        NSString* defaultMimeType = [MimeType json];
        @try
        {
            DebugLog(@"NOTE — THIS MIGHT CAUSE A PROBLEM!!!");
            // if ([[[theRequest headers] userAgent] isBrowser])
            // {
            //
            // }
            // else
            // {
                return [self jsonResponseForRequest:theRequest object:toEncode];
            // }
        }
        @catch (NSException* exception)
        {
            return [self responseForException:exception
                         prefferedMimeType:defaultMimeType
                         ];
        }
    }
}

+(id)
response
{
    return AUTORELEASE([[self alloc] init]);
}

+(id)
responseFor:(id)theObject
asMimeType:(NSString*)theMimeType
error:(NSError**)outErr
{
    NSError* internalError = nil;
    
    GTKHTTPServerResponse* response = [self response];
    
    [response setSource:theObject];
    [response setMIMEType:theMimeType];
    
    @try {
        if ([theMimeType isEqualTo:[MimeType json]]) {
            [response setRawData:[[theMimeType asJSON] asData]];
        } else
        if ([theMimeType isEqualTo:[MimeType html]])
        {
            [response setRawData:[[theMimeType a] asData]];
        }
    } @catch (NSException* exception) {
        BOOL shouldIncludeSensitiveInfo = NO;
        #if DEBUG
        shouldIncludeSensitiveInfo = YES;
        #endif
        
        internalError
            = [exception
                asErrorWithCode:501
                domain:@"Prepare response"
                includeSensitiveInfo:shouldIncludeSensitiveInfo];
                
        if (outErr)
        {
            *outErr = internalError;
        }
    
    }

    return response;
}

-(instancetype)
    init
{
    self = [super init];

    if (self) {
        [self initLocalVars];
    }

    return self;
}

-(void)
    initLocalVars
{
    [self setHTTPResponseCode:200];
}

-(void)setMIMEType:(NSString*)toSet { [self set_MIMEType:toSet]; }
-(void)setMimeType:(NSString*)toSet { [self set_MIMEType:toSet]; }
-(NSString*)MIMEType                { return [self mimeType]; }
M_ACCESSOR(NSString*,  _mimeType, mimeType,  set_MIMEType);
M_ACCESSOR( NSData*,   _rawData,  rawData,   setRawData );
M_ACCESSOR( id,        _value,    value,     setValue );

// M_ACCESSOR( NSError*,  error,    error,     setError);

// M_ACCESSOR( id,        source,   source,    setSource );

S_ACCESSOR(GTKHTTPCode, _httpResponseCode, httpResponseCode, setHTTPResponseCode)



-(void*)bytes
{
    return [[self rawData] bytes];
}
-(NSUInteger)length
{
    return [[self rawData] length];
}

-(NSDictionary*)
applicationHeaders
{
    return _applicationHeaders;
}
-(void)
setApplicationHeaders:(id)theHeaders
{
    if ([theHeaders respondsToSelector:@selector(setObject:forKey:)])
    {
        ASSIGNCOPY(_applicationHeaders, theHeaders);
    } else {
        NSMutableDictionary* dict = [NSMutableDictionary dictionaryWithDictionary:theHeaders];
    
        ASSIGN_ID_gtk(_applicationHeaders, dict);
    }
}

-(NSArray*)cookies
{
    return _cookies;
}

-(void)
    addCookie:(GTKHTTPCookie*)theCookie
{
    if (!_cookies) {
        ASSIGN_ID_gtk(_cookies, [NSMutableArray array]);
    }
    [_cookies addObject:theCookie];
}
-(void)
    removeCookie:(GTKHTTPCookie*)theCookie
{
    [_cookies removeObject:theCookie];
}



-(void)
setValue:(id)value
forHeader:(NSString*)key
{
    DEBUG_NO
    
    if (!_applicationHeaders) {
        ASSIGN_ID_gtk(_applicationHeaders, [NSMutableDictionary dictionary]);
    }
    
    DebugLog(@"Will call `setObject:forKey` to: %@:%@" , NSStringFromClass([_applicationHeaders class]), _applicationHeaders)

    [CAST(NSMutableDictionary*)_applicationHeaders setObject:value forKey:key];
    
    DebugLog(@"Did call `setObject:forKey` to: %@:%@" , NSStringFromClass([_applicationHeaders class]), _applicationHeaders)
}

-(void)
forwardInvocation:(NSInvocation*)invocation
{
	if ( [self value] && [[self value] respondsToSelector:[invocation selector]])
    {
		[invocation invokeWithTarget:[self value]];
	} else if ( [self rawData] ) {
		[invocation invokeWithTarget:[self rawData]];
	}
}

-(NSMethodSignature*)
methodSignatureForSelector:(SEL)selector
{
	NSMethodSignature *sig
        = [super methodSignatureForSelector:selector];

	if ( !sig ) {
		sig
            = [[self value]
                methodSignatureForSelector:selector];
	}
	if ( !sig ) {
		sig
            = [[self rawData]
                methodSignatureForSelector:selector];
	}
	return sig;
}

-(BOOL)
    respondsToSelector:(SEL)aSelector
{
	return [super          respondsToSelector:aSelector]
            || [[self value]   respondsToSelector:aSelector]
            || [[self rawData] respondsToSelector:aSelector];
}

-(id)
    description
{
    NSMutableString* d = [NSMutableString string];
    [d appendFormat:@"<%@ p=%p", NSStringFromClass([self class]), self];
    [d appendFormat:@" sourceObj=%@", NSStringFromClass([[self value] class])];
    [d appendFormat:@" rawDataLen=%lu", [self length]];
    [d appendFormat:@">"];
    return d;
}

/*
-(id)
    description
{
	// BOOL debug = YES;
	DEBUG_NO

	if ( [self value] ) {
		DEBUG_PUTS("Will return description of value");
		return [[self value] description];
	} else {
		if ([self rawData]) {
   			if ([self rawData] == NULL) {
        			DEBUG_PUTS("is null");
			}
			DEBUG_PUTS("Will return description of rawData");
			DebugLog(@"Raw data: %lu", [[self rawData] length]);
			DebugLog(@"Raw data: %@", [self rawData]);
			return [[self rawData] description];

		}

		DEBUG_PUTS("Will return NSObject description.");
		return [super description];
	}
}
*/

-(id)
    valueForKey:(NSString*)key
{
    if ( [self value] ) {
        return [[self value] valueForKey:key];
    } else {
        return [[self rawData] valueForKey:key];
    }
}

-(NSString*)
    stringValue
{
	if ( [self value] )
    {
		return [[self value]
                    stringValue];
	}
    else
    {
		return [[self rawData]
                    stringValue];
	}
}

-(void)
    writeToURL:(NSURL*)url
    atomically:(BOOL)atomically
{
    [[self rawData]
        writeToURL:url
        atomically:atomically];
}

-(NSString*)
    name
{
	return [[[self source] path] lastPathComponent];
}

-(NSString*)
    extension
{
	return [[self name] pathExtension];
}

-(NSData*)
    asData
{
    return [self rawData];
}


-(char*)
mimeTypeForMicroHTTPD
{
    DEBUG_NO

    if (!_mimeType)
    {
        DebugLog(@"Mimetype is NULL. Might ERROR");
        return NULL;
    }

    if (!_mimeAsData)
    {
        ASSIGN_ID_gtk(_mimeAsData, [_mimeType dataUsingEncoding:NSUTF8StringEncoding]);
    }


    DebugLog(@"Did put `mimetype`: %@ in `mimebuf`:%s\n", _mimeType, CAST(char*)[_mimeAsData bytes]);

    return CAST(char*)[_mimeAsData bytes];
}




@end


@implementation GTKHTTPServerResponse (Testing)

+(id)testSelectors
{
    return @[
        @"testSerializeDictionary",
    ];
}

+(GTKHTTPRequest*)
    jsonRequest
{
    GTKHTTPRequest* request = [GTKHTTPRequest requestOnConnection:NULL session:nil];
    [request setHeaders:@{
        @"Accept": @"application/json"
    }];
    return request;
}


/*
GOT:
 {"storeIdentifier":"66C75F58-602F-0707-66A7-D13479C9A283","token":"D53B643D-E26E-E707-7F41-25CE63CC70EB:B7DADEDE-9464-8202-46BB-F21FD4D47413"}1FD4D47413"}
EXPECTED:
 {"storeIdentifier":"66C75F58-602F-0707-66A7-D13479C9A283","token":"D53B643D-E26E-E707-7F41-25CE63CC70EB:B7DADEDE-9464-8202-46BB-F21FD4D47413"}
*/

+(void)
    testSerializeDictionary
{
    GTKOrderedDictionary* dict = [GTKOrderedDictionary dictionary];

    [dict setObject:@"66C75F58-602F-0707-66A7-D13479C9A283"
          forKey:@"storeIdentifier"];

    [dict setObject:@"D53B643D-E26E-E707-7F41-25CE63CC70EB:B7DADEDE-9464-8202-46BB-F21FD4D47413"
          forKey:@"token"];

    //GTKHTTPServerResponse* response = [GTKHTTPServerResponse wrapObject:@{
    //        @"storeIdentifier": @"66C75F58-602F-0707-66A7-D13479C9A283",
    //        @"token": @"D53B643D-E26E-E707-7F41-25CE63CC70EB:B7DADEDE-9464-8202-46BB-F21FD4D47413"
    //} forRequest:[self jsonRequest]];

    GTKHTTPServerResponse* response = [GTKHTTPServerResponse wrapObject:dict forRequest:[self jsonRequest]];

    char *expected = "{\"storeIdentifier\":\"66C75F58-602F-0707-66A7-D13479C9A283\",\"token\":\"D53B643D-E26E-E707-7F41-25CE63CC70EB:B7DADEDE-9464-8202-46BB-F21FD4D47413\"}";

    // EXPECT_FALSE(strcmp([response bytes], expected), FMT(@"Got %s —!!!-  Expected %s", [response bytes], expected));

}

@end
