//
//  GTKServerAuthDelegate.h
//  ObjectiveHTTPDmini
//
//  Created by Gustavo Tavares on 5/29/20.
//  Copyright © 2020 Gustavo Tavares. All rights reserved.
//

@protocol GTKServerSession
-(id)user;
@end

@protocol GTKServerAuthDelegate
    -(id)keyForUserInHTTPHeader;
    -(id)keyForTokenInHTTPHeader;
    -(id)
        isValidCStringUserName:(const char*)aUserName
        withCStringToken:(const char*)aToken;
    
    -(id)
        isValidUserName:(NSString*)aUserName
        withToken:(NSString*)aToken;
        
     // -(id)
     //    validateUserNameWithName:(const char*)aUserName
     //    withPassword:(const char*)aToken;
         
    // -(id)
    //     validateUserNameWithName:(const char*)aUserName
    //     withToken:(const char*)aToken;
        
    // -(id)
    //    validateUserNameWithCStringName:(const char*)aUserName
    //    withCStringPassword:(const char*)aToken;
        
   // -(id)
   //     validateUserNameWithCStringName:(const char*)aUserName
   //     withCStringToken:(const char*)aToken;
        
    // -(id)
    //    isValidUserName:(NSString*)aUserName
    //    withToken:(NSString*)aToken;

@end
