//
//  GTKHTTPServerDelegate.h
//  ObjectiveHTTPD
//
//  Created by Gustavo Tavares on 3/22/20.
//

#import <Foundation/Foundation.h>
#import <GTKFoundation/GTKFoundation.h>



@class GTKHTTPServer;

@protocol GTKHTTPServerDelegate <NSObject>

-(GTKHTTPServer*)
gtkHTTPServer;

-(void)
setGTKHTTPServer:(GTKHTTPServer*)theServer;

-(id)
serveRequest:aRequest;

-(NSString*)
mimeType;

@end

/*

-(id)
    serveRequest:(id)theRequest
    onURLString:(id)theRequestedString;
         

-(NSData*)
     continueGetServiceForURLString:(NSString*)urlString
     onMatchedPrefix:(NSMutableString*)matchedPrefix
     withRequestHeaders:(NSDictionary*)requestHeader
     withURLParameters:(NSDictionary*)urlParams;

     
 -(NSData*)
     continuePostServiceForURLString:(NSString*)urlString
     onMatchedPrefix:(NSMutableString*)matchedPrefix
     withRequestHeaders:(NSDictionary*)requestHeader
     withURLParameters:(NSDictionary*)urlParams
     withRequestBody:(NSData*)requestBody
     withRequestProcessor:(id)aPostProcessor;
 */




