//
//  GTKHTTPRequest.m
//  ObjectiveHTTPDmini
//
//  Created by Gustavo Tavares on 5/29/20.
//  Copyright © 2020 Gustavo Tavares. All rights reserved.
//

#import <ObjectiveHTTPDmini/GTKHTTPRequest.h>
#import <ObjectiveHTTPDmini/GTKMHDHTTPRequest.h>
#import <ObjectiveHTTPDmini/GTKHTTPGetRequest.h>
#import <ObjectiveHTTPDmini/GTKHTTPPostRequest.h>
#import <ObjectiveHTTPDmini/GTKHTTPServerResponse.h>
#import <ObjectiveHTTPDmini/GTKServerAuthDelegate.h>
#import <GTKFoundation/GTKFoundation.h>

@implementation GTKHTTPRequest
{
    NSString*               _hostName;
    NSMutableDictionary*    _headers;
    NSString*               _path;
    NSMutableDictionary*    _uriParameters;
    NSMutableDictionary*    _queryParameters;
    id                      _session;
    GTKHTTPMethod           _gtkHTTPMethod;
    GTKHTTPCode             _HTTPCode;
    NSMutableDictionary*    _cookies;
    NSArray*                _acceptedTypes;
    GTKHTTPServerResponse*  _response;
    GTKHTTPCode             _responseCode;
}

-(BOOL)
writeOnStreamFromProps
{
    return YES;
}

-(NSString*)
log
{
    NSMutableString* log = [NSMutableString string];
    
    [self writeOnLog:log];
    
    return log;
}

-(void)
writeOnLog:(NSMutableString*)theLog
{
    // log_format combined '$remote_addr - $remote_user [$time_local] ' '"$request" $status $body_bytes_sent ' '"$http_referer" "$http_user_agent"';
    NSString* httpMethodString = StringForGTKHTTPMethod([self HTTPMethod]);
    NSInteger httpResponseCode = _responseCode ?: 9999;
    
    [theLog appendFormat:@"%@ %@ (%ld)\n", httpMethodString, [self path], httpResponseCode];
}


+(GTKOrderedDictionary*)
propertyToTypeDictionaryVersion:(NSUInteger)theVersion
withContext:(id)theContext
{
    GTKOrderedDictionary* dict = [GTKOrderedDictionary dictionary];
    
    [dict setObject:[NSString class]               forKey:@"hostName"];
    [dict setObject:[NSMutableDictionary class]    forKey:@"headers"];
    [dict setObject:[NSString class]               forKey:@"path"];
    [dict setObject:[NSMutableDictionary class]    forKey:@"uriParameters"];
    [dict setObject:[NSMutableDictionary class]    forKey:@"queryParameters"];
    
    // GTKHTTPMethod
    [dict setObject:[GTKHTTPMethodClass class]     forKey:@"gtkHTTPMethod"];
    // GTKHTTPCode
    [dict setObject:[CLongAsClass class]           forKey:@"HTTPCode"];
    [dict setObject:[NSArray class]                forKey:@"acceptedTypes"];

    // [dict setObject:[NSMutableDictionary class]    forKey:@"cookies"];
     
    
    return dict;
}

-(void)
dealloc
{
    DEBUG_NO
    DebugLog(@"%@: HEADERS      - %lu", NSStringFromClass([self class]),  [_headers retainCount]);
    DebugLog(@"%@: QUERY PARAMS - %lu", NSStringFromClass([self class]),  [_queryParameters retainCount]);
    DebugLog(@"%@: SESH         - %lu", NSStringFromClass([self class]),  [_session retainCount]);
    DebugLog(@"%@: RESPONSE      - %lu", NSStringFromClass([self class]), RETAIN_COUNT(_response));

    RELEASE(_session);
    RELEASE(_hostName);
    RELEASE(_path);
    RELEASE(_headers);
    RELEASE(_cookies);
    RELEASE(_acceptedTypes);
    RELEASE(_uriParameters);
    RELEASE(_queryParameters);
    RELEASE(_response);

    
    GTK_DEALLOC(super);
}

-(void)
initLocalVars
{
    [self setHeaders:[NSMutableDictionary dictionary]];
    [self setQueryParameters:[NSMutableDictionary dictionary]];
}

#pragma mark - Accessors

M_ACCESSOR(NSMutableDictionary*,   _headers,         headers,         setHeaders);
M_ACCESSOR(NSString*,              _path,            path,            setPath);
M_ACCESSOR(id,                     _session,         session,         setSession);
S_ACCESSOR(GTKHTTPCode,            _HTTPCode,        HTTPCode,        setHTTPCode);
M_ACCESSOR(id,                     _acceptedTypes,   _acceptedTypes,  set_AcceptedTypes);


-(NSString*)
hostName
{
    if (!_hostName)
    {
        NSString* hostName = [_headers objectForKey:@"Host"];
    
        if (!hostName)
        {
            hostName = [_headers objectForKey:@"host"];
        }
        ASSIGN_ID_gtk(_hostName, hostName);
    }
    return _hostName;
}

#pragma mark - Cookies

M_SETTER(NSMutableDictionary*, _cookies, setCookies);
-(NSDictionary*)
cookies
{
    DEBUG_NO
    
    if (!_cookies) {
        DebugLog(@"Constructing cookies in headers: %@", _headers);
        
        NSString* cookieAsString = nil;
        
        cookieAsString = [_headers objectForKey:@"Cookie"];
        
        if (!cookieAsString)
        {
            cookieAsString = [_headers objectForKey:@"cookie"];
        }
        
        DebugLog(@"Cookie as string: %@", cookieAsString);
        
        if (cookieAsString) {
            // https://stackoverflow.com/questions/1969232/what-are-allowed-characters-in-cookies
            NSMutableDictionary* cookies = [NSMutableDictionary dictionary];

            NSArray* keyValuePairs = [cookieAsString componentsSeparatedByString:@";"];
            DebugLog(@"Key value pairs: %@", keyValuePairs);

            for (NSString* keyValueString in keyValuePairs) {
                DebugLog(@"Key value string: %@", keyValueString);
                NSRange eqSymbolRange = [keyValueString rangeOfString:@"=" options:NSLiteralSearch];
                if (eqSymbolRange.location == NSNotFound) {
                    continue;
                }
                NSUInteger lengthOfKeyValueString = [keyValueString length];

                if (eqSymbolRange.location == lengthOfKeyValueString) {
                    continue;
                }

                NSRange   keyRange   = NSMakeRange(0, eqSymbolRange.location);
                NSRange   valueRange = NSMakeRange(eqSymbolRange.location+1, lengthOfKeyValueString-eqSymbolRange.location-1);

                DebugLog(@"EQ Symbol Range : %@", NSStringFromRange(eqSymbolRange));
                DebugLog(@"Key Range       : %@", NSStringFromRange(keyRange));
                DebugLog(@"Value Range     : %@", NSStringFromRange(valueRange));

                NSString* key        = [[keyValueString substringWithRange:keyRange]   stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                NSString* value      = [[keyValueString substringWithRange:valueRange] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];

                DebugLog(@"Key     : %@", key);
                DebugLog(@"Value  : %@", value);

                [cookies setObject:value forKey:key];
            }
            
            [self setCookies:cookies];
        }
    }
    return _cookies;
}

#pragma mark - HTTP Method

// Can't use Macro because Apple defines HTTPMethod
// scalarAccessor(GTKHTTPMethod, HTTPMethod, setHTTPMethod)
-(GTKHTTPMethod)
HTTPMethod
{
    return _gtkHTTPMethod;
}

-(void)
setHTTPMethod:(GTKHTTPMethod)aMethod
{
    _gtkHTTPMethod = aMethod;
}


#pragma mark - Query Paramaters

M_SETTER(NSMutableDictionary*, _queryParameters, setQueryParameters);
-(NSMutableDictionary*)
queryParameters
{
    if (!_queryParameters)
    {
        _queryParameters = [[NSMutableDictionary alloc] init];
    }
    return _queryParameters;
}

M_SETTER(NSMutableDictionary*, _uriParameters, setURIParameters);
-(NSMutableDictionary*)
URIParameters
{
    if (!_uriParameters)
    {
        _uriParameters = [[NSMutableDictionary alloc] init];
    }
    return _uriParameters;
}

-(id)
queryParameterForKey:(id)theKey
{
    id toReturn = [[self queryParameters] objectForKey:theKey];
    
    if (!toReturn)
    {
        toReturn = [[self queryParameters] objectForKey:[theKey lowercaseString]];
    }
    
    return toReturn;
}

-(void)
setQueryParameter:(id)theValue
forKey:(id)theKey
{
    [[self queryParameters] setObject:theValue forKey:theKey];
}



#pragma mark - Headers

-(id)
headerForKey:(id)theKey
{
    return [[self headers] objectForKey:theKey];
}

-(void)
setHeader:(id)theValue
forKey:(id)theKey
{
    [[self headers] setObject:theValue forKey:theKey];
}



-(NSString*)
prefferedMimeType
{
    return [[self acceptedTypes] firstObject];
}

-(NSArray*)
acceptedTypes
{
    DEBUG_NO
    if (!_acceptedTypes) {
        // <MIME_type>/<MIME_subtype> — image/png - A single, precise MIME type, like text/html.
        // <MIME_type>/*              - image/*   - A MIME type, but without any subtype. image/* will match image/png, image/svg, image/gif and any other image types.
        // All/Any Mime Type Mark     -     */*
        // ;q= (q-factor weighting)   - :post     - Any value used is placed in an order of preference expressed using relative quality value called the weight.
        NSString* acceptedString = [[self headers] objectForKey:@"Accept"];

        DebugLog(@"Accepted Types: %@", acceptedString);

        NSArray* a_MimeAndMaybeQFactor = [acceptedString componentsSeparatedByString:@","];

        NSMutableArray* toBeAccepted = [NSMutableArray arrayWithCapacity:[a_MimeAndMaybeQFactor count]];

        for (NSString* mimeAndMaybeQFactor in a_MimeAndMaybeQFactor) {
            NSArray* pair = [mimeAndMaybeQFactor componentsSeparatedByString:@";q="];
            DebugLog(@"Pair: %@", pair);
            [toBeAccepted addObject:[pair firstObject]];
            // if ([pair count] == 1) {
            // }
        }

        DebugLog(@"Mime Types as array: %@", toBeAccepted);

        [self set_AcceptedTypes:toBeAccepted];
    }

    return _acceptedTypes;
}

#pragma mark - Response

GETTER_PLAIN(id, _response, response);
-(void)
setResponse:(id)theResponse
{
    if ([theResponse isKindOfClass:[GTKHTTPServerResponse class]])
    {
        ASSIGN_ID_gtk(_response, theResponse);
    }
    else
    {
        ASSIGN_ID_gtk(_response, [GTKHTTPServerResponse wrapObject:theResponse forRequest:self]);
    }
    
    _responseCode = [_response httpResponseCode];

}

-(GTKHTTPCode)
responseCode
{
    return _responseCode;
}

-(id)
payload
{
    return nil;
}

-(id)
postDataDict
{
    return nil;
}


-(id)
user
{
    return [[self session] user];
}

-(BOOL)
isAuthenticated
{
    return [[self session] isAuthenticated];
}

-(void)
describeOn:(NSMutableString*)aString
{
    [aString appendFormat:@"<%@ : %p", NSStringFromClass([self class]), self];
    [aString appendFormat:@" path=%@", [self path]];
    [aString appendFormat:@" user=%@", [self user] ?: @"NONE"];
    [aString appendFormat:@">"];
}

-(NSString*)
description
{
    NSMutableString* stream = [NSMutableString string];
    [self describeOn:stream];
    return stream;
}

-(void)
removeUnneccesaryItems
{
    RELEASE(_response);
    _response = nil;
}

@end


@implementation GTKHTTPRequest (Testing)

+(NSArray*)
    testSelectors
{
    return @[
        @"testCookieParsing",
    ];
}

+(void)
    testCookieParsing
{
    GTKHTTPRequest* request = [[GTKHTTPRequest alloc] init];

    [request setHeaders:[@{
        @"Cookie": @"KeyOne=ValueOne;KeyTwo=ValueTwo;KeyThree=ValueThree;"
    } mutableCopy]];

    NSDictionary* cookies = [request cookies];

    NSString* valueOne   = [cookies objectForKey:@"KeyOne"];
    NSString* valueTwo   = [cookies objectForKey:@"KeyTwo"];
    NSString* valueThree = [cookies objectForKey:@"KeyThree"];
    NSString* valueFour  = [cookies objectForKey:@"KeyFour"];

    EXPECT_ID(@"ValueOne", valueOne, @"Got valueOne");
    EXPECT_ID(@"ValueTwo", valueTwo, @"Got valueTwo");
    EXPECT_ID(@"ValueThree", valueThree, @"Got valueThree");
    EXPECT_NIL(valueFour, @"Should be nil at value four");
    // EXPECT_NIL(@"NotNil", @"Sanity Check");

}

@end
