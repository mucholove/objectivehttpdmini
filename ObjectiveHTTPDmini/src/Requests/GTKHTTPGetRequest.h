//
//  GTKHTTPGetRequest.h
//  ObjectiveHTTPDmini
//
//  Created by Gustavo Tavares on 5/29/20.
//  Copyright © 2020 Gustavo Tavares. All rights reserved.
//

#import <GTKFoundation/GTKFoundation.h>
#import <ObjectiveHTTPDmini/GTKMHDHTTPRequest.h>



@interface GTKHTTPGetRequest : GTKMHDHTTPRequest

@end


