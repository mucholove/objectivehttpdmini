//
//  GTKHTTPRequest.h
//  ObjectiveHTTPDmini
//
//  Created by Gustavo Tavares on 5/29/20.
//  Copyright © 2020 Gustavo Tavares. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GTKFoundation/GTKFoundation.h>


// NSObjCRuntime.h
// NS_TYPED_ENUM
// NS_TYPED_EXTENSIBLE_ENUM
// NS_EXTENSIBLE_STRING_ENUM
// NS_ERROR_ENUM
// NS_STRING_ENUM
//

@protocol GTKServerSession;



    
@protocol GTKServerRequest // Used for HTTP and WebSocket Router

ACCESSOR_H(GTKHTTPMethod,          HTTPMethod,      setHTTPMethod);
ACCESSOR_H(NSMutableDictionary*,   queryParameters, setQueryParameters);

-(id)user;

-(id)
queryParameterForKey:(id)theKey;

-(void)
setQueryParameter:(id)theValue
forKey:(id)theKey;

-(id)
headerForKey:(id)theKey;

-(void)
setHeader:(id)theValue
forKey:(id)theKey;

@end

@interface GTKHTTPRequest : NSObject <GTKServerRequest>

ACCESSOR_H(NSString*,              hostName,        setHostName);
ACCESSOR_H(NSMutableDictionary*,   headers,         setHeaders);
ACCESSOR_H(NSString*,              path,            setPath);
ACCESSOR_H(NSMutableDictionary*,   URIParameters,   setURIParameters);   //- /path/:parameter/slug
ACCESSOR_H(NSMutableDictionary*,   queryParameters, setQueryParameters); //- /path?parameter=hello
ACCESSOR_H(id,                     session,         setSession);
ACCESSOR_H(GTKHTTPMethod,          HTTPMethod,      setHTTPMethod);
ACCESSOR_H(GTKHTTPCode,            HTTPCode,        setHTTPCode);
ACCESSOR_H(id,                     response,        setResponse);

-(NSString*)
prefferedMimeType;

-(NSArray*)
acceptedTypes;

-(void)
initLocalVars;

-(NSDictionary*)
cookies;

-(NSString*)
log;

-(void)
writeOnLog:(NSMutableString*)theLog;

-(NSData*)
payload;

-(NSData*)
postDataDict;

#pragma mark Sugar :)

-(BOOL)
isAuthenticated;

-(id)
user;

-(GTKHTTPCode)
responseCode;

-(void)
removeUnneccesaryItems;

@end






///*!
//    @category NSURLRequest(NSHTTPURLRequest)
//    The NSHTTPURLRequest on NSURLRequest provides methods for accessing
//    information specific to HTTP protocol requests.
//*/
//@interface NSURLRequest (NSHTTPURLRequest)
//
///*!
//    @abstract Returns the HTTP request method of the receiver.
//    @result the HTTP request method of the receiver.
//*/
//@property (nullable, readonly, copy) NSString* HTTPMethod;
//
///*!
//    @abstract Returns a dictionary containing all the HTTP header fields
//    of the receiver.
//    @result a dictionary containing all the HTTP header fields of the
//    receiver.
//*/
//@property (nullable, readonly, copy) NSDictionary<NSString* , NSString* > *allHTTPHeaderFields;
//
///*!
//    @method valueForHTTPHeaderField:
//    @abstract Returns the value which corresponds to the given header
//    field. Note that, in keeping with the HTTP RFC, HTTP header field
//    names are case-insensitive.
//    @param field the header field name to use for the lookup
//    (case-insensitive).
//    @result the value associated with the given header field, or nil if
//    there is no value associated with the given header field.
//*/
//- (NSString*)valueForHTTPHeaderField:(NSString*)field;
//
///*!
//    @abstract Returns the request body data of the receiver.
//    @discussion This data is sent as the message body of the request, as
//    in done in an HTTP POST request.
//    @result The request body data of the receiver.
//*/
//@property (nullable, readonly, copy) NSData* HTTPBody;
//
///*!
//    @abstract Returns the request body stream of the receiver
//    if any has been set
//    @discussion The stream is returned for examination only; it is
//    not safe for the caller to manipulate the stream in any way.  Also
//    note that the HTTPBodyStream and HTTPBody are mutually exclusive - only
//    one can be set on a given request.  Also note that the body stream is
//    preserved across copies, but is LOST when the request is coded via the
//    NSCoding protocol
//    @result The request body stream of the receiver.
//*/
//@property (nullable, readonly, retain) NSInputStream *HTTPBodyStream;
//
///*!
//    @abstract Determine whether default cookie handling will happen for
//    this request.
//    @discussion NOTE: This value is not used prior to 10.3
//    @result YES if cookies will be sent with and set for this request;
//    otherwise NO.
//*/
//@property (readonly) BOOL HTTPShouldHandleCookies;
//
///*!
// @abstract Reports whether the receiver is not expected to wait for the
// previous response before transmitting.
// @result YES if the receiver should transmit before the previous response
// is received.  NO if the receiver should wait for the previous response
// before transmitting.
// */
//@property (readonly) BOOL HTTPShouldUsePipelining API_AVAILABLE(macos(10.7), ios(4.0), watchos(2.0), tvos(9.0));
//
//@end
