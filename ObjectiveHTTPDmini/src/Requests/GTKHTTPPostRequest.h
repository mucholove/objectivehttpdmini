//
//  GTKHTTPPostRequest.h
//  ObjectiveHTTPDmini
//
//  Created by Gustavo Tavares on 5/29/20.
//  Copyright © 2020 Gustavo Tavares. All rights reserved.
//

#import <ObjectiveHTTPDmini/GTKMHDHTTPRequest.h>



@interface GTKHTTPPostRequest : GTKMHDHTTPRequest

+(instancetype)
processorForSession:(id)aUser
onConnection:(void*)aConnection;

-(void)
appendBytes:(const void*)bytes
length:(int)len
toKey:(NSString*)key
filename:(NSString*)filename
contentType:(NSString*)contentType;

-(void)
addParameters:(NSDictionary*)additionalParameters;

-(int)
appendBytes:(const char*)upload_data
length:(size_t*)upload_data_size
error:(NSError**)outErr;

-(void)
setPayload:(NSMutableData*)theData;

-(NSMutableData*)
payload;

-(void)
appendToPayloadBytes:(void*)theBytes
length:(NSUInteger)theLength;

-(void)
appendToPayloadData:(NSData*)theData;



/*
    void*
pointer in MHD_PostProcessor is created by:
     _MHD_EXTERN struct MHD_PostProcessor *
     MHD_create_post_processor (
         struct MHD_Connection *connection
         ,size_t buffer_size
         ,MHD_PostDataIterator iter
         , void *iter_cls);
 */

-(void*)
processor;

-(void)
setProcessor:(void*)newProcessor;

-(NSMutableDictionary*)
postDataDict;

-(NSMutableData*)
jsonData;

-(NSString*)
stringForFormKey:(NSString*)theFormKey;

@end


