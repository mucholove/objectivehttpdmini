//
//  GTKMHDHTTPRequest.h
//  ObjectiveHTTPDmini
//
//  Created by Gustavo Tavares on 6/16/21.
//  Copyright © 2021 Gustavo Tavares. All rights reserved.
//

#import <ObjectiveHTTPDmini/GTKHTTPRequest.h>

@interface GTKMHDHTTPRequest : GTKHTTPRequest

+(instancetype)
requestOnConnection:(void*)aConnection
session:(id<GTKServerSession>)theSession
ofHTTPType:(GTKHTTPMethod)theHTTPMethod;

+(instancetype)
requestOnConnection:(void*)aConnection
session:(id<GTKServerSession>)theSession;

-(instancetype)
initWithConnection:(void*)aConnection
session:(id<GTKServerSession>)theSession NS_DESIGNATED_INITIALIZER;


#pragma mark - Accessors

ACCESSOR_H(id, server, setServer);

-(void)
setMHDResponse:(void*)theMHDResponse;

-(void*)
MHDResponse;



ACCESSOR_H(int,   responseCode,    setResponseCode)
ACCESSOR_H(BOOL,  isStuffed,       setIsStuffed);
ACCESSOR_H(id,    responder,       setResponder);
ACCESSOR_H(void*, connection,      setConnection);


@end
