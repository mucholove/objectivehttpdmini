//
//  GTKHTTPPostRequest.m
//  ObjectiveHTTPDmini
//
//  Created by Gustavo Tavares on 5/29/20.
//  Copyright © 2020 Gustavo Tavares. All rights reserved.
//

#import <ObjectiveHTTPDmini/GTKHTTPPostRequest.h>
#include <microhttpd.h>
#import <ObjectiveHTTPDmini/GTKHTTPServer.h>

@implementation GTKHTTPPostRequest
{
    NSData*               requestBody;
    NSMutableDictionary*  postDataDict;
    NSMutableDictionary*  filenames;
    NSMutableDictionary*  contentTypes;
    id                    lastKey;
    void*                 processor;
}

-(void)dealloc
{
    DEBUG_NO
    DebugLog(@"%@: VALUES         - %lu", NSStringFromClass([self class]), [postDataDict retainCount]);
    DebugLog(@"%@: FILENAMES      - %lu", NSStringFromClass([self class]), [filenames retainCount]);
    DebugLog(@"%@: CONTENT_TYPES  - %lu", NSStringFromClass([self class]), [contentTypes retainCount]);
    if (processor != NULL) {
        MHD_destroy_post_processor(processor);
    }

    RELEASE(requestBody);
    RELEASE(postDataDict);
    RELEASE(filenames);
    RELEASE(contentTypes);
    RELEASE(lastKey);

    GTK_DEALLOC(super);
}



+(instancetype)
processorForSession:(id)theSession
onConnection:(void*)theConnection
{
    return AUTORELEASE([[self alloc]
                            initWithConnection:theConnection
                            session:theSession]);
}

objectAccessor(NSData*,
    requestBody,
    setRequestBody)

scalarAccessor(void*,
    processor,
    setProcessor )

objectAccessor(NSMutableDictionary*,
    postDataDict,
    setPostDataDict)

objectAccessor(NSMutableDictionary*,
    filenames,
    setFilenames )

objectAccessor(NSMutableDictionary*,
    contentTypes,
    setContentTypes )

managedAccessor(id, lastKey, setLastKey )

-(instancetype)
initWithConnection:(void*)theConnection
session:(id<GTKServerSession>)theSession
{
    self = [super initWithConnection:theConnection session:theSession];

    if (self)
    {
        [self initLocalVars];
    }

    return self;
}

-(void)
initLocalVars
{
    [super initLocalVars];
    [self setHTTPMethod:GTKHTTPMethod_Post];
    [self setPostDataDict:[NSMutableDictionary dictionary]];
    [self setFilenames:[NSMutableDictionary dictionary]];
    [self setContentTypes:[NSMutableDictionary dictionary]];
}


-(void)
appendBytes:(const void*)bytes
length:(int)len
toKey:(NSString*)key
filename:(NSString*)filename
contentType:(NSString*)contentType
{
    NSMutableData* data
        = [[self postDataDict] objectForKey:key];

    // AlwaysLog(@"%@ will: appendBytes: %*s length: %d toKey: %@ filename: %@ contentType: %@"
    //          ,NSStringFromClass([self class])
    //          ,len
    //          ,bytes
    //          ,len
    //          ,key
    //          ,filename
    //          ,contentType);
    /*
    NSString* valueKey;

    if (filename && [filename length]) {
        valueKey = filename;
    } else {
        valueKey = @"data";
    }
    */

    if ( !data  )
    {

        /*
        #if 0
        if ( [self lastKey] != key ) {
            if ( [self lastKey] ) {
                AlwaysLog(@"done with '%@' got %d bytes"
                    ,[self lastKey]
                    ,[[[self values] objectForKey:lastKey] length]);
            }
            [self setLastKey:key];
        }
        AlwaysLog(@"Start POST for '%@'->'%@'",key,filename);
        #endif
        */
        data  = [NSMutableData data];

        [[self postDataDict]
            setObject:data
            forKey:key];
    }

    [data
        appendBytes:bytes
        length:len];

    if (   ![[self filenames] objectForKey:key]
        && filename
    ) {
        [[self filenames]
            setObject:filename
            forKey:key];
    }
}

-(void)
    addParameters:(NSDictionary*)additionalParameters
{
    for ( NSString* key in [additionalParameters allKeys] )
    {
        if ( ![[self postDataDict] objectForKey:key] )
        {
            [[self postDataDict]
                setObject:[CAST(NSString*)[additionalParameters objectForKey:key]
                                    dataUsingEncoding:NSISOLatin1StringEncoding]
                forKey:key];
        }
    }
}

-(NSMutableData*)
jsonData
{
    return [self payload];
}

-(void)
setPayload:(NSMutableData*)theData
{
    [[self postDataDict] setObject:theData forKey:@"data"];
}


-(NSMutableData*)
payload
{
    DEBUG_NO

    NSMutableData* data = [[self postDataDict] objectForKey:@"data"];

    if (!data) {
        data = [NSMutableData data];
        [[self postDataDict] setObject:data forKey:@"data"];
    }

    return data;
}

-(void)
appendToPayloadBytes:(void*)theBytes
length:(NSUInteger)theLength
{
    NSMutableData* payload = [self payload];
    [payload appendBytes:theBytes length:theLength];
}


-(void)
appendToPayloadData:(NSData*)theData
{
    NSMutableData* payload = [self payload];
    [payload appendData:theData];
}



-(NSString*)longDescription
{
    return [NSString
            stringWithFormat:
            @"<%@:%p:  value: %@ filenames: %@ contentTypes: %@"
                ,[self class]
                ,self
                ,postDataDict
                ,filenames
                ,contentTypes];
}

-(void)
    describeOn:(NSMutableString*)aString
{
    [aString appendFormat:@"<%@ : %p",   NSStringFromClass([self class]), self];
    [aString appendFormat:@" path=%@",   [self path]];
    [aString appendFormat:@" user=%@",   [self user] ?: @"NONE"];
    [aString appendFormat:@" bytes=%lu", [[self payload] length] ?: 0 ];
    [aString appendFormat:@">"];
}

-(NSString*)
    description
{
    NSMutableString* stream = [NSMutableString string];
    [self describeOn:stream];
    return stream;
}

-(NSString*)
stringForFormKey:(NSString*)theFormKey
{
    NSData* theData
        = [[self postDataDict] objectForKey:theFormKey];

    NSString* theString
        = [[NSString alloc] initWithData:theData encoding:NSISOLatin1StringEncoding];

    return theString;
}

-(BOOL)
shouldDebug
{
    return [[self server] shouldDebugPath:[self path]];
}

-(int)
appendBytes:(const char*)upload_data
length:(size_t*)upload_data_size
error:(NSError**)outErr
{
    DEBUG_NO

    debug           = [self shouldDebug];
    void* processor = [self processor];

    if (processor)
    {
        DebugLog(@"Will `post_process` with %@/%p", self, [self processor]);
        DebugLog(@"Data to process...size: %d", (int)*upload_data_size);
        // https://stackoverflow.com/questions/2137779/how-do-i-print-a-non-null-terminated-string-using-printf
        DebugLog(@"%.*s", (int)*upload_data_size, upload_data);
        BOOL didProcess = NO;

        didProcess = MHD_post_process (
                        processor,
                        upload_data,
                        *upload_data_size);

        if (didProcess == MHD_YES)
        {
            DebugLog(@"Did `post_process`");
            *upload_data_size = 0;
            return MHD_YES;
        }
        else
        {
            DebugLog(@"POST PROCESS FAIL...");
            return MHD_NO;
        }
    }
    else
    {
        DebugLog(@"Will get JSON data");
        NSMutableData* jsonData = [self payload];

        DebugLog(@"Will apend to JSON data %@", jsonData);
        [jsonData appendBytes:upload_data
                  length:(NSUInteger)*upload_data_size];
        DebugLog(@"Did apend to JSON data");

        *upload_data_size = 0;

        return MHD_YES;
    }
}

-(void)
removeUnneccesaryItems
{
    RELEASE(requestBody);
    requestBody = nil;
    
    RELEASE(filenames);
    filenames = nil;
    
    
    RELEASE(postDataDict);
    postDataDict = nil;
    
    if (processor != NULL) {
        MHD_destroy_post_processor(processor);
        processor = NULL;
    }
    
    [super removeUnneccesaryItems];
}


@end



