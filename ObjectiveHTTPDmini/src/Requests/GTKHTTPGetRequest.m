//
//  GTKHTTPGetRequest.m
//  ObjectiveHTTPDmini
//
//  Created by Gustavo Tavares on 5/29/20.
//  Copyright © 2020 Gustavo Tavares. All rights reserved.
//

#import <ObjectiveHTTPDmini/GTKHTTPGetRequest.h>

@implementation GTKHTTPGetRequest

-(instancetype)
initWithConnection:(void*)aConnection
session:(id<GTKServerSession>)theSession
{
    self = [super initWithConnection:aConnection session:theSession];
    
    if (self)
    {
        [self initLocalVars];
    }
    
    return self;
}

-(void)
initLocalVars
{
    [super initLocalVars];
    [self setHTTPMethod:GTKHTTPMethod_Get];
}

#if !__has_feature(objc_arc)
-(void)
    dealloc
{
    [super dealloc];
}
#endif

@end
