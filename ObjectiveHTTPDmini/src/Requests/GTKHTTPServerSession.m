//
//  GTKHTTPServerSession.m
//  ObjectiveHTTPDmini
//
//  Created by Gustavo Tavares on 8/21/20.
//  Copyright © 2020 Gustavo Tavares. All rights reserved.
//

#import <GTKFoundation/GTKFoundation.h>
#import <ObjectiveHTTPDmini/GTKHTTPServerSession.h>

@implementation GTKHTTPServerSession

#if !__has_feature(objc_arc)
-(void)
    dealloc
{
    [_user release];
    [_ipAddresses release];
    [_requestHistory release];
    [super dealloc];

}
#endif


-(BOOL)
    isAuthenticated
{
    return [self user] ? YES : NO;
}

+(instancetype)session
{
    return AUTORELEASE([[GTKHTTPServerSession alloc] init]);
}

-(void)
    describeOn:(NSMutableString*)aString
{
    [aString appendFormat:@"<%@ : %p", NSStringFromClass([self class]), self];
    [aString appendFormat:@" auth=%@", [self isAuthenticated] ? @"YES" : @"NO"];
    [aString appendFormat:@" user=%@", [self user] ?: @"NONE"];
    [aString appendFormat:@">"];
}

-(NSString*)
    description
{
    NSMutableString* stream = [NSMutableString string];
    [self describeOn:stream];
    return stream;
}


@end
