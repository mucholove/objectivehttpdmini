//
//  GTKMHDHTTPRequest.m
//  ObjectiveHTTPDmini
//
//  Created by Gustavo Tavares on 6/16/21.
//  Copyright © 2021 Gustavo Tavares. All rights reserved.
//

#import <ObjectiveHTTPDmini/GTKMHDHTTPRequest.h>
#import <ObjectiveHTTPDmini/GTKHTTPGetRequest.h>
#import <ObjectiveHTTPDmini/GTKHTTPPostRequest.h>
#import <ObjectiveHTTPDmini/GTKHTTPServerResponse.h>
#import <microhttpd.h>
#import <ObjectiveHTTPDmini/GTKServerAuthDelegate.h>

@implementation GTKMHDHTTPRequest
{
    //--- Request Related
    #if HAS_ARC
        __weak id               _server;
    #else
        id                      _server;
    #endif
    //--- Response Related
    id                      _responder;
    void*                   _connection;
    void*                   _mhdResponse;
    BOOL                    _isStuffed;
}


-(void)
dealloc
{
    DEBUG_NO

    if (_mhdResponse != NULL) {
        MHD_destroy_response(_mhdResponse);
    }


    RELEASE(_responder);
       
    GTK_DEALLOC(super);
}

#pragma mark - Accessors

M_ACCESSOR(id,        _responder,       responder,       setResponder);
S_ACCESSOR(BOOL,      _isStuffed,       isStuffed,       setIsStuffed);
S_ACCESSOR(id,        _server,          server,          setServer);
S_ACCESSOR(void*,     _connection,      connection,      setConnection);
S_ACCESSOR(void*,     _mhdResponse,     MHDResponse,     setMHDResponse);

#pragma mark - Initalizers

+(instancetype)
requestOnConnection:(void*)theConnection
session:(id<GTKServerSession>)theSession
ofHTTPType:(GTKHTTPMethod)theHTTPMethod;
{
    GTKHTTPRequest* instance = nil;

    switch (theHTTPMethod) {
        case GTKHTTPMethod_Get:
            instance = [GTKHTTPGetRequest
                        requestOnConnection:theConnection
                        session:theSession];
            break;
        case GTKHTTPMethod_Post:
            instance = [GTKHTTPPostRequest
                        requestOnConnection:theConnection
                        session:theSession];
            break;
        default:
            instance = [self requestOnConnection:theConnection session:theSession];
            [instance setHTTPMethod:theHTTPMethod];
            break;
    }

    return instance;
}

+(instancetype)
requestOnConnection:(void*)theConnection
session:(id<GTKServerSession>)theSession
{
    return AUTORELEASE([[self alloc] initWithConnection:theConnection session:theSession]);
}

-(instancetype)
initWithConnection:(void*)theConnection
session:(id<GTKServerSession>)theSession
{
    self = [super init];

    if (self)
    {
        [self initLocalVars];
        [self setSession:theSession];
        [self setConnection:theConnection];
        _mhdResponse = NULL;
    }

    return self;
}

-(instancetype)
init
{
    self = [self initWithConnection:nil
                 session:nil];
    
    return self;
}

@end
