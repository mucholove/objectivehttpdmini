//
//  GTKHTTPServerSession.h
//  ObjectiveHTTPDmini
//
//  Created by Gustavo Tavares on 8/21/20.
//  Copyright © 2020 Gustavo Tavares. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface GTKHTTPServerSession : NSObject

+(instancetype)session;
-(BOOL)isAuthenticated;
@property (retain) id user;
@property (retain) id ipAddresses;
@property (retain) id requestHistory;

@end


