//
//  GTKHTTPRuntimeNestablePath.m
//  ObjectiveHTTPDmini
//
//  Created by Gustavo Tavares on 9/22/20.
//  Copyright © 2020 Gustavo Tavares. All rights reserved.
//

#import <ObjectiveHTTPDmini/GTKHTTPRuntimeNestablePath.h>

@implementation GTKHTTPRuntimeNestablePath
{
    GTKHTTPServer *parentServer;
}
#if !__has_feature(objc_arc)
-(void)
    dealloc
{
    [parentServer release];
    [super dealloc];

}
#endif

managedAccessor(GTKHTTPServer*,
    parentServer,
    setParentServer)

-(BOOL)
requiresAuthentication
{
    return YES;
}

- (NSString*)
mimeType
{
    return [MimeType html];
}

+(instancetype)
pathServerWithParent:(GTKHTTPServer*)theParent
{
    return [[self alloc] initWithParentServer:theParent];
}

-(instancetype)
initWithParentServer:(GTKHTTPServer*)theParent
{
    self = [super init];

    if (self)
    {
        [self setParentServer:theParent];
    }

    return self;
}

-(id)
serveRequest:(GTKHTTPRequest*)aRequest
{
    NSString* aPath = [aRequest path];

    NSString* basicPage
        = [NSString stringWithFormat:
             @"<html><head></head><body>"
             @"%@ is Seriving GET %@ :)"
             @"</body></html>\n"
                 ,[self class]
                 ,aPath];

    AlwaysLog(@"%@: Returning %@", NSStringFromClass([self class]), basicPage);

    return basicPage;
}
@end
