#import <ObjectiveHTTPDmini/GTKHTTPRuntimeNestablePath.h>

@interface GTKFolderResponder : GTKHTTPRuntimeNestablePath

ACCESSOR_H(NSString*, prefix,           setPrefix)
ACCESSOR_H(NSString*, pathToFolder,     setPath)
ACCESSOR_H(NSArray*,  whiteListedTypes, setWhiteListedTypes)
ACCESSOR_H(NSArray*,  blackListedTypes, setBlackListedTypes)

@end
