//
//  GTKHTTPPath2ClassDictionaryServer.m
//  ObjectiveHTTPDmini
//
//  Created by Gustavo Tavares on 8/21/20.
//  Copyright © 2020 Gustavo Tavares. All rights reserved.
//

#import <ObjectiveHTTPDmini/GTKHTTPPath2ClassDictionaryServer.h>

#import <ObjectiveHTTPDmini/GTKHTTPServerSession.h>
#import <ObjectiveHTTPDmini/GTKHTTPGetRequest.h>
#import <ObjectiveHTTPDmini/GTKMHDHTTPRequest.h>

#import <ObjectiveHTTPDmini/GTKHTTPRuntimeNestablePath.h>
#import <ObjectiveHTTPDmini/GTKHTTPSwitchPath.h>

#import <ObjectiveHTTPDmini/GTKDebugWebSocketsPath.h>
#import <ObjectiveHTTPDmini/GTKFolderResponder.h>

#if __has_feature(objc_arc)
    #define POOL_START @autoreleasepool {

    #define POOL_END   }

#else
    #if GNUSTEP
        #define POOL_START NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];

        #define POOL_END  [pool drain];

    #else
        #define POOL_START @autoreleasepool {

        #define POOL_END   }

    #endif
#endif


@implementation GTKHTTPPath2ClassDictionaryServer
{
    NSMutableDictionary* _routesToClasses;
    NSMutableDictionary* _routesToObjects;
    NSMutableDictionary* _parametrizedRoutesComponentsToObjects;
    NSMutableDictionary* _folderResponders;
}

-(NSMutableDictionary*)
folderResponders
{
    if (!_folderResponders)
    {
        _folderResponders = [[NSMutableDictionary alloc] init];
    }
    return _folderResponders;
}

#if !__has_feature(objc_arc)
-(void)
dealloc
{
    [_routesToClasses release];
    [_routesToObjects release];
    RELEASE(_folderResponders);
    [_parametrizedRoutesComponentsToObjects release];
    [super dealloc];

}
#endif

+(NSDictionary*)
defaultRoutesToClasses
{
    return @{};
}

+(id)
server
{
    NSDictionary* d = [self defaultRoutesToClasses];
    return AUTORELEASE([[self alloc] initWithRouteDictionary:d]);
}

+(instancetype)
serverWithRouteDictionary:(NSDictionary*)theRouteDictionary
{
    return AUTORELEASE([[self alloc] initWithRouteDictionary:theRouteDictionary]);
}

-(void)
initLocalVars
{
    [super initLocalVars];
}

-(instancetype)
initWithRouteDictionary:(NSDictionary*)theRouteDictionary
{
    self = [super init];

    if (self)
    {
        NSDictionary*        routeToClassMapping = theRouteDictionary;
        NSMutableDictionary* instanceMap         = [NSMutableDictionary dictionaryWithCapacity:[routeToClassMapping count]];

        [self setRoutesToObjects:instanceMap];

        [self fromDictionary:routeToClassMapping
              initPathsOnMutableDictionary:instanceMap];
              
        // if ([self debugWebSockets])
        // {
            [self addRoute:[GTKDebugWebSocketsPath class]
                  atPath:@"/debugWebSockets"];
                  
            [self addRoute:[GTKDebugOneWebSocketPath class]
                  atPath:@"/debug/webSocket"];
        // }
        // NSDictionary* authPathMapping = [self authenticatedRouteToClassMapping];
        // [self setRouteToObjectMapping:[NSMutableDictionary dictionaryWithCapacity:[authPathMapping count]]];
        // [self fromDictionary:authPathMapping initPathsOnMutableDictionary:[self authenticatedPaths]]

    }

    return self;
}

-(void)
addRoute:(id)theRoute
atPath:(id)thePath
{
    DEBUG_NO
    
    id instance = nil;
    
    if ([theRoute respondsToSelector:@selector(alloc)])
    {
        Class classToInit = (Class)theRoute;
        DebugLog(@"Add route of class: %@ at path: %@", NSStringFromClass(classToInit), thePath);
        if ([classToInit respondsToSelector:@selector(pathServerWithParent:)])
        {
            instance = [classToInit pathServerWithParent:self];
        }
        else
        {
            instance = [[classToInit alloc] init];
        }
    }
    else
    {
        instance = theRoute;
    }
    
    
    if ([thePath isKindOfClass:[NSString class]])
    {
        [self setResponder:instance
              forPath:thePath];
    }
    else if ([thePath isKindOfClass:[NSArray class]])
    {
        for (NSString* theString in thePath)
        {
            [self setResponder:instance
                  forPath:theString];
        }
    }
    else
    {
        @throw [NSException
            exceptionWithName:@"WrongKindOfObject"
            reason:@"Object needs to be array or string"
            userInfo:nil];
    }
}

-(void)
setResponder:(id)instance
forPath:(NSString*)casedPath
{
    DEBUG_NO
    
    NSString* lowercasedPath = [casedPath lowercaseString];
    
    DebugLog(@"Set responder %@ at path: %@", instance, lowercasedPath);
    
    if ([instance respondsToSelector:@selector(setServer:)])
    {
        [instance setServer:self];
    }
    
    if ([instance isKindOfClass:[NSString class]])
    {
        GTKFolderResponder* folderResponder = [[GTKFolderResponder alloc] init];
        // [folderResponder addPrefix:lowercasedPath];
        [folderResponder setPrefix:lowercasedPath];
        [folderResponder setPath:instance];
        [[self folderResponders]
            setObject:folderResponder
            forKey:lowercasedPath];
        return;
    }
    
    if ([lowercasedPath containsString:@"/%"])
    {
        NSArray* pathComponents = [lowercasedPath componentsSeparatedByString:@"/"];
        
        [[self parametrizedRoutesComponentsToObjects]
            setObject:instance
            forKey:pathComponents];
    }
    else
    {
        [[self routesToObjects]
            setObject:instance
            forKey:lowercasedPath];
    }
}

M_SETTER(NSMutableDictionary*, _parametrizedRoutesComponentsToObjects, setParametrizedRoutesComponentsToObjects);
-(NSMutableDictionary*)
parametrizedRoutesComponentsToObjects
{
    if (!_parametrizedRoutesComponentsToObjects)
    {
        ASSIGN_ID_gtk(_parametrizedRoutesComponentsToObjects, [NSMutableDictionary dictionary]);
    }
    return _parametrizedRoutesComponentsToObjects;
}

M_ACCESSOR(NSMutableDictionary*
    ,_routesToClasses
    ,routesToClasses
    ,setRoutesToClasses);

M_ACCESSOR(NSMutableDictionary*
    ,_routesToObjects
    ,routesToObjects
    ,setRoutesToObjects);

-(void)
fromDictionary:(NSDictionary*)sourceDict
initPathsOnMutableDictionary:(NSMutableDictionary*)destinationDict
{
    for (id key in sourceDict)
    {
        Class classToInit
            = [sourceDict objectForKey:key];

        [self addRoute:classToInit
              atPath:key];
    }

}


-(id)
notAuthorizeddResponseForRequest:(GTKHTTPRequest*)theRequest
{
    DEBUG_NO

    NSString* thePath = [theRequest path] ?: @"PATH_UNKNOWN";

    NSError* notAuthorizedError = [NSError
        errorWithDomain:@"ServerError"
        code:GTKHTTPCodeNotAuthorized
        userInfo:@{
        	@"class" : [self class],
        	@"path"  :  thePath
        }];

    DebugLog(@"%@: Returning %@", NSStringFromClass([self class]), notAuthorizedError);

    return notAuthorizedError;
}

-(id)
notAuthenticatedResponseForRequest:(GTKHTTPRequest*)theRequest
{
    DEBUG_NO
    NSString* thePath = [theRequest path] ?: @"PATH_UNKNOWN";

    NSError* notAuthenticatedError = [NSError
        errorWithDomain:@"ServerError"
        code:GTKHTTPCodeAuthFailed
        userInfo:@{
            @"class" : [self class],
            @"path": thePath
        }];

    DebugLog(@"%@: Returning %@", NSStringFromClass([self class]), notAuthenticatedError);
    
    GTKHTTPServerResponse* response = [[GTKHTTPServerResponse alloc] init];
    
    NSString* mimeType = [theRequest prefferedMimeType] ?: [MimeType json];
    
    [response setMIMEType:mimeType];
    [response setHTTPResponseCode:401];
    [response setRawData:[notAuthenticatedError asData]];

    return response;
    
    
}

-(id)
doesNotExistResponseForRequest:(GTKHTTPRequest*)theRequest
{
    DEBUG_NO
    NSString* thePath = [theRequest path] ?: @"PATH_UNKNOWN";

    NSError* notFoundError = [NSError
        errorWithDomain:@"ServerError"
        code:GTKHTTPCodeNotFound userInfo:@{
            @"class" : [self class],
            @"path": thePath
        }];

    DebugLog(@"%@: Returning %@", NSStringFromClass([self class]), notFoundError);

    GTKHTTPServerResponse* response = [[GTKHTTPServerResponse alloc] init];
    
    NSString* mimeType = [theRequest prefferedMimeType] ?: [MimeType json];
    
    [response setMIMEType:mimeType];
    [response setHTTPResponseCode:404];
    [response setRawData:[notFoundError asData]];

    return response;
}

-(id)
responderForRequest:(GTKHTTPRequest*)theRequest
{
    DEBUG_YES
    
    __block id responder = nil;
    
    NSString* path = [[theRequest path] lowercaseString];
    
    responder = [[self routesToObjects] objectForKey:path];
    
    if (responder)
    {
        return responder;
    }
    
    #pragma Try Folder Path
    
    [[self folderResponders]
     enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL* stop){
        if ([path hasPrefix:key])
        {
            responder = obj;
            *stop     = YES;
        }
    }];
    
    if (responder)
    {
        return responder;
    }
    
    #pragma Try Parametrized Path
    
    NSArray*    pathComponents       = [path componentsSeparatedByString:@"/"];
    NSUInteger  pathComponentsCount  = [pathComponents count];
    
    if ((!pathComponentsCount) || (pathComponentsCount == 1))
    {
        return nil;
    }
    
    VAR(__block BOOL)    didMatch    = NO;
    
    for (NSArray* candidateComponents in [self parametrizedRoutesComponentsToObjects])
    {
        didMatch     = YES;
        NSUInteger  candidateComponentsCount = [candidateComponents count];
        
        if (pathComponentsCount != candidateComponentsCount)
        {
            continue;
        }
        
        __block NSMutableDictionary* uriParameters = nil;
        
        
        [candidateComponents
            enumerateObjectsUsingBlock:
            ^(id candidateComponent, NSUInteger idx, BOOL* stop)
            {
                NSString* requestedComponent = [pathComponents objectAtIndex:idx];
            
                if ([candidateComponent hasPrefix:@"%"])
                {
                    NSString* paramKey   = [candidateComponent substringFromIndex:1];
                    NSString* paramValue = requestedComponent;
                
                    if (!uriParameters)
                    {
                        uriParameters = [NSMutableDictionary dictionary];
                    }
                
                    [uriParameters setObject:paramValue forKey:paramKey];
                }
                else
                {
                    if (![candidateComponent isEqual:requestedComponent])
                    {
                        *stop = YES;
                        didMatch = NO;
                    }
                }
            }];
        
        if (didMatch)
        {
            responder = [[self parametrizedRoutesComponentsToObjects] objectForKey:candidateComponents];
            [theRequest setURIParameters:uriParameters];
            break;
        }
    }
    
    return responder;
}

-(BOOL)
respondsToRequest:(GTKHTTPRequest*)theRequest
{
    DEBUG_NO
    id responderForRequest = [self responderForRequest:theRequest];
    DebugLog(@"Responder for request: %@", responderForRequest);
    if (responderForRequest)
    {
        return YES;
    } else {
        return NO;
    }
}


-(id)
serveRequest:(GTKMHDHTTPRequest*)theRequest
{
    DEBUG_NO
    
    NSString* aPath = [theRequest path];

    id<IsGTKHTTPPathForClassDictionary> responder = [self responderForRequest:theRequest];

    AlwaysLog(@"%@: Serving request %@ for %@ with responder: %@", NSStringFromClass([self class]), theRequest, [theRequest path], responder);

    id response =  nil;

    // #if HAS_ARC
    // @autoreleasepool {
    // #else
    // id pool = [[NSAutoreleasePool alloc] init];
    // #endif

    if (responder)
    {
        BOOL requiresAuthentication = YES;
        
        BOOL respondsToRequiresAuthentication = [responder respondsToSelector:@selector(requiresAuthentication)];
        
        if (respondsToRequiresAuthentication)
        {
            requiresAuthentication = [responder requiresAuthentication];
        }
    
        if ((requiresAuthentication && [theRequest isAuthenticated]) || !requiresAuthentication)
        {
            DebugLog(@"Will respond for request  : %@", theRequest);
            DebugLog(@"Will respond for session  : %@", [theRequest session]);
            DebugLog(@"Will respond for user     : %@", [[theRequest session] user]);
        
            BOOL isAuthorized = YES;
            
            if ([responder respondsToSelector:@selector(isAuthorizedRequest:)])
            {
                isAuthorized = [responder isAuthorizedRequest:theRequest];
            }
            
            if (isAuthorized)
            {
                response = [responder serveRequest:theRequest];
            }
            else
            {
                response = [self notAuthorizeddResponseForRequest:theRequest];
            }
        } else {
            DebugLog(@"Reponder requires authentication. Returning 401.");
            response =  [self notAuthenticatedResponseForRequest:theRequest];
        }
    }
    else
    {
        response = [self doesNotExistResponseForRequest:theRequest];
    }

    // #if HAS_ARC
    // }
    // #else
    // [pool drain];
    // #endif

    return response;
}

/*

-(id)parentServer {
    SUB_CLASS_REPONSIBILITY
    return nil;
}


+(id)pathServerWithParent:(id)parent {
    SUB_CLASS_REPONSIBILITY
    return nil;
}

-(void)setParentServer:(id)newVar {
    SUB_CLASS_REPONSIBILITY
}


- (NSString*)mimeType {
    SUB_CLASS_REPONSIBILITY
    return nil;
}

- (int)port {
    SUB_CLASS_REPONSIBILITY
    return 0;
}

-(void)setPort:(int)newVar {
    SUB_CLASS_REPONSIBILITY
}

-(BOOL)startSecureHTTPServerUsingKeyFile:(char*)keyFile andCertifcateFile:(char*)certFile {
    
}

-(BOOL)startSecureHTTPServerUsingKeyFile:(char*)keyFile andCertifcateFile:(char*)certFile withAuthDelegate:(id<GTKServerAuthDelegate>)anAuthDelegate {
    
}

-(void)stop {
    
}

- (NSString*)stringFromData:(NSData*)putData printingDebugInfo:(BOOL)logDebug {
    
}
*/

@end

@interface TestRequireAuth : GTKHTTPSwitchPath
@end

@implementation TestRequireAuth

-(id)serveGetRequest:(id)aGetRequest
{
    return @"Did get.";
}

@end

@interface TestAllowAll : GTKHTTPSwitchPath
@end

@implementation TestAllowAll
-(BOOL)
requiresAuthentication
{
    return NO;
}
-(id)
serveGetRequest:(id)aGetRequest
{
    if ([[aGetRequest URIParameters] count])
    {
        return [aGetRequest URIParameters];
    }
    else
    {
        return @"Did get.";
    }
}

@end



@implementation GTKHTTPPath2ClassDictionaryServer (Test)

+(id)
testSelectors        { return [self testGTKHTTPClassDictionaryServer]; }
+(id)
testGTKHTTPClassDictionaryServer
{
    return @[
        @"testAuthenticationFilter",
        @"testParametrizedRoutes",
    ];
}

+(void)
testAuthenticationFilter
{
    GTKHTTPPath2ClassDictionaryServer *x = [GTKHTTPPath2ClassDictionaryServer serverWithRouteDictionary:@{
        @"/testAuthenticationFilter" : [TestRequireAuth class],
        @"/testAllowAll"             : [TestAllowAll class]
    }];

    GTKHTTPServerSession *s = [GTKHTTPServerSession session];

    GTKHTTPGetRequest *r = [GTKHTTPGetRequest requestOnConnection:nil session:nil];
    [r setPath:@"/testAuthenticationFilter"];

    id unAuthResult = [x serveRequest:r];
    id expected = [x notAuthenticatedResponseForRequest:r];

    [r setPath:@"/testAllowAll"];
    id allowAllWithNoAuth = [x serveRequest:r];

    EXPECT_ID(expected, unAuthResult, @"Auth does not work.");
    EXPECT_ID(@"Did get.", allowAllWithNoAuth, @"Auth does not work.");

    AlwaysLog(@"Finished first test.");

    [s setUser:@{@"fake": @"user"}];
    [r setSession:s];

    AlwaysLog(@"Request      : %@", r);
    AlwaysLog(@"Session      : %@", s);
    AlwaysLog(@"Session User : %@", [s user]);

    [r setPath:@"/testAuthenticationFilter"];
    id authResult = [x serveRequest:r];
    [r setPath:@"/testAllowAll"];
    id allowAllWithAuth = [x serveRequest:r];

    EXPECT_ID(@"Did get.", authResult, @"Auth does not work.");
    EXPECT_ID(@"Did get.", allowAllWithAuth, @"Auth does not work.");
}

+(void)
testParametrizedRoutes
{
    GTKHTTPPath2ClassDictionaryServer *x = [GTKHTTPPath2ClassDictionaryServer serverWithRouteDictionary:@{
        @"/testAllowAll"                                 : [TestAllowAll class],
        @"/test/%firstParam"                             : [TestAllowAll class],
        @"/test/%firstParam/with_trailing/%secondParam"  : [TestAllowAll class],
    }];
    
    id result = nil;
    
    GTKHTTPGetRequest *r = [GTKHTTPGetRequest requestOnConnection:nil session:nil];
    
    [r setPath:@"/testAllowAll"];
    result = [x serveRequest:r];
    EXPECT_ID(@"Did get.", result, @"Auth does not work.");
    
    [r setPath:@"/test/theFirstNameToCheck"];
    result = [x serveRequest:r];
    
    EXPECT_TRUE([result isKindOfClass:[NSDictionary class]], FMT(@"Expecting dictionary got: %@", NSStringFromClass([result class])));
    EXPECT_ID(@{ @"firstParam": @"thefirstnametocheck" }, result, @"Was expecting different dict");
    
    [r setPath:@"/test/theFirstNameToCheck/with_trailing/theSecondNameToCheck"];
    result = [x serveRequest:r];
    EXPECT_ID((@{ @"firstParam": @"thefirstnametocheck", @"secondParam": @"thesecondnametocheck" }), result, @"Was expecting different dict");
}

@end
