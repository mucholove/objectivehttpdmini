//
//  GTKHTTPBundleResourcePath.m
//  ObjectiveHTTPDmini
//
//  Created by Gustavo Tavares on 9/24/20.
//  Copyright © 2020 Gustavo Tavares. All rights reserved.
//

#import <ObjectiveHTTPDmini/GTKHTTPBundleResourcePath.h>
#import <ObjectiveHTTPDmini/GTKHTTPServerResponse.h>

@implementation GTKHTTPBundleResourcePath
{
    NSString* _resourceName;
    NSString* _resourceType;
}

#if !__has_feature(objc_arc)
-(void)
    dealloc
{
    [_resourceName release];
    [_resourceType release];
    [super dealloc];

}
#endif

SET_ACCESSOR(NSString*, _resourceName, setResourceName);

+(NSString*)
    resourceName
{
    return nil;
}

-(NSString*)
    resourceName
{
    if (_resourceName) {
        return _resourceName;
    } else {
        return [[self class] resourceName];
    }
}

-(NSString*)
    mimeType
{
    return nil;
}

+(NSString*)
    resourceType
{
    return nil;
}

-(NSString*)
    resourceType
{
    if (_resourceType) {
        return _resourceType;
    } else {
        return [[self class] resourceType];
    }
}

-(id)
    serveRequest:(GTKHTTPRequest*)aRequest
{
    static GTKHTTPServerResponse* response;
    static NSData                *page;
    static NSError               *pageError;
    static dispatch_once_t        onceToken;
    NSString* aPath = [aRequest path];

    dispatch_once(&onceToken, ^{
        //retain path

        if (![self resourceName]) {
            pageError = [NSError
                errorWithDomain:@"GTKHTTPBundleResourcePathErrror"
                code:100
                userInfo:@{
                    NSLocalizedDescriptionKey : FMT(@"No resource name set for path: %@", aPath)
                }];
            return;
        }

        if (![self resourceType]) {
            pageError = [NSError
                errorWithDomain:@"GTKHTTPBundleResourcePathErrror"
                code:100
                userInfo:@{
                    NSLocalizedDescriptionKey : FMT(@"No resource type set for path: %@", aPath)
                }];
            return;
        }

        // NSString* allBundles = [NSBundle allBundles];

        NSBundle *bundleToUse
            = [NSBundle bundleForClass:[self class]];
            // = [NSBundle mainBundle];

        NSString* resourcePath = [bundleToUse pathForResource:[self resourceName] ofType:[self resourceType]];

        if (!resourcePath) {
            pageError = [NSError
                        errorWithDomain:@"GTKHTTPBundleResourcePathErrror"
                        code:100
                        userInfo:@{
                            NSLocalizedDescriptionKey : FMT(@"Error getting Resource Path - Requested Path: %@ — Bundle: %@ / Requested Resource Name: %@", aPath, bundleToUse, [self resourceName])
                        }];
            return;
        }


#if GNUSTEP
        page = RETAIN([NSData dataWithContentsOfFile:resourcePath]);
#else
        page = RETAIN([NSData dataWithContentsOfFile:resourcePath options:0 error:nil]);
#endif


        if (!page) {
            pageError = [NSError
                errorWithDomain:@"GTKHTTPBundleResourcePathErrror"
                code:100
                userInfo:@{
                    NSLocalizedDescriptionKey : FMT(@"Error reading resource for path %@ — Requested resource name is: %@", aPath, [self resourceName])
                }];
        }

        response = RETAIN([GTKHTTPServerResponse response]);
        [response setRawData:page];
        if ([self mimeType]) {
            [response setMIMEType:[self mimeType]];
        } else {
            [response setMIMEType:[MimeType mimeTypeForFileType:[self resourceType]]];
        }

    });

    if (pageError) {
        return pageError;
    } else {
        return response;
    }
}


@end
