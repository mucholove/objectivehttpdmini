//
//  GTKHTTPSwitchPathDefaultImplementation.h
//  ObjectiveHTTPDmini
//
//  Created by Gustavo Tavares on 9/22/20.
//  Copyright © 2020 Gustavo Tavares. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ObjectiveHTTPDmini/GTKHTTPPath2ClassDictionaryServer.h>
#import <ObjectiveHTTPDmini/GTKHTTPRuntimeNestablePath.h>
#import <GTKFoundation/GTKFoundation.h>



@interface GTKHTTPSwitchPath : GTKHTTPRuntimeNestablePath<IsGTKHTTPSwitchPath>
@end

@protocol GTKHTTPJSONPostPathDelegate <NSObject>

-(id)
addOrUpdateItems:(id)items
forSession:(id)session
error:(NSError**)outErr;

@end

@interface GTKHTTPJSONPostPath : GTKHTTPSwitchPath

ACCESSOR_H(Class, classToProcess, setClassToProcess);

+(BOOL)
isSchemaPath;

@end


