//
//  GTKHTTPBundleResourcePath.h
//  ObjectiveHTTPDmini
//
//  Created by Gustavo Tavares on 9/24/20.
//  Copyright © 2020 Gustavo Tavares. All rights reserved.
//

#import <ObjectiveHTTPDmini/GTKHTTPRuntimeNestablePath.h>



@interface GTKHTTPBundleResourcePath : GTKHTTPRuntimeNestablePath

scalarAccessor_h(NSString*, resourceName, setResourceName);
scalarAccessor_h(NSString*, resourceType, setResourceType);

@end


