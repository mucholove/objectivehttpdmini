//
//  GTKHTTPSwitchPathDefaultImplementation.m
//  ObjectiveHTTPDmini
//
//  Created by Gustavo Tavares on 9/22/20.
//  Copyright © 2020 Gustavo Tavares. All rights reserved.
//

#import <ObjectiveHTTPDmini/GTKHTTPSwitchPath.h>
#import <ObjectiveHTTPDmini/GTKHTTPPostRequest.h>

@implementation GTKHTTPSwitchPath

#if !__has_feature(objc_arc)
-(void)
    dealloc
{
    [super dealloc];

}
#endif

-(id)
    serveRequest:(GTKHTTPRequest*)aRequest
{
    DEBUG_NO
    // respondsToMimeType:(MimeType)
    
    GTKHTTPMethod requestMimeType = [aRequest HTTPMethod];
    
    DebugLog(@"Request MimeType: %@", NSStringForGTKHTTPMethod(requestMimeType));
    
    switch (requestMimeType)
    {
        case GTKHTTPMethod_Get:
            if ([self respondsToSelector:@selector(serveGetRequest:)]) {
                return [self serveGetRequest:aRequest];
            }
            DebugLog(@"Does not respond to GET `serveGetRequest` : %@", self);
            break;
        case GTKHTTPMethod_Post:
            if ([self respondsToSelector:@selector(servePostRequest:)])
            {
                return [self servePostRequest:aRequest];
            }
            DebugLog(@"Does not respond to POST `servePostRequest` : %@", self);
            break;
        case GTKHTTPMethod_Put:
            if ([self respondsToSelector:@selector(servePutRequest:)])
            {
                return [self servePutRequest:aRequest];
            }
            DebugLog(@"Does not respond to PUT `servePutRequest` : %@", self);
            break;
        case GTKHTTPMethod_Patch:
            if ([self respondsToSelector:@selector(servePatchRequest:)])
            {
                return [self servePatchRequest:aRequest];
            }
            DebugLog(@"Does not respond to PATCH `servePatchRequest` : %@", self);
            break;
        case GTKHTTPMethod_Delete:
            if ([self respondsToSelector:@selector(servePatchRequest:)])
            {
                return [self serveDeleteRequest:aRequest];
            }
            DebugLog(@"Does not respond to DELETE `serveDeleteRequest` : %@", self);
            break;
        case GTKHTTPMethod_Unknown:
            if ([self respondsToSelector:@selector(serveUnknownTypeRequest:)])
            {
                return [self serveUnknownTypeRequest:aRequest];
            }
            DebugLog(@"Does not respond to UNKNOWN `serveUnknownTypeRequest` : %@", self);
            break;
    }
    
    NSString* errorDescription;

    #if DEBUG
        errorDescription = FMT(@"Unsupported request method %@ at path %@ — of class %@.",
                                   StringForGTKHTTPMethod([aRequest HTTPMethod]),
                                   [aRequest path],
                                   NSStringFromClass([self class]));
    #else
        errorDescription = FMT(@"Unsupported request method %@ at path  %@",
                                   StringForGTKHTTPMethod([aRequest HTTPMethod]),
                                   [aRequest path]);
    #endif

    NSError* e = [NSError
        errorWithDomain:@"ServerError"
        code:501
        userInfo:@{
            NSLocalizedDescriptionKey :errorDescription
        }];

    return e;
}


@end


@implementation GTKHTTPJSONPostPath
{
    id<GTKHTTPJSONPostPathDelegate> _delegate;
    Class                           _classToProcess;
}

+(Class)
classToProcess
{
    SUB_CLASS_REPONSIBILITY
    return nil;
}

-(Class)
classToProcess
{
    if (_classToProcess)
    {
        return _classToProcess;
    }
    else
    {
        return [[self class] classToProcess];
    }
}

+(BOOL)
isSchemaPath
{
    return NO;
}

-(BOOL)
isSchemaPath
{
    return NO;
}

M_ACCESSOR(id<GTKHTTPJSONPostPathDelegate>, _delegate, delegate, setDelegate)

-(id)
servePostRequest:(GTKHTTPPostRequest*)theRequest
{
    NSData* data = [theRequest payload];
    
    GTKMASONParser* parser = nil;
    
    if ([self isSchemaPath])
    {
        parser = [[self classToProcess] schemaParser];
    }
    else
    {
        parser = [[self classToProcess] jsonParser];
    }
    
    NSError* internalError = nil;
    
    NSArray* items
        =  [parser parsedData:data
                   error:&internalError];
                   
    if (internalError)
    {
        return internalError;
    }
    
    id session = [theRequest session];

    NSArray<GTKDidUpdateServerType*>* identifierAndServerEditTimeArray
        = [[self delegate]
            addOrUpdateItems:items
            forSession:session
            error:&internalError];
        
    if (internalError)
    {
        return internalError;
    }
    else
    {
        return identifierAndServerEditTimeArray;
    }
}


@end
