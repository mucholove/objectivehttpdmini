//
//  GTKHTTPFilePath.m
//  ObjectiveHTTPDmini
//
//  Created by Gustavo Tavares on 9/22/20.
//  Copyright © 2020 Gustavo Tavares. All rights reserved.
//

#import <ObjectiveHTTPDmini/GTKHTTPFilePath.h>

@implementation GTKHTTPFilePath
{
    NSString* _filePath;
}
#if !__has_feature(objc_arc)
-(void)
    dealloc
{
    [_filePath release];
    [super dealloc];

}
#endif

SET_ACCESSOR(NSString*, _filePath, setFilePath);

+(NSString*)
    filePath
{
    return nil;
}

-(NSString*)
    filePath
{
    if (_filePath) {
        return _filePath;
    } else {
        return [[self class] filePath];
    }
}

-(id)
    serveRequest:(GTKHTTPRequest*)aRequest
{
    static NSData  *page;
    static NSError* pageError;
    static dispatch_once_t onceToken;
    NSString* aPath = [aRequest path];

    dispatch_once(&onceToken, ^{
        //retain path

        if ([self filePath]) {
		#if GNUSTEP
                page = RETAIN([NSData dataWithContentsOfFile:[self filePath]]);
		#else
                page = RETAIN([NSData dataWithContentsOfFile:[self filePath] options:0 error:nil]);
                #endif

                if (!page) {
                    pageError = [NSError
                        errorWithDomain:@"GTKHTTPBundleResourcePathErrror"
                        code:100
                        userInfo:@{
                            NSLocalizedDescriptionKey : FMT(@"Error reading file for path %@ — Requested file name is: %@", aPath, [self filePath])
                        }];
                }
            } else {
                pageError = [NSError
                    errorWithDomain:@"GTKHTTPBundleResourcePathErrror"
                    code:100
                    userInfo:@{
                        NSLocalizedDescriptionKey : FMT(@"No file name set for path: %@", aPath)
                    }];
            }
    });

    if (pageError) {
        return pageError;
    } else {
        return page;
    }
}


@end
