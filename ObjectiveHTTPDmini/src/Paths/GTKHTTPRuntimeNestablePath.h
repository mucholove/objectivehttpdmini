//
//  GTKHTTPRuntimeNestablePath.h
//  ObjectiveHTTPDmini
//
//  Created by Gustavo Tavares on 9/22/20.
//  Copyright © 2020 Gustavo Tavares. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ObjectiveHTTPDmini/GTKHTTPPath2ClassDictionaryServer.h>



@interface GTKHTTPRuntimeNestablePath : NSObject<IsGTKHTTPPathForClassDictionary>

@end


