#import <ObjectiveHTTPDmini/GTKFolderResponder.h>

@implementation GTKFolderResponder
{
    NSString* _prefix;
    NSArray*  _prefixes;
    NSString* _pathToFolder;
    NSArray*  _whiteListedTypes;
    NSArray*  _blackListedTypes;
}
M_SETTER(NSString*, _prefix,           setPrefix)
M_SETTER(NSString*, _pathToFolder,     setPath)
M_SETTER(NSArray*,  _whiteListedTypes, setWhiteListedTypes)
M_SETTER(NSArray*,  _blackListedTypes, setBlackListedTypes)
-(BOOL)
requiresAuthentication
{
    return NO;
}
-(id)
serveRequest:(GTKHTTPRequest*)aRequest
{
    DEBUG_YES
    
    GTKHTTPServerResponse* response      = nil;
    NSString*              pathToFile    = nil;
    NSString*              requestedPath = [aRequest path];
    NSString*              mimeType      = nil;
    NSData*                data          = nil;
    
    
    
    if ([requestedPath isEqual:_prefix])
    {
     
        GTKHTTPServerResponse* response = [GTKHTTPServerResponse response];
        [response setHTTPResponseCode:301];
        NSString* baseURL     = @"https://localhost:51001";
        BOOL prefixHasSuffix = [_prefix hasSuffix:@"/"];
        NSString* newLocation = FMT(@"%@%@%@index.html",  baseURL, _prefix, prefixHasSuffix ? @"" : @"/");
        [response setApplicationHeaders:@{
            @"Location": newLocation,
        }];
        return response;
    }
    else
    {
        NSString* slug = [requestedPath stringByReplacingCharactersInRange:NSMakeRange(0, [_prefix length]) withString:@""];
        pathToFile = [_pathToFolder stringByAppendingString:slug];
        pathToFile = [pathToFile stringByReplacingOccurrencesOfString:@"//" withString:@"/"];
    }
    
    if (response)
    {
        return response;
    }
    
    DebugLog(@"Will search for data at path: %@", pathToFile);
    // Won't work if the file is an `alias` done by left-click, make alias
    // Will work if file is a `ln -s` symlink
    data = [NSData dataWithContentsOfFile:pathToFile];
    
    if (!data) // Is it a folder??
    {
        DebugLog(@"HTML search failed. Will try as directory.");
        BOOL isDirectory = NO;
        
        BOOL fileExists = [[NSFileManager defaultManager]
                fileExistsAtPath:pathToFile
                isDirectory:&isDirectory];
                
        
                
        if (!fileExists)
        {
            BOOL hasSuffix = [pathToFile hasSuffix:@"/"];
            
            if (hasSuffix)
            {
                DebugLog(@"Has Suffix: /");
                DebugLog(@"Will change suffix...");
                pathToFile = [pathToFile stringByReplacingCharactersInRange:NSMakeRange([pathToFile length]-1, 1) withString:@""];
                DebugLog(@"Got path to file by changing suffix: %@", pathToFile);
                fileExists = [[NSFileManager defaultManager]
                    fileExistsAtPath:pathToFile
                    isDirectory:&isDirectory];
            }
        }
                
        DebugLog(@"File exists?  %@", fileExists  ? @"YES" : @"NO");
        DebugLog(@"Is Directory? %@", isDirectory ? @"YES" : @"NO");
        
        if (fileExists && isDirectory)
        {

            if (!data) // First catch. Try with .html
            {
                
                BOOL hasSuffix = [pathToFile hasSuffix:@"/"];
                NSString* htmlPathToFile = FMT(hasSuffix ? @"%@index.html" : @"%@/index.html", pathToFile);
                BOOL indexExists = NO;
                DebugLog(@"Search failed. Will try with path to index: %@", htmlPathToFile);
                
                indexExists = [[NSFileManager defaultManager] fileExistsAtPath:htmlPathToFile];
                
                if (indexExists)
                {
                
                    GTKHTTPServerResponse* response = [GTKHTTPServerResponse response];
                    [response setHTTPResponseCode:301];
                    NSString* baseURL     = @"https://localhost:51001";
                    NSString* newLocation = FMT(@"%@%@index.html",  baseURL, _prefix);
                    [response setApplicationHeaders:@{
                        @"Location": newLocation,
                    }];
                    return response;
                }
    
            }
            
            if (!data)
            {
                DebugLog(@"File exists AND is directory.");
                
                NSMutableString* s = [NSMutableString string];
                
                NSError* internalError = nil;
                
                NSString* dListString = FMT(@"Directory listing for %@</h2>", requestedPath);
                
                [s appendString:@"<html>"];
                [s appendString:@"<head>"];
                [s appendFormat:@"<title>%@</title>", dListString];
                [s appendString:@"</head>"];
                [s appendString:@"<body>"];
                [s appendFormat:@"<h2>%@</h2>", dListString];
                [s appendString:@"<ul>"];
                
                NSArray* directoryContents = nil;
                
                #if GNUSTEP
                directoryContents = [[NSFileManager defaultManager] directoryContentsAtPath:pathToFile];
                #else
                directoryContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:pathToFile error:&internalError];
                #endif
                
                DebugLog(@"Internal error: %@", directoryContents);
                
                for (id subPath in directoryContents)
                {
                    NSLog(@"Subpath: %@", subPath);
                    BOOL subPathIsDirectory = NO;
                    
                    [[NSFileManager defaultManager]
                        fileExistsAtPath:subPath
                        isDirectory:&subPathIsDirectory];
                    
                    NSString* toPublish = FMT(@"%@%@", subPath, subPathIsDirectory ? @"/" : @"");
                    
                    [s appendFormat:@"<li><a href=\"%@\">%@</li>", toPublish, toPublish];
                }
                
                [s appendString:@"</ul>"];
                [s appendString:@"</body>"];
                [s appendString:@"</html>"];
                
                response = [GTKHTTPServerResponse response];
                [response setRawData:[s asData]];
                [response setMIMEType:[MimeType html]];
                return response;
            }
        }
        else if (fileExists)
        {
            DebugLog(@"File exists but is not directory. Weird Weird.");
            // File exists. Error loading file...
        }
    }

    if (!data)
    {
        DebugLog(@"Data not found. Returning error.");
        return [NSError
            errorWithDomain:@"GTKFindDomain"
            code:404
            message:@"Resource not found"];
    }
    
    if (!mimeType)
    {
        NSString* pathExtension = [pathToFile pathExtension];
        mimeType = [MimeType mimeTypeForFileType:pathExtension];
        if (!mimeType)
        {
           mimeType = [MimeType html];
        }
    }
    
    response = [GTKHTTPServerResponse response];
    [response setRawData:data];
    [response setMIMEType:mimeType];
    return response;
}

@end


