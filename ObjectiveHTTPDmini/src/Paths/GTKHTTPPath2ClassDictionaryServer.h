//
//  GTKHTTPPath2ClassDictionaryServer.h
//  ObjectiveHTTPDmini
//
//  Created by Gustavo Tavares on 8/21/20.
//  Copyright © 2020 Gustavo Tavares. All rights reserved.
//

#import <ObjectiveHTTPDmini/GTKHTTPServer.h>
#import <ObjectiveHTTPDmini/GTKMHDHTTPRequest.h>
#import <ObjectiveHTTPDmini/GTKHTTPServerDelegate.h>
#import <ObjectiveHTTPDmini/GTKServerPathProtocols.h>




@interface ImpIsGTKHTTPPathForClassDictionary : NSObject<IsGTKHTTPPathForClassDictionary>
@end

@interface ImpIsGTKHTTPSwitchPath : ImpIsGTKHTTPPathForClassDictionary<IsGTKHTTPSwitchPath>
@end

@interface GTKHTTPPath2ClassDictionaryServer :  GTKHTTPServer<
    GTKHTTPServerDelegate,
    IsGTKHTTPPathForClassDictionary
>

+(NSDictionary*)
defaultRoutesToClasses;

-(id)
responderForRequest:(GTKHTTPRequest*)theRequest;

+(id)
server;

+(id)
serverWithRouteDictionary:(NSDictionary*)theRouteDictionary;

-(id)
initWithRouteDictionary:(NSDictionary*)theRouteDictionary;

scalarAccessor_h(NSDictionary*, routesToClasses, setRoutesToClasses);

-(void)
addRoute:(Class)classToInit
atPath:(NSString*)aString;

-(void)
setResponder:(id)instance
forPath:(NSString*)thePath;

-(id)
notAuthorizeddResponseForRequest:(GTKHTTPRequest*)theRequest
atPath:(NSString*)aPath;

-(id)
notAuthenticatedResponseForRequest:(GTKHTTPRequest*)theRequest
atPath:(NSString*)aPath;

-(id)
doesNotExistResponseForRequest:(GTKHTTPRequest*)theRequest
atPath:(NSString*)aPath;

@end


