

> ./bootstrap
Uncrustify not detected, hook not installed. Please install uncrustify if you plan on doing development
Creating directory build-aux
Copying file build-aux/config.rpath
Copying file po/Makevars.template
glibtoolize: putting auxiliary files in AC_CONFIG_AUX_DIR, 'build-aux'.
glibtoolize: copying file 'build-aux/ltmain.sh'
glibtoolize: putting macros in AC_CONFIG_MACRO_DIRS, 'm4'.
glibtoolize: copying file 'm4/libtool.m4'
glibtoolize: copying file 'm4/ltoptions.m4'
glibtoolize: copying file 'm4/ltsugar.m4'
glibtoolize: copying file 'm4/ltversion.m4'
glibtoolize: copying file 'm4/lt~obsolete.m4'
configure.ac:63: installing 'build-aux/compile'
configure.ac:62: installing 'build-aux/config.guess'
configure.ac:62: installing 'build-aux/config.sub'
configure.ac:27: installing 'build-aux/install-sh'
configure.ac:27: installing 'build-aux/missing'
Makefile.am: installing './INSTALL'
doc/Makefile.am:16: installing 'build-aux/mdate-sh'
doc/Makefile.am:16: installing 'build-aux/texinfo.tex'
doc/examples/Makefile.am: installing 'build-aux/depcomp'
parallel-tests: installing 'build-aux/test-driver'


> ./configure 
checking for a BSD-compatible install... /usr/bin/install -c
checking whether build environment is sane... yes
checking for a thread-safe mkdir -p... build-aux/install-sh -c -d
checking for gawk... no
checking for mawk... no
checking for nawk... no
checking for awk... awk
checking whether make sets $(MAKE)... yes
checking whether make supports nested variables... yes
checking whether z/OS special settings are required... no
checking for gawk... (cached) awk
checking for grep that handles long lines and -e... /usr/bin/grep
checking whether ln -s works... yes
checking whether make sets $(MAKE)... (cached) yes
checking build system type... x86_64-apple-darwin18.7.0
checking host system type... x86_64-apple-darwin18.7.0
checking whether make supports the include directive... yes (GNU style)
checking for gcc... gcc
checking whether the C compiler works... yes
checking for C compiler default output file name... a.out
checking for suffix of executables... 
checking whether we are cross compiling... no
checking for suffix of object files... o
checking whether we are using the GNU C compiler... yes
checking whether gcc accepts -g... yes
checking for gcc option to accept ISO C89... none needed
checking whether gcc understands -c and -o together... yes
checking dependency style of gcc... gcc3
checking for gcc option to accept ISO C99... none needed
checking for gcc option to accept ISO Standard C... (cached) none needed
checking how to run the C preprocessor... gcc -E
checking for presence of stdio.h... yes
checking for presence of wchar.h... yes
checking for presence of stdlib.h... yes
checking for presence of string.h... yes
checking for presence of strings.h... yes
checking for presence of stdint.h... yes
checking for presence of fcntl.h... yes
checking for presence of sys/types.h... yes
checking for presence of time.h... yes
checking for presence of unistd.h... yes
checking for fgrep... /usr/bin/grep -F
checking whether _GNU_SOURCE is already defined... no
checking whether headers accept _GNU_SOURCE... yes
checking whether to try __BSD_VISIBLE macro... no
checking whether to try _DARWIN_C_SOURCE macro... yes
checking whether _DARWIN_C_SOURCE is already defined... no
checking whether headers accept _DARWIN_C_SOURCE... yes
checking whether to try __EXTENSIONS__ macro... no
checking whether to try _NETBSD_SOURCE macro... no
checking whether to try _BSD_SOURCE macro... no
checking whether to try _TANDEM_SOURCE macro... no
checking whether to try _ALL_SOURCE macro... no
checking whether _XOPEN_SOURCE is already defined... no
checking headers for POSIX.1-2008/SUSv4 features... available, works with _XOPEN_SOURCE=700, works with extension macro
checking for useful system-specific features... yes
checking whether useful system-specific features works with _XOPEN_SOURCE=700... no
checking for final set of defined symbols... _GNU_SOURCE _DARWIN_C_SOURCE
checking how to print strings... printf
checking for a sed that does not truncate output... /usr/bin/sed
checking for egrep... /usr/bin/grep -E
checking for ld used by gcc... /Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/ld
checking if the linker (/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/ld) is GNU ld... no
checking for BSD- or MS-compatible name lister (nm)... /usr/bin/nm -B
checking the name lister (/usr/bin/nm -B) interface... BSD nm
checking the maximum length of command line arguments... 196608
checking how to convert x86_64-apple-darwin18.7.0 file names to x86_64-apple-darwin18.7.0 format... func_convert_file_noop
checking how to convert x86_64-apple-darwin18.7.0 file names to toolchain format... func_convert_file_noop
checking for /Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/ld option to reload object files... -r
checking for objdump... objdump
checking how to recognize dependent libraries... pass_all
checking for dlltool... no
checking how to associate runtime and link libraries... printf %s\n
checking for ar... ar
checking for archiver @FILE support... no
checking for strip... strip
checking for ranlib... ranlib
checking command to parse /usr/bin/nm -B output from gcc object... ok
checking for sysroot... no
checking for a working dd... /bin/dd
checking how to truncate binary pipes... /bin/dd bs=4096 count=1
checking for mt... no
checking if : is a manifest tool... no
checking for dsymutil... dsymutil
checking for nmedit... nmedit
checking for lipo... lipo
checking for otool... otool
checking for otool64... no
checking for -single_module linker flag... yes
checking for -exported_symbols_list linker flag... yes
checking for -force_load linker flag... yes
checking for ANSI C header files... yes
checking for sys/types.h... yes
checking for sys/stat.h... yes
checking for stdlib.h... yes
checking for string.h... yes
checking for memory.h... yes
checking for strings.h... yes
checking for inttypes.h... yes
checking for stdint.h... yes
checking for unistd.h... yes
checking for dlfcn.h... yes
checking for objdir... .libs
checking if gcc supports -fno-rtti -fno-exceptions... yes
checking for gcc option to produce PIC... -fno-common -DPIC
checking if gcc PIC flag -fno-common -DPIC works... yes
checking if gcc static flag -static works... no
checking if gcc supports -c -o file.o... yes
checking if gcc supports -c -o file.o... (cached) yes
checking whether the gcc linker (/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/ld) supports shared libraries... yes
checking dynamic linker characteristics... darwin18.7.0 dyld
checking how to hardcode library paths into programs... immediate
checking whether stripping libraries is possible... yes
checking if libtool supports shared libraries... yes
checking whether to build shared libraries... yes
checking whether to build static libraries... yes
checking for windres... no
checking whether NLS is requested... yes
checking for msgfmt... /usr/local/bin/msgfmt
checking for gmsgfmt... /usr/local/bin/msgfmt
checking for xgettext... /usr/local/bin/xgettext
checking for msgmerge... /usr/local/bin/msgmerge
checking for ld used by gcc... /Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/ld
checking if the linker (/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/ld) is GNU ld... no
checking for shared library run path origin... done
checking for CFPreferencesCopyAppValue... yes
checking for CFLocaleCopyCurrent... yes
checking for GNU gettext in libc... no
checking for iconv... yes
checking for working iconv... yes
checking how to link with libiconv... -liconv
checking for GNU gettext in libintl... yes
checking whether to use NLS... yes
checking where the gettext function comes from... external libintl
checking how to link with libintl... -L/usr/local/lib -lintl -Wl,-framework -Wl,CoreFoundation
checking for stdbool.h... yes
checking for bool... yes
checking whether "true" is defined or builtin... yes
checking whether "false" is defined or builtin... yes
checking whether "true" and "false" could be used... yes
checking whether C compiler accepts -Werror=attributes... yes
checking whether -Werror=attributes actually works... yes
checking for function inline keywords supported by gcc... inline __attribute__((always_inline))
checking for target host OS... Darwin
checking whether gcc is Clang... yes
checking whether Clang needs flag to prevent "argument unused" warning when linking with -pthread... no
checking for joinable pthread attribute... PTHREAD_CREATE_JOINABLE
checking whether more special flags are required for pthreads... no
checking for PTHREAD_PRIO_INHERIT... yes
checking for threading lib to use with libmicrohttpd (auto)... posix
checking for pthread_np.h... no
checking whether pthread_attr_setname_np is declared... no
checking whether pthread_setname_np is declared... yes
checking for pthread_setname_np(3) in NetBSD or OSF1 form... no
checking for pthread_setname_np(3) in GNU/Linux form... no
checking for pthread_setname_np(3) in Darwin form... yes
checking whether to enable thread names... yes
checking for sys/time.h... yes
checking for gettimeofday... yes
checking for time.h... yes
checking for nanosleep... yes
checking for unistd.h... (cached) yes
checking for usleep... yes
checking for string.h... (cached) yes
checking for sys/types.h... (cached) yes
checking for sys/socket.h... yes
checking for netinet/in.h... yes
checking for time.h... (cached) yes
checking for sys/select.h... yes
checking for netinet/tcp.h... yes
checking whether shutdown of listen socket trigger select()... no
checking whether sendmsg is available... checking for library containing sendmsg... none required
checking whether writev is available... checking for writev... yes
checking whether MSG_MORE is defined... no
checking whether the linker accepts -fno-strict-aliasing... yes
checking whether C compiler accepts -fno-strict-aliasing... yes
checking whether byte ordering is bigendian... no
checking for variable-length arrays... yes
checking whether __builtin_bswap32() is available... yes
checking whether __builtin_bswap64() is available... yes
checking for curl... yes
checking for makeinfo... yes
checking for poll.h... yes
checking for poll... yes
checking linux/version.h usability... no
checking linux/version.h presence... no
checking for linux/version.h... no
checking for Linux epoll(7) interface... no
checking for fcntl.h... yes
checking for math.h... yes
checking for errno.h... yes
checking for limits.h... yes
checking for stdio.h... yes
checking for locale.h... yes
checking for sys/stat.h... (cached) yes
checking for sys/types.h... (cached) yes
checking for sys/types.h... (cached) yes
checking for sys/time.h... (cached) yes
checking for sys/msg.h... yes
checking for time.h... (cached) yes
checking for sys/mman.h... yes
checking for sys/ioctl.h... yes
checking for sys/socket.h... (cached) yes
checking for sys/select.h... (cached) yes
checking for netdb.h... yes
checking for netinet/in.h... (cached) yes
checking for netinet/ip.h... yes
checking for netinet/tcp.h... (cached) yes
checking for arpa/inet.h... yes
checking for endian.h... no
checking for machine/endian.h... yes
checking for sys/endian.h... no
checking for sys/param.h... yes
checking for sys/machine.h... no
checking for sys/byteorder.h... no
checking for machine/param.h... yes
checking for sys/isa_defs.h... no
checking for inttypes.h... (cached) yes
checking for stddef.h... yes
checking for unistd.h... (cached) yes
checking for sockLib.h... no
checking for inetLib.h... no
checking for net/if.h... yes
checking for search.h... yes
checking for tsearch... yes
checking whether tdelete works... yes
checking for dlfcn.h... (cached) yes
checking for zlib.h... yes
checking for function random... yes
checking for struct sockaddr_in.sin_len... yes
checking for struct sockaddr_in6.sin6_len... yes
checking for struct sockaddr_storage.ss_len... yes
checking for function getsockname... yes
checking whether getsockname() is usable... yes
checking for sys/eventfd.h... no
checking whether pipe(3) is usable... yes
checking whether pipe2(2) is usable... no
checking for accept4... no
checking for gmtime_r... yes
checking for memmem... yes
checking for snprintf... yes
checking whether gmtime_s is declared... no
checking whether SOCK_NONBLOCK is declared... no
checking whether clock_gettime is declared... yes
checking for library containing clock_gettime... none required
checking for clock_get_time... yes
checking for gethrtime... no
checking for IPv6... yes
checking for function sysconf... no
checking whether the linker accepts -fvisibility=hidden... yes
checking whether C compiler accepts -fvisibility=hidden... yes
checking for gawk... (cached) awk
checking for curl-config... /usr/bin/curl-config
checking for the version of libcurl... 7.54.0
checking for libcurl >= version 7.16.4... yes
checking whether libcurl is usable... yes
checking for curl_free... yes
checking for suitable libmagic... no
checking for special C compiler options needed for large files... no
checking for _FILE_OFFSET_BITS value needed for large files... no
checking for _LARGEFILE_SOURCE value needed for large files... no
checking for lseek64... no
checking for pread64... no
checking for pread... yes
checking for Linux-style sendfile(2)... no
checking for FreeBSD-style sendfile(2)... no
checking for Darwin-style sendfile(2)... yes
checking whether to generate error messages... yes
checking whether to enable postprocessor... yes
checking for zzuf... no
checking for socat... no
checking for pkg-config... /usr/local/bin/pkg-config
checking pkg-config is at least version 0.9.0... yes
checking how to find GnuTLS library... automatically
checking whether to add pkg-config special search directories... no
checking for gnutls... yes
checking whether GnuTLS is usable... yes
checking for gnutls_privkey_import_x509_raw()... yes
checking whether GnuTLS require libgcrypt initialisaion... no
checking whether to support HTTPS... yes (using libgnutls)
checking whether to support HTTP basic authentication... yes
checking whether to support HTTP digest authentication... yes
checking whether to support HTTP "Upgrade"... yes
checking for calloc()... yes
checking for fork()... yes
checking for waitpid()... yes
checking whether to compile with support for code coverage analysis... no
checking the number of available CPUs... 8
checking whether to enable debug asserts... no
checking whether to compile experimental code... no
checking that generated files are newer than configure... done
configure: creating ./config.status
config.status: creating po/Makefile.in
config.status: creating src/microhttpd/microhttpd_dll_res.rc
config.status: creating libmicrohttpd.pc
config.status: creating w32/common/microhttpd_dll_res_vc.rc
config.status: creating Makefile
config.status: creating contrib/Makefile
config.status: creating doc/Makefile
config.status: creating doc/doxygen/libmicrohttpd.doxy
config.status: creating doc/doxygen/Makefile
config.status: creating doc/examples/Makefile
config.status: creating m4/Makefile
config.status: creating src/Makefile
config.status: creating src/include/Makefile
config.status: creating src/lib/Makefile
config.status: creating src/microhttpd/Makefile
config.status: creating src/examples/Makefile
config.status: creating src/testcurl/Makefile
config.status: creating src/testcurl/https/Makefile
config.status: creating src/testzzuf/Makefile
config.status: creating MHD_config.h
config.status: executing depfiles commands
config.status: executing libtool commands
config.status: executing po-directories commands
config.status: creating po/POTFILES
config.status: creating po/Makefile
configure: GNU libmicrohttpd 0.9.70 Configuration Summary:
  Target directory:  /usr/local
  Cross-compiling:   no
  Operating System:  darwin18.7.0
  Shutdown of listening socket trigger select: no
  Inter-thread comm: pipe
  poll support:      yes
  epoll support:     no
  sendfile used:     yes, Darwin-style
  HTTPS support:     yes (using libgnutls)
  Threading lib:     posix
  Use thread names:  yes
  Use debug asserts: no
  Messages:          yes
  Gettext:           yes
  Basic auth.:       yes
  Digest auth.:      yes
  HTTP "Upgrade":    yes
  Postproc:          yes
  Build docs:        yes
  Build examples:    yes
  Test with libcurl: yes

configure: HTTPS subsystem configuration:
  License         :  LGPL only
