 if you want to produce a heading such as <h1>Something great</h1> you’ll see that you should use the 
 WAHtmlCanvas>>heading message which produces a brush instance of WAHeadingTag which has the following API:

Methods in WAHeadingTag    Description
level:    Specify anInteger as the heading level for this brush.
with:    Render this heading tag with anObject as its body.

Another Example:

    html paragraph with: 'Hello world.'.
    html orderedList with: [
       html listItem: 'Item 1'.
       html listItem: 'Item 2' ].
       
To help you to find the correct brush, the brushes are presented from the perspective of the HTML tags in the following table:

HTML         Factory Selector    Brush Class
----------------------------------------------------------
a            anchor              WAAnchorTag
a            map                 WAImageMapTag
a            popupAnchor         WAPopupAnchorTag
abbr         abbreviated         WAGenericTag
acronym      acronym             WAGenericTag
address      address             WAGenericTag
big          big                 WAGenericTag
blockquote   blockquote          WAGenericTag
br           break               WABreakTag
button       button              WAButtonTag
caption      tableCaption        WAGenericTag
cite         citation            WAGenericTag
code         code                WAGenericTag
col          tableColumn         WATableColumnTag
colgroup     tableColumnGroup    WATableColumnGroupTag
dd           definitionData      WAGenericTag
del          deleted             WAEditTag
dfn          definition          WAGenericTag
div          div                 WADivTag
dl           definitionList      WAGenericTag
dt           definitionTerm      WAGenericTag
em           emphasis            WAGenericTag
fieldset     fieldSet            WAFieldSetTag
form         form                WAGenericTag
h1           heading             WAHeadingTag
hr           horizontalRule      WAHorizontalRuleTag
iframe       iframe              WAIframeTag
img          image               WAImageTag
input        cancelButton        WACancelButtonTag
input        checkbox            WACheckboxTag
input        fileUpload          WAFileUploadTag
input        hiddenInput         WAHiddenInputTag
input        imageButton         WAImageButtonTag
input        passwordInput       WAPasswordInputTag
input        radioButton         WARadioButtonTag
input        submitButton        WASubmitButtonTag
input        textInput           WATextInputTag
ins          inserted            WAEditTag
kbd          keyboard            WAGenericTag
label        label               WALabelTag
legend       legend              WAGenericTag
li           listItem            WAGenericTag
object       object              WAObjectTag
ol           orderedList         WAOrderedListTag
optgroup     optionGroup         WAOptionGroupTag
option       option              WAOptionTag
p            paragraph           WAGenericTag
param        parameter           WAParameterTag
pre          preformatted        WAGenericTag
q            quote               WAGenericTag
rb           rubyBase            WAGenericTag
rbc          rubyBaseContainer   WAGenericTag
rp           rubyParentheses     WAGenericTag
rt           rubyText            WARubyTextTag
rtc          rubyTextContainer   WAGenericTag
ruby         ruby                WAGenericTag
samp         sample              WAGenericTag
script       script              WAScriptTag
select       multiSelect         WAMultiSelectTag
select       select              WASelectTag
small        small               WAGenericTag
span         span                WAGenericTag
strong       strong              WAGenericTag
sub           subscript           WAGenericTag
sup           superscript         WAGenericTag
table         table               WATableTag
tag:           tag:                WAGenericTag
tbody        tableBody           WAGenericTag
td              tableData           WATableDataTag
textarea     textArea            WATextAreaTag
tfoot           tableFoot           WAGenericTag
th              tableHeading        WATableHeadingTag
thead        tableHead           WAGenericTag
tr               tableRow            WAGenericTag
tt               teletype            WAGenericTag
ul              unorderedList       WAUnorderedListTag
var             variable            WAGenericTag#



Hierarchy
WABrush
WACompound
   WADateInput
   WATimeInput
WATagBrush
   WAAnchorTag
      WAImageMapTag
      WAPopupAnchorTag
   WAAudioTag
   WABasicFormTag
      WAFormTag
   WABreakTag
   WACanvasTag
   WACollectionTag
      WADatalistTag
      WAListTag
         WAOrderedListTag
         WAUnorderedListTag
      WASelectTag
         WAMultiSelectTag
   WACommandTag
   WADetailsTag
   WADivTag
   WAEventSourceTag
   WAFieldSetTag
   WAFormInputTag
      WAAbstractTextAreaTag
         WAColorInputTag
         WAEmailInputTag
         WASearchInputTag
         WASteppedTag
            WAClosedRangeTag
               WANumberInputTag
               WARangeInputTag
               WATimeInputTag
            WADateInputTag
            WADateTimeInputTag
            WADateTimeLocalInputTag
            WAMonthInputTag
            WAWeekInputTag
         WATelephoneInputTag
         WATextAreaTag
         WATextInputTag
            WAPasswordInputTag
         WAUrlInputTag
      WAButtonTag
      WACheckboxTag
      WAFileUploadTag
      WAHiddenInputTag
      WARadioButtonTag
      WASubmitButtonTag
         WACancelButtonTag
         WAImageButtonTag
   WAGenericTag
      WAEditTag
   WAHeadingTag
   WAHorizontalRuleTag
   WAIframeTag
   WAImageTag
   WAKeyGeneratorTag
   WALabelTag
   WAMenuTag
   WAMeterTag
   WAObjectTag
   WAOptionGroupTag
   WAOptionTag
   WAParameterTag
   WAProgressTag
   WARubyTextTag
   WAScriptTag
   WASourceTag
   WATableCellTag
      WATableColumnGroupTag
         WATableColumnTag
      WATableDataTag
         WATableHeadingTag
   WATableTag
   WATimeTag
   WAVideoTag
