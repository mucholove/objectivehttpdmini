#include <Foundation/Foundation.h>
#include <ObjectiveHTTPDmini/ObjectiveHTTPDmini.h>

char *
    load_file_as_string(
        char * filename)
{
    FILE *f = fopen(filename, "rb");
    
    if (f == NULL)
    {
        return NULL;
    }
    
    fseek(f, 0, SEEK_END);
    long fsize = ftell(f);
    fseek(f, 0, SEEK_SET);  /* same as rewind(f); */

    char *string = malloc(fsize + 1);
    fread(string, 1, fsize, f);
    fclose(f);

    string[fsize] = 0;
    
    return string;
}

int main (void)
{
      @autoreleasepool {
        char *keyFile;
        char *certFile;

        AlwaysLog(@"Starting Server: %@", NSStringFromClass([GTKHTTPServer class]));


        char * keyFileName
	    = "/root/KNN-build/objectivehttpdmini/DemoServer/server.key";
	    // = "/home/ubuntu/ObjectiveHTTPDmini/DemoServer/server.key";
            // = "/Users/gtk-air/Code/ThirdDraft/KnittyServer/server.key";
            // = "KnittyServer/server.key";
            // = "server.key";

        char * certFileName
	    = "/root/KNN-build/objectivehttpdmini/DemoServer/server.pem";
	    // = "/home/ubuntu/ObjectiveHTTPDmini/DemoServer/server.pem";
            // = "/Users/gtk-air/Code/ThirdDraft/KnittyServer/server.pem";
            // = "KnittyServer/server.pem";
            // = "server.pem";

        keyFile
            = load_file_as_string(keyFileName);

        certFile
            = load_file_as_string(certFileName);

        if ((keyFile == NULL) || (certFile == NULL))
        {
          printf ("The key/certificate files could not be read.\n");
          return 1;
        }

        // KNNWebServer *server
        //    = [[KNNWebServer alloc] init];

        GTKHTTPServer *server
            = [[GTKHTTPServer alloc] init];

        BOOL serverStarted
            // = [server startHttpd];
            = [server
                startSecureHTTPServerUsingKeyFile:keyFile
                andCertifcateFile:certFile];

        if (serverStarted)
        {
            while (getchar() != 'q')
            {
                AlwaysLog(@"Server will keep running");
            }
            [server stop];
        }
        else
        {
            AlwaysLog(@"Server couldn't be started");
        }

        //
        free(keyFileName);
        free(certFileName);
        //
        free(keyFile);
        free(certFile);
    }
    return 0;
}

